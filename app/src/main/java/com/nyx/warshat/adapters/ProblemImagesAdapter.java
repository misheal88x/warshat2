package com.nyx.warshat.adapters;

import android.content.Context;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nyx.warshat.Intrafaces.IMove;
import com.nyx.warshat.R;
import com.nyx.warshat.models.ProblemImageObject;

import java.util.List;

public class ProblemImagesAdapter extends RecyclerView.Adapter<ProblemImagesAdapter.ViewHolder> {
    private Context context;
    private List<ProblemImageObject> list;
    private IMove iMove;

    public ProblemImagesAdapter(Context context,List<ProblemImageObject> list,IMove iMove) {
        this.context = context;
        this.list = list;
        this.iMove = iMove;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView img_image;

        public ViewHolder(View view) {
            super(view);
            img_image = view.findViewById(R.id.item_problem_image_image);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_problem_image, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ProblemImageObject pio = list.get(position);
        if (pio.getType() == 0){
            holder.img_image.setImageResource(R.drawable.ic_plus_icon_white);
            holder.img_image.setBackgroundColor(Color.parseColor("#F7F7F7"));
            holder.img_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    iMove.move(position);
                }
            });
        }
        else if (pio.getType() == 1){
            holder.img_image.setImageBitmap(list.get(position).getBitmap());
        } else {
            Glide.with(context).load(pio.getUrl())
                    .apply(RequestOptions.placeholderOf(R.drawable.placeholder))
                    .into(holder.img_image);
        }
    }
}
