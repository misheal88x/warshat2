package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.infinites.CategoryCompanies;
import com.nyx.warshat.activities.infinites.CategoryWorkshops;
import com.nyx.warshat.models.Category;

import java.util.List;


public class CategoryAdapter
        extends RecyclerView.Adapter {

    private List data;
    private Activity C;
    private boolean is_emergency;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public View body;
        public TextView text;
        public ImageView pic;
        public MyViewHolder(View view) {
            super(view);
            body = view.findViewById(R.id.body);
            text = view.findViewById(R.id.text);
            pic = view.findViewById(R.id.pic);
        }
    }


    public CategoryAdapter(List moviesList , Activity c,boolean is_emergency) {
        this.data = moviesList;
        this.C = c;
        this.is_emergency = is_emergency;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_image_text_card_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, final int position) {
        final Category r = (Category)data.get(position);

      final  MyViewHolder holder = (MyViewHolder)h;

        Glide.with(C).load(r.getPic()).into(holder.pic);
        holder.text.setText(r.getName());
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii=new Intent(C , CategoryWorkshops.class);
                if(r.getType().equals(Category.TYPE_COMPANY))
                     ii=new Intent(C , CategoryCompanies.class);
                else
                    ii.putExtra("is_emergency",is_emergency);
                ii.putExtra("cat_id" ,r.getId());
                ii.putExtra("icon" ,r.getPic());
                ii.putExtra("name" ,r.getName());
                C.startActivity(ii);
                //
            }
        });

    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}