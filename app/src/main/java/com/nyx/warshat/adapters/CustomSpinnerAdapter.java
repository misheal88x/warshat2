package com.nyx.warshat.adapters;

import android.content.Context;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nyx.warshat.R;

import java.util.List;

public class CustomSpinnerAdapter extends ArrayAdapter<String> {
    private List<String> list;
    private LayoutInflater mInflater;
    public CustomSpinnerAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.list = objects;
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        //TextView view = (TextView) super.getView(position, convertView, parent);
        //view.setGravity(Gravity.CENTER|Gravity.RIGHT);
        //view.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/GE_SS_Two_Light.otf")/*, -1*/);
        //view.setTextColor(getContext().getResources().getColor(R.color.dark_grey));
        //return view;
        final CustomSpinnerAdapter.ListContent holder;
        View v = convertView;
        if (v == null) {
            v = mInflater.inflate(R.layout.my_spinner_style, null);
            holder = new CustomSpinnerAdapter.ListContent();
            holder.name = (TextView) v.findViewById(R.id.textView1);
            v.setTag(holder);
        } else {
            holder = (CustomSpinnerAdapter.ListContent) v.getTag();
        }
        holder.name.setText(list.get(position));
        return v;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        //TextView view = (TextView) super.getDropDownView(position, convertView, parent);
        //view.setGravity(Gravity.CENTER|Gravity.RIGHT);
        //view.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/GE_SS_Two_Light.otf")/*, -1*/);
        //view.setTextColor(getContext().getResources().getColor(R.color.dark_grey));
        //return view;
        final CustomSpinnerAdapter.ListContent holder;
        View v = convertView;
        if (v == null) {
            v = mInflater.inflate(R.layout.my_spinner_style, null);
            holder = new CustomSpinnerAdapter.ListContent();
            holder.name = (TextView) v.findViewById(R.id.textView1);
            v.setTag(holder);
        } else {
            holder = (CustomSpinnerAdapter.ListContent) v.getTag();
        }
        holder.name.setText(list.get(position));
        return v;
    }

    static class ListContent {
        TextView name;
    }
}
