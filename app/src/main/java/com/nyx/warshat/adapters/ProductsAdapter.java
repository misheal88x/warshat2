package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nyx.warshat.R;
import com.nyx.warshat.models.ProductSelector;

import java.util.List;

import Arabs.ArabTextView;


public class ProductsAdapter
        extends RecyclerView.Adapter {

        private List data;
    private Activity C;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ArabTextView item;
        public MyViewHolder(View view) {
            super(view);
            item = view.findViewById(R.id.text);
        }
    }

    public ProductsAdapter(List moviesList , Activity c) {
        this.data = moviesList;
        this.C = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_company_work, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, final int position) {
        final ProductSelector r = (ProductSelector)data.get(position);

      final  MyViewHolder holder = (MyViewHolder)h;

      holder.item.setText(r.getName());
      /*
      holder.item.setOnCheckedChangeListener(null);
      holder.item.setChecked(r.isChecked());
      holder.item.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
          @Override
          public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
              r.setChecked(isChecked);
          }
      });
**/

    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}