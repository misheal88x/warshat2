package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.details.WorkshopDetails;
import com.nyx.warshat.models.Workshop;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;


public class WorkshopsAdapter
        extends RecyclerView.Adapter {

        private List data;
    private Activity C;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name ,address ,rating_no,response_rate;
        public View body;
        public ImageView pic;
        public RatingBar rating;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            rating_no = (TextView) view.findViewById(R.id.rate_no);
            address = (TextView) view.findViewById(R.id.address);
            body = view.findViewById(R.id.body);
            pic = view.findViewById(R.id.pic);
            rating = view.findViewById(R.id.rating);
            response_rate = view.findViewById(R.id.ans_rate);
        }
    }


    public WorkshopsAdapter(List moviesList , Activity c) {
        this.data = moviesList;
        this.C = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_workshop_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, int position) {
        final Workshop r = (Workshop)data.get(position);

      final  MyViewHolder holder = (MyViewHolder)h;
        if (r.getName()!=null&&!r.getName().equals("null")){
            holder.name.setText(r.getName());
        }else {
            holder.name.setText("لا يوجد اسم");
        }

        holder.address.setText( r.getAddress());
        Glide.with(C).load(r.getPic())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big))
                .into(holder.pic);
        if (r.getRating() == 0f){
            holder.rating.setRating((float) r.getRating());
            holder.rating_no.setText("0");
        }else {
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
            DecimalFormat f = (DecimalFormat) nf;
            f.applyPattern("##.0");
            String formattedValue = f.format(r.getRating());
            String formatted_rate = String.valueOf(r.getRating());
            holder.rating.setRating((float) r.getRating());
            holder.rating_no.setText(formattedValue);
        }
        //DecimalFormat f = new DecimalFormat("##.0");
        //String formattedValue = f.format(r.getRating());
        //holder.rating.setRating((float) r.getRating());
        //holder.rating_no.setText(String.valueOf(r.getRating()));
        //holder.rating_no.setText(formattedValue);

        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent i = new Intent(C , RequestDetails.class);
//                i.putExtra("id" ,r.getId());
//                C.startActivity(i);
                Intent ii=new Intent(C , WorkshopDetails.class);
                ii.putExtra("id" ,r.getId());
                ii.putExtra("icon" ,r.getPic());
                if (r.getName()!=null&&!r.getName().equals("null")){
                    ii.putExtra("name" ,r.getName());
                }else {
                    ii.putExtra("name" ,"لا يوجد اسم");
                }

                C.startActivity(ii);
            }
        });

        try {
            double a = r.getResponse_time_rating();
            if (a >= 0 && a <= 1) {
                holder.response_rate.setText("معدل الرد : بطيء جدا");
            } else if (a > 1 && a <= 2) {
                holder.response_rate.setText("معدل الرد : بطيء");
            } else if (a > 2 && a <= 3) {
                holder.response_rate.setText("معدل الرد : متوسط");
            } else if (a > 3 && a <= 4) {
                holder.response_rate.setText("معدل الرد : سريع");
            } else if (a > 4 && a <= 5) {
                holder.response_rate.setText("معدل الرد : سريع جدا");
            }
        }catch (Exception e){}

    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}