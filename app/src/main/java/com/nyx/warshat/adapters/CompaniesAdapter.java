package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.details.CompanyDetails;
import com.nyx.warshat.activities.details.WorkshopDetails;
import com.nyx.warshat.models.Workshop;

import java.util.List;


public class CompaniesAdapter
        extends RecyclerView.Adapter {

        private List data;
    private Activity C;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name ,address;
        public View body;
        public ImageView pic;
        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            address = (TextView) view.findViewById(R.id.address);
            body = view.findViewById(R.id.body);
            pic = view.findViewById(R.id.pic);
        }
    }


    public CompaniesAdapter(List moviesList , Activity c) {
        this.data = moviesList;
        this.C = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_company_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, int position) {
        final Workshop r = (Workshop)data.get(position);

      final  MyViewHolder holder = (MyViewHolder)h;
        holder.name.setText(r.getName());
        holder.address.setText( r.getAddress());
        Glide.with(C).load(r.getPic())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big).centerCrop()).into(holder.pic);
if(r.getType().equals("5"))
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ii=new Intent(C , CompanyDetails.class);
                ii.putExtra("id" ,r.getId());
                ii.putExtra("icon" ,r.getPic());
                ii.putExtra("name" ,r.getName());
                C.startActivity(ii);
            }
        });
else
    holder.body.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent ii=new Intent(C , WorkshopDetails.class);
            ii.putExtra("id" ,r.getId());
            ii.putExtra("icon" ,r.getPic());
            ii.putExtra("name" ,r.getName());
            C.startActivity(ii);
        }
    });

    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}