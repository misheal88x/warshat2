package com.nyx.warshat.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nyx.warshat.R;
import com.nyx.warshat.utilities.BaseFunctions;

import java.util.List;

import Arabs.ArabTextView;

public class RequestDatesAdapter extends RecyclerView.Adapter<RequestDatesAdapter.ViewHolder> {
    private Context context;
    private List<String> list;


    public RequestDatesAdapter(Context context,List<String> list) {
        this.context = context;
        this.list = list;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ArabTextView tv_text;

        public ViewHolder(View view) {
            super(view);
            tv_text = view.findViewById(R.id.item_date_txt);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_date, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        String in = list.get(position);
        //String month = in.substring(0,2);
        //String day = in.substring(3,5);
        //String year = in.substring(6, in.length());
        String day = in.substring(8,10);
        String month = in.substring(5,7);
        String year = in.substring(0,4);
        String dayTxt = BaseFunctions.dateToDayConverter(BaseFunctions.stringToDateConverter(year+"-"+month+"-"+day));
        String monthTxt = "";
        switch (month){
            case "01" : monthTxt = "كانون الثاني";break;
            case "02" : monthTxt = "شباط";break;
            case "03" : monthTxt = "اذار";break;
            case "04" : monthTxt = "نيسان";break;
            case "05" : monthTxt = "أيار";break;
            case "06" : monthTxt = "حزيران";break;
            case "07" : monthTxt = "تموز";break;
            case "08" : monthTxt = "اب";break;
            case "09" : monthTxt = "إيلول";break;
            case "10" : monthTxt = "تشرين الأول";break;
            case "11" : monthTxt = "تشرين الثاني";break;
            case "12" : monthTxt = "كانون الأول";break;
        }
        holder.tv_text.setText(dayTxt+" "+day+" "+monthTxt);
    }
}
