package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.nyx.warshat.R;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.SelectableRegion;

import org.json.JSONException;

import java.util.List;


public class SelectedRegionsAdapterEditable
        extends RecyclerView.Adapter {

        private List data;
    private Activity C;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public CheckBox c2;
        public View del;
        public MyViewHolder(View view) {
            super(view);
            text = view.findViewById(R.id.name);
            c2 = view.findViewById(R.id.emergency);
            del = view.findViewById(R.id.delete);
        }
        public  SelectableRegion r;
        public int position;
    }


    public SelectedRegionsAdapterEditable(List moviesList , Activity c) {
        this.data = moviesList;
        this.C = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_selectable_region_editable, parent, false);
final MyViewHolder holder =new MyViewHolder(itemView);


        holder.del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.del.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(final View v) {
                        //delete logic here

                        new AlertDialog.Builder(C)
                                .setTitle("حذف المنطقة")
                                .setMessage("لن تظهر ورشتك في نتائج البحث المتعلقة ب"+holder.r.getName())
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        holder.del.setEnabled(false);

                                        BackgroundServices.getInstance(C).setBaseUrl(APIUrl.SERVER+"regions/delete")
                                                .addPostParam("id" ,holder.r.getId())
                                                .CallPost(new PostAction() {
                                                    @Override
                                                    public void whenFinished(String status, String response) throws JSONException {
                                                        data.remove(holder.position);
                                                        SelectedRegionsAdapterEditable.this.notifyDataSetChanged();

                                                    }
                                                });


                                    }})
                                .setNegativeButton(android.R.string.no, null).show();









                    }
                });
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, final int position) {
        final SelectableRegion r = (SelectableRegion)data.get(position);
      final  MyViewHolder holder = (MyViewHolder)h;
      holder.r = r;
      holder.position = position;

        holder.text.setText(r.getName());
        holder.c2.setChecked(r.isEmergency());
        holder.c2.setEnabled(false);
        holder.del.setVisibility(View.VISIBLE);



    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}