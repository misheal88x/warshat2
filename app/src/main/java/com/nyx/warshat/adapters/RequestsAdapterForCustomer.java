package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import Arabs.ArabTextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.details.RateWorkshop;
import com.nyx.warshat.activities.details.RequestDetails;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.RequestItem;

import org.json.JSONException;

import java.util.List;

import Arabs.ArabButton;


public class RequestsAdapterForCustomer
        extends RecyclerView.Adapter {

        private List data;
    private Activity C;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView workshop,date ,status;
        public View body ;
        public ArabButton delete;
        public ImageView pic;
        public MyViewHolder(View view) {
            super(view);
            workshop = (TextView) view.findViewById(R.id.workshop);
            status = (TextView) view.findViewById(R.id.status);
            date = (TextView) view.findViewById(R.id.date);
            body = view.findViewById(R.id.body);
            pic = view.findViewById(R.id.pic);
            delete = view.findViewById(R.id.cancel_request);
        }
    }


    public RequestsAdapterForCustomer(List moviesList , Activity c) {
        this.data = moviesList;
        this.C = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_request_item_customer, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, final int position) {
        final RequestItem r = (RequestItem)data.get(position);

      final  MyViewHolder holder = (MyViewHolder)h;
        holder.workshop.setText(r.getWorkshop());
        holder.status.setText(r.getStatusString(C));
        Log.i("gfgtgfg", "onBindViewHolder: "+r.getPic());
        Log.i("gfgtgfg", "onBindViewHolder: "+r.getId());
        Glide.with(C).load(r.getPic())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                Target<Drawable> target, boolean isFirstResource) {
                        Log.i("gfgtgfg", "onBindViewHolder: "+e.getMessage());
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                   DataSource dataSource, boolean isFirstResource) {

                        return false;
                    }
                })
                .into(holder.pic);
        if(r.getStatus().equals(RequestItem.REQUEST_FINISHED))
        {
            holder.delete.setVisibility(View.GONE);
            if(!r.isRated()) {
                holder.status.setText(C.getString(R.string.waiting_for_rating));
                holder.body.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (r.getStatus().equals(RequestItem.REQUEST_FINISHED)) {
                            Intent i = new Intent(C, RateWorkshop.class);
                            i.putExtra("id", r.getTo());
                            i.putExtra("name", r.getWorkshop());
                            i.putExtra("problem_pic", r.getPic());
                            i.putExtra("icon", r.getWorkshop_profile());
                            i.putExtra("city",r.getCity());
                            i.putExtra("region",r.getRegion());
                            i.putExtra("rating",r.getRating());
                            i.putExtra("request_id",r.getId());
                            C.startActivity(i);
                        }

                    }
                });
            }else{
                holder.status.setText(C.getString(R.string.rated));
                holder.body.setOnClickListener(null);
            }
        }else if(r.getStatus().equals(RequestItem.REQUEST_NEW)){
            holder.status.setText("بانتظار التواصل من الورشة");
            holder.delete.setVisibility(View.VISIBLE);
            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(C);
                    View view = C.getLayoutInflater().inflate(R.layout.dialog_remove_order,null);
                    ArabTextView ok = view.findViewById(R.id.ok);
                    ArabTextView cancel = view.findViewById(R.id.cancel);
                    builder.setView(view);
                    AlertDialog dialog = builder.create();
                    cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.cancel();
                        }
                    });
                    ok.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ProgressDialog progressDialog = new ProgressDialog(C);
                            progressDialog.setMessage("الرجاء الانتظار..");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            BackgroundServices.getInstance(C).setBaseUrl(APIUrl.SERVER+"requests/update")
                                    .addPostParam("request_id" ,r.getId())
                                    .addPostParam("status" ,"2")
                                    .CallPost(new PostAction() {
                                        @Override
                                        public void whenFinished(String status, String response) throws JSONException {
                                            progressDialog.cancel();
                                            if (status.equals("200")){
                                                Toast.makeText(C, "تم حذف الطلب بنجاح", Toast.LENGTH_SHORT).show();
                                                data.remove(position);
                                                RequestsAdapterForCustomer.this.notifyDataSetChanged();
                                            }else {
                                                Toast.makeText(C, "حدث خطأ ما", Toast.LENGTH_SHORT).show();
                                            }

                                            dialog.cancel();
                                        }
                                    });
                        }
                    });
                    dialog.show();
                    /*
                    AlertDialog myQuittingDialogBox =new AlertDialog.Builder(C)
                            //set message, title, and icon
                            .setTitle(R.string.cancel_request)
                            .setMessage(C.getString(R.string.are_u_sure_to_cancel_request))
                            .setPositiveButton(C.getString(R.string.yes), new DialogInterface.OnClickListener() {

                                public void onClick(final DialogInterface dialog, int whichButton) {
                                    BackgroundServices.getInstance(C).setBaseUrl(APIUrl.SERVER+"requests/update")
                                            .addPostParam("request_id" ,r.getId())
                                            .addPostParam("status" ,"2")
                                            .CallPost(new PostAction() {
                                                @Override
                                                public void whenFinished(String status, String response) throws JSONException {
                                                    if (status.equals("200")){
                                                        Toast.makeText(C, "تم حذف الطلب بنجاح", Toast.LENGTH_SHORT).show();
                                                        data.remove(position);
                                                        RequestsAdapterForCustomer.this.notifyDataSetChanged();
                                                    }else {
                                                        Toast.makeText(C, "حدث خطأ ما", Toast.LENGTH_SHORT).show();
                                                    }

                                                    dialog.dismiss();
                                                }
                                            });


                                }

                            })



                            .setNegativeButton(C.getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                }
                            })
                            .create();
                    myQuittingDialogBox.show();
                     */
                }
            });
            holder.body.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(C,RequestDetails.class);
                    i.putExtra("id",r.getId());
                    i.putExtra("show",false);
                    C.startActivity(i);
                }
            });
        }else {
            holder.body.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(C,RequestDetails.class);
                    i.putExtra("id",r.getId());
                    i.putExtra("show",false);
                    C.startActivity(i);
                }
            });
            holder.delete.setVisibility(View.GONE);
        }

        holder.date.setText( r.getDateString());





    }
    String m_Text = "";

void confirm(final String id ,final int position){
    AlertDialog.Builder builder = new AlertDialog.Builder(C);
    builder.setTitle(C.getString(R.string.cancel_aprove));
    builder.setMessage(C.getString(R.string.question));

// Set up the input
    final EditText input = new EditText(C);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
    input.setInputType(InputType.TYPE_CLASS_NUMBER);
    builder.setView(input);

// Set up the buttons
    builder.setPositiveButton(C.getString(R.string.ok), new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            m_Text = input.getText().toString();
            if(m_Text.equals("27")){
                //ok
                BackgroundServices.getInstance(C).setBaseUrl(APIUrl.SERVER+"requests/update")
                        .addPostParam("request_id" ,id)
                        .addPostParam("status" ,"2")
                        .CallPost(new PostAction() {
                            @Override
                            public void whenFinished(String status, String response) throws JSONException {
//                                int maxLogSize = 1000;
//                                for(int i = 0; i <= response.length() / maxLogSize; i++) {
//                                    int start = i * maxLogSize;
//                                    int end = (i+1) * maxLogSize;
//                                    end = end > response.length() ? response.length() : end;
//                                    Log.d("SAXAFON", response.substring(start, end));
//                                }
                            }
                        });
                data.remove(position);
                RequestsAdapterForCustomer.this.notifyDataSetChanged();


            }else{
                Toast.makeText(C, C.getString(R.string.ignored), Toast.LENGTH_SHORT).show();
            }
        }
    });
    builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.cancel();
        }
    });

    builder.show();
}

    @Override
    public int getItemCount() {
        return data.size();
    }
}