package com.nyx.warshat.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import com.nyx.warshat.Intrafaces.ISelected;
import com.nyx.warshat.R;
import com.nyx.warshat.models.RegionObject;
import com.nyx.warshat.models.SelectedRegion;

import java.util.List;

import Arabs.ArabCheckbox;

public class RegionsAdapter extends RecyclerView.Adapter<RegionsAdapter.ViewHolder> {
    private Context context;
    private List<RegionObject> list;
    private ISelected iSelected;
    private List<SelectedRegion> selected;

    public RegionsAdapter(Context context, List<RegionObject> list, List<SelectedRegion> selected, ISelected iSelected) {
        this.context = context;
        this.list = list;
        this.iSelected = iSelected;
        this.selected = selected;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ArabCheckbox chk_check;

        public ViewHolder(View view) {
            super(view);
            chk_check = view.findViewById(R.id.item_check);
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_region, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        final RegionObject ro = list.get(position);
        holder.chk_check.setText(ro.getName());
        boolean found = false;
        if (selected.size()>0){
            for (SelectedRegion sr : selected){
                if (sr.getId() == ro.getId()){
                    found = true;
                    break;
                }
            }
            if (found){
                holder.chk_check.setChecked(true);
            }
        }
        final boolean finalFound = found;
        holder.chk_check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    if (!finalFound){
                        iSelected.checked(ro.getId());
                    }
                }else {
                    iSelected.not_checked(ro.getId());
                }
            }
        });
    }
}
