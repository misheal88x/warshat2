package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.app.Activity;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nyx.warshat.R;
import com.nyx.warshat.models.SelectedDate;

import java.util.List;


public class DatesAdapter
        extends RecyclerView.Adapter {

        private List data;
    private Activity C;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView date ,period;
        public MyViewHolder(View view) {
            super(view);
            date = view.findViewById(R.id.date);
            period = view.findViewById(R.id.period);
        }
        public  SelectedDate r;
        public int position;
    }


    public DatesAdapter(List moviesList , Activity c) {
        this.data = moviesList;
        this.C = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_date, parent, false);
final MyViewHolder holder =new MyViewHolder(itemView);


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, final int position) {
        final SelectedDate r = (SelectedDate)data.get(position);
      final  MyViewHolder holder = (MyViewHolder)h;
      holder.r = r;
      holder.position = position;

        holder.date.setText(r.getDate().replace("-","/"));
        holder.period.setText(r.getPeriod().replace(",","\n"));



    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}