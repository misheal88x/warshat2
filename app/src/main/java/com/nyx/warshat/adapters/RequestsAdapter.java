package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.app.Activity;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.details.RequestDetails;
import com.nyx.warshat.models.RequestItem;

import java.util.List;


public class RequestsAdapter
        extends RecyclerView.Adapter {

        private List data;
    private Activity C;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView id,date ,status;
        public View body;
        public ImageView pic;
        public MyViewHolder(View view) {
            super(view);
            id = (TextView) view.findViewById(R.id.id);
            status = (TextView) view.findViewById(R.id.status);
            date = (TextView) view.findViewById(R.id.date);
            body = view.findViewById(R.id.body);
            pic = view.findViewById(R.id.pic);
        }
    }


    public RequestsAdapter(List moviesList , Activity c) {
        this.data = moviesList;
        this.C = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_request_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, int position) {
        final RequestItem r = (RequestItem)data.get(position);

      final  MyViewHolder holder = (MyViewHolder)h;
        holder.id.setText("000"+r.getId());
        holder.status.setText(r.getStatusString(C));
        if (r.isRated()){
            holder.status.setText("تم التقييم من الزبون");
        }
        holder.date.setText( r.getDateString());
        Glide.with(C).load(r.getPic())
                .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big))
                .into(holder.pic);
        holder.body.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(C , RequestDetails.class);
                i.putExtra("id" ,r.getId());
                i.putExtra("show",true);
                //i.putExtra("city",r.getCity());
                //i.putExtra("region",r.getRegion());
                C.startActivity(i);
            }
        });

    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}