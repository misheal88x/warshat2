package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nyx.warshat.R;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.Image;

import org.json.JSONException;

import java.util.List;


public class GalleryAdapter
        extends RecyclerView.Adapter {

        private List data;
    private Activity C;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public View del_btn;
        public ImageView pic;
        public MyViewHolder(View view) {
            super(view);
            del_btn = view.findViewById(R.id.btn);
            pic = view.findViewById(R.id.pic);
        }
    }

boolean editable=true;
    public GalleryAdapter(List moviesList , Activity c ,boolean isE) {
        this.data = moviesList;
        this.C = c;
        this.editable=isE;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_editable_pic_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, final int position) {
        final Image r = (Image)data.get(position);

      final  MyViewHolder holder = (MyViewHolder)h;

        Glide.with(C).load(r.getPath())
                .apply(RequestOptions.placeholderOf(R.drawable.placeholder))
                .into(holder.pic);
        if(editable)
        holder.del_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
            //delete logic here
                v.setEnabled(false);
                if(!r.getId().trim().equals("")) {
                    BackgroundServices.getInstance(C).setBaseUrl(APIUrl.SERVER + "gallery/delete")
                            .addPostParam("id", r.getId())
                            .CallPost(new PostAction() {
                                @Override
                                public void whenFinished(String status, String response) throws JSONException {
                                    data.remove(position);
                                    GalleryAdapter.this.notifyDataSetChanged();
                                }
                            });
                }else{
                    holder.del_btn.setVisibility(View.GONE);
                }
            }
        });
        else
            holder.del_btn.setVisibility(View.GONE);


    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}