package com.nyx.warshat.adapters;

/**
 * Created by Luminance on 2/24/2018.
 */


import android.app.Activity;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.nyx.warshat.R;
import com.nyx.warshat.models.SelectableRegion;

import java.util.List;


public class SelectedRegionsAdapter
        extends RecyclerView.Adapter {

        private List data;
    private Activity C;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView text;
        public CheckBox c1 ,c2;
        public MyViewHolder(View view) {
            super(view);
            text = view.findViewById(R.id.name);
            c1 = view.findViewById(R.id.selected);
            c2 = view.findViewById(R.id.emergency);
        }
    }


    public SelectedRegionsAdapter(List moviesList , Activity c) {
        this.data = moviesList;
        this.C = c;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.single_selectable_region, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder h, final int position) {
        final SelectableRegion r = (SelectableRegion)data.get(position);

      final  MyViewHolder holder = (MyViewHolder)h;

        holder.text.setText(r.getName());
        holder.c1.setChecked(r.isSelected());
        holder.c2.setChecked(r.isEmergency());

        holder.c1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                r.setSelected(isChecked);
            }
        });
        holder.c2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                r.setEmergency(isChecked);

            }
        });



    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}