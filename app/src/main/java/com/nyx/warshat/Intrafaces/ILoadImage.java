package com.nyx.warshat.Intrafaces;

public interface ILoadImage {
    void onLoaded();
    void onFailed();
}
