package com.nyx.warshat.Intrafaces;

public interface IResponse {
    void onResponse();
    void onNotActivated();
    void onResponse(Object json);
}
