package com.nyx.warshat.models;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("message") private String message = "";
    @SerializedName("data") private Object data = null;
    @SerializedName("status") private int status = 0;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
