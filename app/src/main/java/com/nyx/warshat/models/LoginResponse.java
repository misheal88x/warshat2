package com.nyx.warshat.models;

import com.google.gson.annotations.SerializedName;

public class LoginResponse {
    @SerializedName("token") private String token = "";
    @SerializedName("user_type") private int user_type = 0;

    public int getUser_type() {
        return user_type;
    }

    public void setUser_type(int user_type) {
        this.user_type = user_type;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
