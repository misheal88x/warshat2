package com.nyx.warshat.models;

import com.google.gson.annotations.SerializedName;

public class RegionObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("city_id") private int city_id = 0;

    public RegionObject(int id, String name, int city_id) {
        this.id = id;
        this.name = name;
        this.city_id = city_id;
    }

    public RegionObject() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }
}
