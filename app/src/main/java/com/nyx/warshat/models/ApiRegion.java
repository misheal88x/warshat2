package com.nyx.warshat.models;

import java.io.Serializable;

public class ApiRegion implements Serializable {
   private String id ,e;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public ApiRegion(String id, String e) {
        this.id = id;
        this.e = e;
    }
}
