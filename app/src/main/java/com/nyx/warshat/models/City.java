package com.nyx.warshat.models;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

public class City implements Serializable {
    private String id, name;
    private ArrayList<Region> regions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Region> getRegions() {
        return regions;
    }

    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }

    public City(JSONObject d) throws JSONException {
        this.id = d.getString("id");
        this.name = d.getString("name");
        regions = new ArrayList<>();
        JSONArray arr = d.getJSONArray("regions");
        for (int i = 0; i < arr.length(); i++)
            this.regions.add(new Region(arr.getJSONObject(i).getString("id"),
                    arr.getJSONObject(i).getString("name")));
    }
}
