package com.nyx.warshat.models;

import android.graphics.Bitmap;

public class ProblemImageObject {
    private Bitmap bitmap;
    private String url = "";
    private int type;

    public ProblemImageObject(Bitmap bitmap, int type) {
        this.bitmap = bitmap;
        this.type = type;
    }

    public ProblemImageObject() {
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
