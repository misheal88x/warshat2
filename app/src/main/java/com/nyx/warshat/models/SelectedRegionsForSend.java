package com.nyx.warshat.models;

import com.google.gson.annotations.SerializedName;

public class SelectedRegionsForSend {
    @SerializedName("id") int id = 0;
    @SerializedName("e") int e = 0;

    public SelectedRegionsForSend() { }

    public SelectedRegionsForSend(int id, int e) {
        this.id = id;
        this.e = e;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getE() {
        return e;
    }

    public void setE(int e) {
        this.e = e;
    }
}
