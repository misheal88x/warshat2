package com.nyx.warshat.models;

import android.content.Context;

import com.nyx.warshat.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class RequestItem implements Serializable {

   public static String REQUEST_NEW="-1";
   public static String REQUEST_STARTED="1";
   public static String REQUEST_FINISHED="3";
   public static String REQUEST_CANCELLED="2";

    private String id  ,date ,status ,pic ,workshop ,workshop_profile ,to,city,region;
    private String rating1,rating2,rating3,rating4;
private boolean isRated;

    public String getWorkshop_profile() {
        return workshop_profile;
    }

    public String getTo() {
        return to;
    }

    public RequestItem(JSONObject j) throws JSONException {
        this.id = j.getString("id");
        this.date = j.getString("created_at");;
        this.status = j.getString("status");;
        this.pic = j.getString("request_pic");;
        this.workshop = j.getString("workshop_name");;
        this.workshop_profile = j.getString("workshop_profile_pic");;
        this.to = j.getString("to_id");
        this.isRated = j.getBoolean("isRated");
        this.city = j.getString("workshop_city_name");
        this.region = j.getString("workshop_region_name");
        this.rating1 = j.getJSONObject("workshop_ratings").getString("rating1");
        this.rating2 = j.getJSONObject("workshop_ratings").getString("rating2");
        this.rating3 = j.getJSONObject("workshop_ratings").getString("rating3");
        this.rating4 = j.getJSONObject("workshop_ratings").getString("rating4");
    }

    public boolean isRated() {
        return isRated;
    }

    public String getWorkshop() {
        return workshop;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }
    public String getDateString() {
        return date.split(" ")[0];
    }
    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }
    public String getStatusString(Context c) {
        if(status.equals("-1"))return c.getString(R.string.new_string);
        if(status.equals("1"))return c.getString(R.string.started);
        if(status.equals("2"))return c.getString(R.string.cancelled);
        if(status.equals("3"))return c.getString(R.string.finished);
        return "?";
    }
    public void setStatus(String status) {
        this.status = status;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Float getRating(){
        Float r1 = 0f;
        Float r2 = 0f;
        Float r3 = 0f;
        Float r4 = 0f;
        if (rating1!=null&&!rating1.equals("null")){
            if (!rating1.equals("")){
                r1 = Float.valueOf(rating1);
            }
        }
        if (rating2!=null&&!rating2.equals("null")){
            if (!rating2.equals("")){
                r2 = Float.valueOf(rating2);
            }
        }
        if (rating3!=null&&!rating3.equals("null")){
            if (!rating3.equals("")){
                r3 = Float.valueOf(rating3);
            }
        }
        if (rating4!=null&&!rating4.equals("null")){
            if (!rating4.equals("")){
                r4 = Float.valueOf(rating4);
            }
        }
        float total_rate = (r1+r2+r3+r4)/4;
        float formatted_rate = Float.valueOf(String.valueOf(Math.round(total_rate)));
        return formatted_rate;
    }
}
