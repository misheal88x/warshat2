package com.nyx.warshat.models;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

public class Image {
    private String id ,path;
    public Image(JSONObject j) throws JSONException {
        this.id = j.getString("id");
        this.path = j.getString("pic_url");;
    }
    public Image(String id, String path) {
        this.id = id;
        this.path = path;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
