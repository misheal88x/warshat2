package com.nyx.warshat.models;

import com.google.gson.JsonObject;
import com.google.gson.annotations.SerializedName;

import org.json.JSONException;
import org.json.JSONObject;

public class Workshop {
    private String id ,name ,address ,pic,type ;
    private double rating  ;
    @SerializedName("response_time_rating") private double response_time_rating = 0;

    public Workshop(JSONObject j) throws JSONException {
        this.id = j.getString("id");
        this.name = j.getString("work_name");;
        this.address = j.getString("address");;
        this.pic = j.getString("profile_pic");;
        this.type = j.getString("user_type");;
       double s = j.getDouble("response_time_rating")+
       j.getDouble("performance_rating")
        +j.getDouble("quality_rating")
      + j.getDouble("support_rating");;
      this.rating = s/4;
      this.response_time_rating = j.getDouble("response_time_rating");
    }

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPic() {
        return pic;
    }

    public double getRating() {
        return rating;
    }

    public double getResponse_time_rating() {
        return response_time_rating;
    }

    public void setResponse_time_rating(double response_time_rating) {
        this.response_time_rating = response_time_rating;
    }
}
