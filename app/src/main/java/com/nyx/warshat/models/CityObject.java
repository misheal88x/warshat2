package com.nyx.warshat.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CityObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("regions") private List<RegionObject> regions = new ArrayList<>();

    public CityObject(int id, String name, List<RegionObject> regions) {
        this.id = id;
        this.name = name;
        this.regions = regions;
    }

    public CityObject() { }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<RegionObject> getRegions() {
        return regions;
    }

    public void setRegions(List<RegionObject> regions) {
        this.regions = regions;
    }
}
