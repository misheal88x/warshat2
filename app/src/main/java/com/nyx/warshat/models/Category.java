package com.nyx.warshat.models;

import org.json.JSONException;
import org.json.JSONObject;

public class Category {
   public static String TYPE_WORKSHOP_MAIN="2";
   public static String TYPE_WORKSHOP_FIX="1";
   public static String TYPE_WORKSHOP_BOTH="3";
   public static String TYPE_COMPANY="5";


    private String id ,name ,pic ,type;

    public Category(JSONObject j) throws JSONException {
        this.id = j.getString("id");
        this.name = j.getString("name");
        this.pic = j.getString("cat_pic");
        this.type = j.getString("target_type");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
