package com.nyx.warshat.models;

public class SelectableRegion extends Region {
    private boolean selected ,emergency;

    public SelectableRegion(String id, String name, boolean selected, boolean emergency) {
        super(id, name);
        this.selected = selected;
        this.emergency = emergency;
    }

    @Override
    public String toString() {
        return "SelectableRegion{" +
                "selected=" + selected +
                ", emergency=" + emergency +
                '}';
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isEmergency() {
        return emergency;
    }

    public void setEmergency(boolean emergency) {
        this.emergency = emergency;
    }
}
