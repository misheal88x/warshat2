package com.nyx.warshat.models;

public class SelectedRegion {
    private int id = 0;

    public SelectedRegion(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
