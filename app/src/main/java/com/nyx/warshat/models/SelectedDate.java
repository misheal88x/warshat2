package com.nyx.warshat.models;



public class SelectedDate{
    private  String date ,period;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public SelectedDate(String date, String period) {
        this.date = date;
        this.period = period;
    }
}
