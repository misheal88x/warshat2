package com.nyx.warshat.models;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class ProductSelector implements Serializable {
    private String id ,name;
    private boolean isChecked;

    public ProductSelector(JSONObject j) throws JSONException {
        this.id = j.getString("id");
        this.name = j.getString("title");
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }
}
