package com.nyx.warshat.models;

import com.google.gson.annotations.SerializedName;

public class TempUserObject {
    @SerializedName("customer_login_type") private String customer_login_type = "";
    @SerializedName("customer_user_type") private String customer_user_type = "";
    @SerializedName("customer_name") private String customer_name = "";
    @SerializedName("customer_phone") private String customer_phone = "";
    @SerializedName("customer_password") private String customer_password = "";
    @SerializedName("customer_c_password") private String customer_c_password = "";
    @SerializedName("customer_email") private String customer_email = "";

    @SerializedName("work_full_name") private String work_full_name = "";
    @SerializedName("work_phone") private String work_phone = "";
    @SerializedName("work_email") private String work_email = "";
    @SerializedName("work_password") private String work_password = "";
    @SerializedName("work_w_full_name") private String work_w_full_name = "";
    @SerializedName("work_w_mobile") private String work_w_mobile = "";
    @SerializedName("work_w_land_line") private String work_w_land_line = "";
    @SerializedName("work_city_id") private String work_city_id = "";
    @SerializedName("work_region_id") private String work_region_id = "";
    @SerializedName("work_cat_id") private String work_cat_id = "";
    @SerializedName("work_desc") private String work_desc = "";
    @SerializedName("work_regions_json") private String work_regions_json = "";
    @SerializedName("work_urgent_regions_json") private String work_urgent_regions_json = "";
    @SerializedName("work_pic") private String work_pic = "";

    public String getCustomer_login_type() {
        return customer_login_type;
    }

    public void setCustomer_login_type(String customer_login_type) {
        this.customer_login_type = customer_login_type;
    }

    public String getCustomer_user_type() {
        return customer_user_type;
    }

    public void setCustomer_user_type(String customer_user_type) {
        this.customer_user_type = customer_user_type;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getCustomer_phone() {
        return customer_phone;
    }

    public void setCustomer_phone(String customer_phone) {
        this.customer_phone = customer_phone;
    }

    public String getCustomer_password() {
        return customer_password;
    }

    public void setCustomer_password(String customer_password) {
        this.customer_password = customer_password;
    }

    public String getCustomer_c_password() {
        return customer_c_password;
    }

    public void setCustomer_c_password(String customer_c_password) {
        this.customer_c_password = customer_c_password;
    }

    public String getCustomer_email() {
        return customer_email;
    }

    public void setCustomer_email(String customer_email) {
        this.customer_email = customer_email;
    }

    public String getWork_full_name() {
        return work_full_name;
    }

    public void setWork_full_name(String work_full_name) {
        this.work_full_name = work_full_name;
    }

    public String getWork_phone() {
        return work_phone;
    }

    public void setWork_phone(String work_phone) {
        this.work_phone = work_phone;
    }

    public String getWork_email() {
        return work_email;
    }

    public void setWork_email(String work_email) {
        this.work_email = work_email;
    }

    public String getWork_password() {
        return work_password;
    }

    public void setWork_password(String work_password) {
        this.work_password = work_password;
    }

    public String getWork_w_full_name() {
        return work_w_full_name;
    }

    public void setWork_w_full_name(String work_w_full_name) {
        this.work_w_full_name = work_w_full_name;
    }

    public String getWork_w_mobile() {
        return work_w_mobile;
    }

    public void setWork_w_mobile(String work_w_mobile) {
        this.work_w_mobile = work_w_mobile;
    }

    public String getWork_w_land_line() {
        return work_w_land_line;
    }

    public void setWork_w_land_line(String work_w_land_line) {
        this.work_w_land_line = work_w_land_line;
    }

    public String getWork_city_id() {
        return work_city_id;
    }

    public void setWork_city_id(String work_city_id) {
        this.work_city_id = work_city_id;
    }

    public String getWork_region_id() {
        return work_region_id;
    }

    public void setWork_region_id(String work_region_id) {
        this.work_region_id = work_region_id;
    }

    public String getWork_cat_id() {
        return work_cat_id;
    }

    public void setWork_cat_id(String work_cat_id) {
        this.work_cat_id = work_cat_id;
    }

    public String getWork_desc() {
        return work_desc;
    }

    public void setWork_desc(String work_desc) {
        this.work_desc = work_desc;
    }

    public String getWork_regions_json() {
        return work_regions_json;
    }

    public void setWork_regions_json(String work_regions_json) {
        this.work_regions_json = work_regions_json;
    }

    public String getWork_urgent_regions_json() {
        return work_urgent_regions_json;
    }

    public void setWork_urgent_regions_json(String work_urgent_regions_json) {
        this.work_urgent_regions_json = work_urgent_regions_json;
    }

    public String getWork_pic() {
        return work_pic;
    }

    public void setWork_pic(String work_pic) {
        this.work_pic = work_pic;
    }
}
