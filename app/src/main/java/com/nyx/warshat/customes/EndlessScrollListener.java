package com.nyx.warshat.customes;

/**
 * Created by Luminance on 5/17/2018.
 */

public interface EndlessScrollListener {
    void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy);
}