package com.nyx.warshat.utilities;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.nyx.warshat.Intrafaces.ILoadImage;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.R;
import com.nyx.warshat.adapters.CustomSpinnerAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.BaseResponse;

import org.json.JSONException;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class BaseFunctions {
    public static String dateExtractor(String datetime){
        String myDate = "";
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!=' '){
                myDate+=datetime.charAt(i);
            }else {
                break;
            }
        }
        return myDate;
    }

    public static String timeExtractor(String datetime){
        String myTime = "";
        boolean isTime = false;
        for (int i = 0; i <datetime.length() ; i++) {
            if (datetime.charAt(i)!=' '&&isTime){
                myTime+=datetime.charAt(i);
            }else if (datetime.charAt(i)==' '){
                isTime = true;
            }
        }
        return myTime;
    }

    public static String formatSeconds(long totalSecs) {
        long hours = (int) ((totalSecs) / 3600);
        long minutes = (int) ((totalSecs % 3600) / 60);
        long seconds = (int) (totalSecs % 60);
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public static String languageToNumberConverter(String in){
        String out = "";
        if (in.equals("en")){
            out = "1";
        }else {
            out = "2";
        }
        return out;
    }

    public static String numberToLanguageConverter(String in){
        String out = "";
        if (in.equals("1")){
            out = "en";
        }else {
            out = "ar";
        }
        return out;
    }

    public static void showNotification(Context context,
                                        PendingIntent pendingIntent,
                                        String message,
                                        NotificationCompat.Builder builder,
                                        NotificationManager notificationManager,
                                        String NOTIFICATION_CHANNEL_ID,
                                        int MY_NOTIFICATION_ID){
        if (pendingIntent!=null){
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pendingIntent)
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo)
                    .setVibrate(new long[]{500, 500, 500});
        }else {
            builder = new NotificationCompat.Builder(context)
                    .setContentTitle(context.getResources().getString(R.string.app_name))
                    .setContentText(message)
                    .setStyle(new NotificationCompat.BigTextStyle()
                            .bigText(message))
                    .setTicker("new notification")
                    .setWhen(System.currentTimeMillis())
                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setAutoCancel(true)
                    .setSmallIcon(R.drawable.logo)
                    .setVibrate(new long[]{500, 500, 500});
        }
        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        //notificationManager.notify(MY_NOTIFICATION_ID, myNotification);
        //Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNAEL", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.BLUE);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{500, 500, 500});
            assert notificationManager != null;
            builder.setChannelId(NOTIFICATION_CHANNEL_ID);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        assert notificationManager != null;
        notificationManager.notify(MY_NOTIFICATION_ID, builder.build());
    }

    public static void callUpdateTokenAPI(final Context context, String token){
        BackgroundServices.getInstance(context).setBaseUrl(APIUrl.SERVER+"fcm/update")
                .addPostParam("fcm_token" ,token)
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {

                    }
                });
    }

    public static String getDeviceId(Context context){
        String android_id = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return android_id;
    }

    public static void setGlideImage(Context context, ImageView image, String url){
        try{
            RequestOptions requestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .circleCrop();
            Glide.with(context).load(url)
                    .apply(requestOptions)
                    .into(image);
        }catch (Exception e){}
    }
    public static void setGlideImageWithEvents(Context context, ImageView image, String url, final ILoadImage iLoadImage){
        try{
            RequestOptions requestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .circleCrop();
            Glide.with(context).load(url)
                    .apply(requestOptions)
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model,
                                                    Target<Drawable> target, boolean isFirstResource) {
                            iLoadImage.onFailed();
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target,
                                                       DataSource dataSource, boolean isFirstResource) {
                            iLoadImage.onLoaded();
                            return false;
                        }
                    })
                    .into(image);
        }catch (Exception e){}
    }
    public static void setGlideDrawableImage(Context context, ImageView image, int drawable){
        try{
            RequestOptions requestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .circleCrop();
            Glide.with(context).load(drawable)
                    .apply(requestOptions)
                    .into(image);
        }catch (Exception e){}
    }
    public static void setGlideFileImage(Context context, ImageView image, File file){
        try{
            RequestOptions requestOptions = new RequestOptions()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .circleCrop();
            Glide.with(context).load(file)
                    .apply(requestOptions)
                    .into(image);
        }catch (Exception e){}
    }

    public static void processResponse(Response<BaseResponse> response, IResponse onResponse, Context context){
        if (response!=null){
            if (response.body()!=null){
                if (response.body().getStatus() == 200){
                    onResponse.onResponse(response.body().getData());
                }else if (response.body().getStatus() == 403){
                    onResponse.onNotActivated();
                } else {
                    onResponse.onResponse();
                }
            }else {
                if (response.code() == 401){
                    Toast.makeText(context, "رقم الجوال موجود مسبقا", Toast.LENGTH_SHORT).show();
                    onResponse.onResponse();
                }else if (response.code() == 402){
                    Toast.makeText(context, "البريد الإلكتروني موجود مسبقا", Toast.LENGTH_SHORT).show();
                    onResponse.onResponse();
                }else {
                    Toast.makeText(context, "يوجد خطأ في الخدمة, الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                    onResponse.onResponse();
                }
            }
        }else {
            Toast.makeText(context, "يوجد خطأ في الخدمة, الرجاء إعادة المحاولة!", Toast.LENGTH_SHORT).show();
            onResponse.onResponse();
        }
    }
    public static Date stringToDateConverter(String stringDate){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date d = sdf.parse(stringDate);
            return d;
        } catch (ParseException ex) {
            Log.v("Exception", ex.getLocalizedMessage());
            return null;
        }
    }
    public static String dateToDayConverter(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        String dayOfTheWeek = sdf.format(date);
        return dayOfTheWeek;
    }

    public static String todayDateString(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        return formattedDate;
    }

    public static long deferenceBetweenTwoDates(Date d1,Date d2){
        return d1.getTime()-d2.getTime();
    }

    public static String processDate(Context context,String date){
        Date inputDate = stringToDateConverter(date);
        Date todayDate = stringToDateConverter(todayDateString());
        long i = deferenceBetweenTwoDates(todayDate,inputDate);
        long[] res = processMilliseconds(i);
        if (res[0] != 0){
            return String.valueOf(res[0])+" أيام";
        }else if (res[1] != 0){
            return String.valueOf(res[1])+" ساعات";
        }else if (res[2] != 0){
            return String.valueOf(res[2])+" دقائق";
        }else {
            return "منذ لحظات";
        }
    }

    public static long[] processMilliseconds(long milli){
        long[] out = new long[]{0,0,0};

        long seconds = milli / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;

        long day = days;
        //long day = TimeUnit.MILLISECONDS.toDays(milli);

        //long hours = TimeUnit.MILLISECONDS.toHours(milli)
        //- TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(milli));

        //long minutes = TimeUnit.MILLISECONDS.toMinutes(milli)
        //- TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(milli));
        out[0] = day;
        out[1] = hours%24;
        out[2] = minutes%60;
        return out;
    }

    public static String subString(String input,int start,int end){
        return input.substring(start,end);
    }

    public static String booleanToStringNumber(boolean in){
        String out = "";
        if (in){
            out = "1";
        }else {
            out = "0";
        }
        return out;
    }
    public static boolean stringNumberToBoolean(String in){
        boolean out = false;
        if (in.equals("1")){
            out = true;
        }else {
            out = false;
        }
        return out;
    }

    public static int getSpinnerPosition(AdapterView<?> parent, int i){
        long pos = parent.getItemIdAtPosition(i);
        int position = Integer.valueOf(String.valueOf(pos));
        return position;
    }

    public static void openBrowser(Context context,String url){
        Intent i = new Intent(Intent.ACTION_VIEW,
                Uri.parse(url));
        context.startActivity(i);
    }
    public static void openDialer(Context context,String phone_number){
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phone_number));
        context.startActivity(intent);
    }
    public static void openEmail(Context context,String email){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        String[] addresses = new String[]{email};
        intent.putExtra(Intent.EXTRA_EMAIL, addresses);
        intent.putExtra(Intent.EXTRA_SUBJECT, "Sawwah");
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static void openFacebook(Context context,String url){
        context.startActivity(newFacebookIntent(context.getPackageManager(),url));
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public static void openTwitter(Context context,String url){
        Intent intent = null;
        try {
            context.getPackageManager().getPackageInfo("com.twitter.android", 0);
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        } catch (PackageManager.NameNotFoundException e) {
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            context.startActivity(intent);
        }
    }
    public static void sendMessageBySMS(Context context,String message,String errorMessage){
        try {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse("sms:"));
            sendIntent.putExtra("sms_body", message);
            context.startActivity(sendIntent);
        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void sendMessageByMessenger(Context context,String message,String errorMessage){
        try{
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, message);
            sendIntent.setType("text/plain");
            sendIntent.setPackage("com.facebook.orca");
            context.startActivity(sendIntent);
        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void sendMessageByWhatsapp(Context context,String message,String errorMessage){
        try{
            PackageManager pm = context.getPackageManager();
            Intent waIntent = new Intent(Intent.ACTION_SEND);
            waIntent.setType("text/plain");
            String text = message;
            PackageInfo info=pm.getPackageInfo("com.whatsapp", PackageManager.GET_META_DATA);
            waIntent.setPackage("com.whatsapp");
            waIntent.putExtra(Intent.EXTRA_TEXT, text);
            context.startActivity(Intent.createChooser(waIntent, "Share with"));

        }catch (Exception e){
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }
    }

    public static void playMusic(Context context,int music){
        MediaPlayer mPlayer = MediaPlayer.create(context, music);
        mPlayer.start();
    }
    public void stopPlayMusic(MediaPlayer mediaPlayer){
        mediaPlayer.stop();
    }

    public static MultipartBody.Part uploadFileImageConverter(String imageFieldName, String imagePath){
        File file = new File(imagePath);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"),file);
        MultipartBody.Part body = MultipartBody.Part.createFormData(imageFieldName,file.getName(),requestBody);
        return body;
    }

    public static RequestBody uploadFileStringConverter(String input){
        return RequestBody.create(MultipartBody.FORM,input);
    }

    public static void init_spinner(Context context , Spinner spinner , CustomSpinnerAdapter adapter, List<String> list){
        adapter = new CustomSpinnerAdapter(context,android.R.layout.simple_list_item_1,list);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

}
