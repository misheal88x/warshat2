package com.nyx.warshat.utilities;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;

import com.nyx.warshat.R;


/**
 * Created by Luminance on 1/5/2018.
 */

public abstract class SelectHourPopUp extends Dialog{

    public Activity c;
    public Dialog d;
    public Button yes, no;
    public  String id;
    public CheckBox[] boxes;

    public SelectHourPopUp(Activity a ,String ad) {
        super(a);
        // TODO Auto-generated constructor stub
        this.c = a;
        this.id=ad;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_dialog);
        boxes = new CheckBox[]{
                (CheckBox) findViewById(R.id.c1),
                (CheckBox) findViewById(R.id.c2),
                (CheckBox) findViewById(R.id.c3),
                (CheckBox) findViewById(R.id.c4),
                (CheckBox) findViewById(R.id.c5),
        };
        yes = (Button) findViewById(R.id.btn_yes);
        no = (Button) findViewById(R.id.btn_no);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String options = "";
                for(int i=0;i<boxes.length;i++)
                    if(boxes[i].isChecked())options+=boxes[i].getText()+" ,";
                yesClicked(options);
                dismiss();
            }
        });
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


    }

   public abstract void yesClicked(String optiona);

}
