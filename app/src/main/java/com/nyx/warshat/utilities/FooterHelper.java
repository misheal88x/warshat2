package com.nyx.warshat.utilities;

import android.app.Activity;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.homes.AboutActivity;
import com.nyx.warshat.activities.homes.CustomerHome;
import com.nyx.warshat.activities.infinites.CustomerRequests;
import com.nyx.warshat.activities.edits.ProfileActivity;
import com.nyx.warshat.helper.ConnectionUtils;

public class FooterHelper {
    public static void initFooterForCustomer(final Activity parent){
        parent.findViewById(R.id.profile_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(parent)){
                    Toast.makeText(parent, "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
                    return;
                }
                parent.startActivity(new Intent(parent , ProfileActivity.class));
                parent.finish();

            }
        });
        parent.findViewById(R.id.my_requests_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(parent)){
                    Toast.makeText(parent, "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent i = new Intent(parent,CustomerRequests.class);
                i.putExtra("is_from_notifications",false);
                parent.startActivity(i);
                parent.finish();

            }
        });
        parent.findViewById(R.id.home_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parent.startActivity(new Intent(parent , CustomerHome.class));
                parent.finish();
            }
        });


        parent.findViewById(R.id.new_request_layout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(parent)){
                    Toast.makeText(parent, "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent i=new Intent(parent , AboutActivity.class);
                i.putExtra("code" ,"ABOUT");
                i.putExtra("name" , R.string.about);
                parent.startActivity(i);
            }
        });
    }
}
