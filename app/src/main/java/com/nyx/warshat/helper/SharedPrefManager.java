package com.nyx.warshat.helper;

/**
 * Created by Luminance on 1/18/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;


import com.facebook.login.LoginManager;
import com.google.gson.Gson;
import com.nyx.warshat.models.TempUserObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class SharedPrefManager {

    private static SharedPrefManager mInstance;
    private static Context mCtx;
    private static final String SHARED_PREF_NAME = "warshat-repo";
    private static final String KEY_TOKEN = "TTK56";
    private static final String KEY_TYPE = "TTK57";
    private static final String KEY_PHONE = "TTK58";
    private static final String LANGUAGE_LOCALE = "T785";
    public static final String FIREBASE_TOKEN = "776yhg";
    public static final String ACCOUT_CONFIRMED = "ac5544";
    public static final String TEMP_ACCOUNT = "TA5544";
    public static final String ACCOUNT_TYPE = "AT5544";




    private SharedPrefManager(Context context) {
        mCtx = context;
    }
    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }
    public String getSavedLanguage()  {
        SharedPreferences sharedPreferences =
                mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String o = sharedPreferences.getString(LANGUAGE_LOCALE, "");
        if(!o.trim().equals("")){
            return  o.trim().toLowerCase();
        }else{
            try {
                saveData("ar",LANGUAGE_LOCALE);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return "ar";
        }
    }
    public boolean saveData(String data ,String key)  {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, data);
        editor.apply();
        return true;
    }
    public boolean saveToken(String token ,String user_type)  {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_TYPE, user_type);
        editor.apply();
        return true;
    }
    public void savePhoneNumber(String phone){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(KEY_PHONE,phone);
        editor.apply();
    }
    public String getPhoneNumber() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String o = sharedPreferences.getString(KEY_PHONE, null);
        return (o != null && !o.trim().equals(""))?o:"";

    }
    public boolean isLoggedIn() {
        return !getToken().equals("");
    }
    public boolean isCustomer() {
        return !getToken().equals("") && getType().equals("1");
    }
    public void logout() {
        saveToken("" ,"");
        savePhoneNumber("");
        LoginManager.getInstance().logOut();
    }
    public String getType() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String o = sharedPreferences.getString(KEY_TYPE, null);
        return (o != null && !o.trim().equals(""))?o:"";

    }
    public String getToken() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String o = sharedPreferences.getString(KEY_TOKEN, null);
        return (o != null && !o.trim().equals(""))?o:"";

    }

    public void setAccountConfirmed(boolean value){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(ACCOUT_CONFIRMED,value);
        editor.commit();
    }

    public boolean getAccountConfirmed() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        boolean value = sharedPreferences.getBoolean(ACCOUT_CONFIRMED, true);
        return value;
    }

    public void setAccountType(String type){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCOUNT_TYPE,type);
        editor.commit();
    }

    public String getAccountType() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(ACCOUNT_TYPE, "customer");
        return value;
    }

    public void setTempAccount(TempUserObject tempAccount){
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(TEMP_ACCOUNT,new Gson().toJson(tempAccount));
        editor.commit();
    }

    public TempUserObject getTempAccount() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        String value = sharedPreferences.getString(TEMP_ACCOUNT, "{}");
        if (value!=null&&!value.equals("")&&!value.equals("{}")){
            TempUserObject tuo = new Gson().fromJson(value,TempUserObject.class);
            return tuo;
        }else {
            return new TempUserObject();
        }
    }
}