package com.nyx.warshat.helper;

/**
 * Created by Luminance on 5/19/2018.
 */
import org.json.JSONException;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;



public class BackgroundServices {

    private static BackgroundServices mInstance;
    private static Context mCtx;
   private HashMap keys;
   private HashMap<String  ,ArrayList<File>> filesMap;
   static String url;
    private BackgroundServices(Activity context) {
        mCtx = context;
    }
    private BackgroundServices(Context context) {
        mCtx = context;
    }
    public static synchronized BackgroundServices getInstance(Activity context) {
        if (mInstance == null) {
            mInstance = new BackgroundServices(context);
        }
        mInstance.keys = new HashMap();
        mInstance.filesMap = new HashMap<>();
        url=APIUrl.SERVER;
        return mInstance;
    }
    public static synchronized BackgroundServices getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new BackgroundServices(context);
        }
        mInstance.keys = new HashMap();
        mInstance.filesMap = new HashMap<>();
        url=APIUrl.SERVER;
        return mInstance;
    }
    public BackgroundServices setBaseUrl(String u){
        url=u;
        return mInstance;
    }
    public BackgroundServices addFile(String fname , File file){
        if(filesMap.get(fname)==null) filesMap.put(fname ,new ArrayList<File>());
        this.filesMap.get(fname).add(file);
        return mInstance;
    }
    public BackgroundServices addPostParam(String key ,String value){
        keys.put(key ,value);
        return mInstance;
    }
    public void CallGet(final PostAction pa ) {
        CallInternal( url ,  null , pa,0 ,"get");
    }
    public void CallPost(final PostAction pa ) {
        CallInternal( url ,  this.keys , pa,0 ,"post");
    }
    private String status="-1";
    private void CallInternal(final String url , final HashMap<String, String> args , final PostAction pa, final int try_
     ,final String method) {

Log.d("PKHTTP3_PARAMS" ,keys.toString() + "\nFILES\n" + filesMap.toString());
        class CostumTask extends AsyncTask<String, Void, String> {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                if(s.equals("")) {
                    if(try_<100){
                      CallInternal(url ,args ,pa ,try_+1 ,method);
                    }else{
                    Toast.makeText(mCtx, "لا يوجد اتصال في الشبكة", Toast.LENGTH_SHORT).show();
                    try {
                        pa.whenFinished(status ,s);
                    } catch (JSONException e) {
                   Log.d("JSONPARSE" ,e.getMessage());
                    }}

                }else {
                    try {
                        pa.whenFinished(status ,s);
                    } catch (JSONException e) {
                        Log.d("JSONPARSE" ,e.getMessage());
                    }
                }
            }
            @Override
            protected String doInBackground(String... params) {
                String[] result=null;
                try {
                    if(filesMap.size()==0)
                    result =
                            ConnectionUtils.sendPostRequest(url,
                                    args, method ,
                                    SharedPrefManager.getInstance(mCtx).getToken());
                    else
                        result =   ConnectionUtils.sendPostRequestWithFile(url,
                                keys,SharedPrefManager.getInstance(mCtx).getToken() , filesMap
                                );

                  status = result[0] ;
                  return result[1];

                } catch (Exception e) {
                    Log.d("ERROR IN CALLBACK "+url+" : "  , e.getMessage());
                }

                return "";
            }
        }
        CostumTask ru = new CostumTask();
        ru.execute();
    }

}
