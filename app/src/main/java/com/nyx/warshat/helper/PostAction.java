package com.nyx.warshat.helper;

import org.json.JSONException;

/**
 * Created by Luminance on 1/29/2019.
 */

public abstract class PostAction {
    public  abstract void whenFinished(String status ,String response) throws JSONException;
}
