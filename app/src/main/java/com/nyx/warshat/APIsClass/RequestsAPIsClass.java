package com.nyx.warshat.APIsClass;

import android.content.Context;

import com.nyx.warshat.APIs.RequestsAPIs;
import com.nyx.warshat.Intrafaces.IFailure;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.models.BaseResponse;
import com.nyx.warshat.utilities.BaseFunctions;
import com.nyx.warshat.utilities.BaseRetrofit;

import java.io.File;
import java.util.List;
import java.util.concurrent.RecursiveTask;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Part;

public class RequestsAPIsClass extends BaseRetrofit {

    public static void addRequest(
            final Context context,
            String to_id,
            String lat,
            String lng,
            String req_phone,
            String dates,
            String problem_details,
            List<String> images,
            IResponse onResponse1,
            IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;
        MultipartBody.Part[] multipartTypedOutput = null;
        if (images.size()>0) {
            multipartTypedOutput = new MultipartBody.Part[images.size()];
            if (images.size() > 0) {
                for (int i = 0; i < images.size(); i++) {
                    File file = new File(images.get(i));
                    RequestBody requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    multipartTypedOutput[i] = MultipartBody.Part.createFormData("request_pic[]", file.getName(), requestBody);
                }
            }
        }

        //Name
        RequestBody to_idRequest = RequestBody.create(MultipartBody.FORM,to_id);
        //Name
        RequestBody latRequest = RequestBody.create(MultipartBody.FORM,lat);
        //Name
        RequestBody lngRequest = RequestBody.create(MultipartBody.FORM,lng);
        //Name
        RequestBody req_phoneRequest = RequestBody.create(MultipartBody.FORM,req_phone);
        //Name
        RequestBody datesRequest = RequestBody.create(MultipartBody.FORM,dates);
        //Name
        RequestBody problem_detailsRequest = RequestBody.create(MultipartBody.FORM,problem_details);

        Retrofit retrofit = configureRetrofitWithBearer(context);
        RequestsAPIs api = retrofit.create(RequestsAPIs.class);
        Call<BaseResponse> call = api.add_request(
                to_idRequest,
                latRequest,
                lngRequest,
                req_phoneRequest,
                datesRequest,
                problem_detailsRequest,
                images.size()>0?multipartTypedOutput:null);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                onFailure.onFailure();
            }
        });
    }
}
