package com.nyx.warshat.APIsClass;

import android.content.Context;

import com.nyx.warshat.APIs.AccountAPIs;
import com.nyx.warshat.Intrafaces.IFailure;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.models.BaseResponse;
import com.nyx.warshat.utilities.BaseFunctions;
import com.nyx.warshat.utilities.BaseRetrofit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class AccountAPIsClass extends BaseRetrofit {

    public static void getPrivacy(final Context context,
                                  IResponse onResponse1,
                                  IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        AccountAPIs api = retrofit.create(AccountAPIs.class);
        Call<BaseResponse> call = api.get_privacy();
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable throwable) {
                onFailure.onFailure();
            }
        });
    }

    public static void confirmAccount(final Context context,
                                  String phone,
                                  String code,
                                  IResponse onResponse1,
                                  IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        AccountAPIs api = retrofit.create(AccountAPIs.class);
        Call<BaseResponse> call = api.confirm_account(phone,code);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable throwable) {
                onFailure.onFailure();
            }
        });
    }

    public static void resendCode(final Context context,
                                      String phone,
                                      IResponse onResponse1,
                                      IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        AccountAPIs api = retrofit.create(AccountAPIs.class);
        Call<BaseResponse> call = api.resend_code(phone);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable throwable) {
                onFailure.onFailure();
            }
        });
    }

    public static void check_phone(final Context context,
                                      String phone,
                                      String email,
                                      IResponse onResponse1,
                                      IFailure onFailure1){
        onResponse = onResponse1;
        onFailure = onFailure1;

        Retrofit retrofit = configureRetrofitWithoutBearer();
        AccountAPIs api = retrofit.create(AccountAPIs.class);
        Call<BaseResponse> call = api.check_phone(phone,email);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                BaseFunctions.processResponse(response,onResponse,context);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable throwable) {
                onFailure.onFailure();
            }
        });
    }
}
