package com.nyx.warshat.activities.details;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.warshat.Intrafaces.IMove;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.ProblemImagesAdapter;
import com.nyx.warshat.models.ProblemImageObject;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import Arabs.ArabButton;
import Arabs.ArabEditText;

public class AddRequestStepThree extends GlobalActivity {

    private ImageButton btn_back;
    private ArabButton btn_next;
    private ArabEditText edt_desc;
    private RecyclerView rv_images;
    private final static int SELECT_PHOTO = 12345;
    private final static int PLACE_PICKER_REQUEST = 999;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private String imagePath = "";
    private Bitmap bitmap;
    private Uri croppedImg;
    private static final int PICK_FROM_GALLERY = 14436;
    private List<String> imagesPathsList;
    private List<ProblemImageObject> imagesBitmapsList;
    private ProblemImagesAdapter imagesAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request_step_three);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //ImageButton
        btn_back = findViewById(R.id.go_back);
        //Button
        btn_next = findViewById(R.id.next_btn);
        //RecyclerView
        rv_images =  findViewById(R.id.images_recycler);
        //EditText
        edt_desc = findViewById(R.id.desc);
    }

    private void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String desc = "لا يوجد وصف للتصليحة";
                if (!edt_desc.getText().toString().trim().equals("")){
                    desc = edt_desc.getText().toString();
                }
                Intent intent = new Intent(AddRequestStepThree.this,AddRequestStepFour.class);
                intent.putExtra("dates",getIntent().getStringExtra("dates"));
                intent.putExtra("times",getIntent().getStringExtra("times"));
                intent.putExtra("desc",desc);
                intent.putExtra("images",new Gson().toJson(imagesPathsList));
                intent.putExtra("id",getIntent().getStringExtra("id"));
                startActivity(intent);
            }
        });
    }

    private void init_activity() {
        init_images_recycler();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pick();
                } else {
                    Toast.makeText(getApplicationContext(), "Not allowed to open gallery", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    void pick() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }

        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("RESULTED ", requestCode + " and " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        Log.e("GOTCHA ", "yes");
                        final Uri resultUri = UCrop.getOutput(data);
                        Log.e("Picsaved1 in : ", resultUri.toString());
                        Log.e("Picsaved2 in : ", resultUri.getPath());
                        Log.e("Picsaved3 in : ", resultUri.getEncodedPath());

                        try {
                            imagePath = resultUri.getPath();
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            bitmap = BitmapFactory.decodeFile(resultUri.getPath(), options);
                            File file = new File(imagePath);
                            if (file.exists()) {
                                imagesBitmapsList.add(new ProblemImageObject(bitmap, 1));
                                imagesPathsList.add(imagePath);
                                imagesAdapter.notifyDataSetChanged();
                            }else {
                                Toast.makeText(this, "الرجاء اختيار صورة اخرى", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception e) {

                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;


                case SELECT_PHOTO:
                    if (data != null) {
                        Uri pickedImage = data.getData();
                        String destinationFileName = "P_"+System.nanoTime() + ".jpg";


                        UCrop.of(pickedImage, Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(1, 1)
                                .withMaxResultSize(480, 480)
                                .start(AddRequestStepThree.this);
                    }
                    break;
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.e("CROP ERR", cropError.getMessage());

        }


    }

    private void init_images_recycler(){
        imagesPathsList = new ArrayList<>();
        imagesBitmapsList = new ArrayList<>();
        imagesBitmapsList.add(new ProblemImageObject(null,0));
        imagesAdapter = new ProblemImagesAdapter(AddRequestStepThree.this, imagesBitmapsList, new IMove() {
            @Override
            public void move(int position) {
                try {
                    if (ActivityCompat.checkSelfPermission(AddRequestStepThree.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(AddRequestStepThree.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    } else {
                        pick();
                    }
                } catch (Exception e) {
                }
            }
        });
        rv_images.setLayoutManager(new LinearLayoutManager(AddRequestStepThree.this,LinearLayoutManager.HORIZONTAL,false));
        rv_images.setAdapter(imagesAdapter);
    }


}
