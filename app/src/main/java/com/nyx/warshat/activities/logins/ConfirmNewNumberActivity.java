package com.nyx.warshat.activities.logins;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.nyx.warshat.APIsClass.AccountAPIsClass;
import com.nyx.warshat.Intrafaces.IFailure;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.edits.ProfileActivity;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;

import org.json.JSONException;
import org.json.JSONObject;

import Arabs.ArabButton;
import Arabs.ArabEditText;

public class ConfirmNewNumberActivity extends GlobalActivity {

    private ImageButton btn_back;
    private ArabEditText edt_code;
    private ArabButton btn_confirm;
    private ProgressBar pb_loading;
    private LinearLayout btn_resend_code;
    private Intent myIntent;
    private String phone_number = "";
    private ImageView img_resend_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_new_number);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //ImageButton
        btn_back = findViewById(R.id.go_back);
        //EditText
        edt_code = findViewById(R.id.confirm_account_code);
        //Button
        btn_confirm = findViewById(R.id.confirm_account_confirm);
        //ProgressBar
        pb_loading = findViewById(R.id.confirm_account_resend_progress);
        //LinearLayout
        btn_resend_code = findViewById(R.id.confirm_account_resend);
        //Intent
        myIntent = getIntent();
        //ImageView
        img_resend_icon = findViewById(R.id.confirm_account_resend_icon);
    }

    private void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_code.getText().toString().equals("")){
                    edt_code.requestFocus();
                    edt_code.setHintTextColor(Color.RED);
                    return;
                }
                callConfirmAPI(phone_number,edt_code.getText().toString().trim());
            }
        });
        btn_resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callResendCodeAPI(phone_number);
            }
        });
    }

    private void init_activity() {
        phone_number = myIntent.getStringExtra("phone");
        callResendCodeAPI(phone_number);
    }

    private void callConfirmAPI(String phone,String code){
        btn_confirm.setEnabled(false);
        AccountAPIsClass.confirmAccount(
                ConfirmNewNumberActivity.this,
                phone,
                code,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        btn_confirm.setEnabled(true);
                    }

                    @Override
                    public void onNotActivated() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            callUpdateAccountAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        btn_confirm.setEnabled(true);
                        Toast.makeText(ConfirmNewNumberActivity.this, "لا يوجد اتصال بالانترنت, الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callResendCodeAPI(String phone){
        img_resend_icon.setVisibility(View.GONE);
        pb_loading.setVisibility(View.VISIBLE);
        btn_resend_code.setEnabled(false);
        btn_confirm.setEnabled(false);
        edt_code.setEnabled(false);
        AccountAPIsClass.resendCode(ConfirmNewNumberActivity.this,
                phone,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        img_resend_icon.setVisibility(View.VISIBLE);
                        pb_loading.setVisibility(View.GONE);
                        btn_resend_code.setEnabled(true);
                        btn_confirm.setEnabled(true);
                        edt_code.setEnabled(true);
                    }

                    @Override
                    public void onNotActivated() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        img_resend_icon.setVisibility(View.VISIBLE);
                        pb_loading.setVisibility(View.GONE);
                        btn_resend_code.setEnabled(true);
                        btn_confirm.setEnabled(true);
                        edt_code.setEnabled(true);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_resend_icon.setVisibility(View.VISIBLE);
                        pb_loading.setVisibility(View.GONE);
                        btn_resend_code.setEnabled(true);
                        btn_confirm.setEnabled(true);
                        edt_code.setEnabled(true);
                        Toast.makeText(ConfirmNewNumberActivity.this, "لا يوجد اتصال بالانترنت, الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callUpdateAccountAPI(){
        BackgroundServices.getInstance(ConfirmNewNumberActivity.this).setBaseUrl(APIUrl.SERVER+"update_profile")
                .addPostParam("name" ,  getIntent().getStringExtra("name"))
                .addPostParam("phone" ,  getIntent().getStringExtra("phone"))
                .addPostParam("address" ,  "address")
                .addPostParam("email" ,  getIntent().getStringExtra("email"))
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        btn_confirm.setEnabled(true);
                        Toast.makeText(ConfirmNewNumberActivity.this, new JSONObject(response)
                                .getString("message"), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(ConfirmNewNumberActivity.this,ProfileActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
    }
}
