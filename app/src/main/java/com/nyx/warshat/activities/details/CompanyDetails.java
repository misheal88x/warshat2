package com.nyx.warshat.activities.details;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.JsonIOException;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.ProductsAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.ProductSelector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CompanyDetails extends GlobalActivity {

    private LinearLayout lyt_phone_number,lyt_land_line,lyt_email;
    private TextView tv_phone_number,tv_land_line,tv_email;
    private RelativeLayout whatsapp,instagram,facebook;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_details);
        findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lyt_phone_number = findViewById(R.id.phone_number_lyt);
        lyt_land_line = findViewById(R.id.land_line_lyt);
        lyt_email = findViewById(R.id.email_lyt);
        tv_phone_number = findViewById(R.id.phone_number_txt);
        tv_land_line = findViewById(R.id.land_line_txt);
        tv_email = findViewById(R.id.email_txt);
        whatsapp = findViewById(R.id.whatsapp_layout);
        instagram = findViewById(R.id.instagram);
        facebook = findViewById(R.id.facebook);

        String id = getIntent().getStringExtra("id");
        ((TextView)findViewById(R.id.name)).setText(getIntent().getStringExtra("name"));
        ((TextView)findViewById(R.id.name2)).setText(getIntent().getStringExtra("name"));
        try {
            Log.i("ddfdfe", "onCreate: "+getIntent().getStringExtra("icon"));
            Glide.with(this).load(getIntent().getStringExtra("icon"))
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big).centerCrop()).into(
                    ((ImageView) findViewById(R.id.pic)));
        }catch (Exception e){}
        try {
            Glide.with(this).load(getIntent().getStringExtra("icon"))
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big)).into(
                    ((ImageView) findViewById(R.id.icon)));
        }catch (Exception e){}
        retrive(id);
    }
    void retrive(final String id){
        BackgroundServices.getInstance(this)
                .setBaseUrl(APIUrl.SERVER+"get_workshop?id="+id).CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                findViewById(R.id.loading).setVisibility(View.GONE);
                final JSONObject data = new JSONObject(response).getJSONObject("data");
                /*
                if (data.getString("workshop_city_name")!=null&&data.getString("workshop_region_name")!=null){
                    if (!data.getString("workshop_region_name").equals("")){
                        ((ArabTextView)findViewById(R.id.address)).setText(data.getString("workshop_city_name")+", "+data.getString("workshop_region_name"));
                    }else {
                        findViewById(R.id.address).setVisibility(View.GONE);
                    }
                }else {
                    findViewById(R.id.address).setVisibility(View.GONE);
                }
                **/
                ((TextView)findViewById(R.id.address)).setText(data.getString("address"));
                ((TextView)findViewById(R.id.about)).setText(data.getString("details"));
                ((TextView)findViewById(R.id.trades)).setText(data.getString("supported_trademarks"));
                findViewById(R.id.loading).setVisibility(View.GONE);
                findViewById(R.id.main).setVisibility(View.VISIBLE);


                JSONArray array = new JSONObject(response).getJSONObject("data").getJSONArray("products");
                final ArrayList imaages = new ArrayList<>();
                if (array.length() == 0){
                    findViewById(R.id.no_items).setVisibility(View.VISIBLE);
                }else {
                    findViewById(R.id.no_items).setVisibility(View.GONE);
                }
                for(int i=0;i< array.length();i++)imaages.add(new ProductSelector(array.getJSONObject(i)));

                RecyclerView list = (RecyclerView)findViewById(R.id.pics_list);
                list.setLayoutManager(new GridLayoutManager(CompanyDetails.this ,3));
                ProductsAdapter adapter =new ProductsAdapter(imaages ,CompanyDetails.this);
                list.setAdapter(adapter);
                if (data.getString("phone")!=null&&!data.getString("phone").equals("")){
                    tv_phone_number.setText("00963-"+data.getString("phone"));
                }else {
                    findViewById(R.id.phone_number_lyt).setVisibility(View.GONE);
                    findViewById(R.id.call).setVisibility(View.GONE);
                }
                if(data.getString("local_phone") != null && !data.getString("local_phone").equals("")){
                    tv_land_line.setText("00963-"+data.getString("local_phone"));
                }else {
                    findViewById(R.id.land_line_lyt).setVisibility(View.GONE);
                }
                if(data.getString("email") != null && !data.getString("email").equals("")){
                    tv_email.setText(data.getString("email"));
                }else {
                    findViewById(R.id.email_lyt).setVisibility(View.GONE);
                }

                findViewById(R.id.call).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        try {
                            callIntent.setData(Uri.parse("tel: "+data.getString("phone")));
                            startActivity(callIntent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
                /*
                findViewById(R.id.whats).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        boolean is_whatsapp_installed = appInstalledOrNot("com.whatsapp" ,CompanyDetails.this);
                        if(is_whatsapp_installed) {
                            Intent intent= null;
                            try {
                                intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone="+
                                        data.getString("whatsapp")));
                                startActivity(intent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        else{
                            Toast.makeText(CompanyDetails.this, "Whatsapp is not installed !", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
**/
                /*
                findViewById(R.id.contact).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent ii=new Intent(CompanyDetails.this, SendRequestToCompany.class);
                        ii.putExtra("id" ,getIntent().getStringExtra("id"));
                        ii.putExtra("name" ,getIntent().getStringExtra("name"));
                        try {
                            ii.putExtra("address" ,data.getString("address"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        ii.putExtra("icon" ,getIntent().getStringExtra("icon"));
                        ii.putExtra("items" ,imaages);
                        startActivity(ii);
                    }
                });


                 */

                    if (data.has("whatsapp_phone")) {
                        if (data.getString("whatsapp_phone") != null) {
                            if (!data.getString("whatsapp_phone").equals("")) {
                                whatsapp.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        try {
                                            openWhatsapp(data.getString("whatsapp_phone"));
                                        }catch (JSONException e){}
                                    }
                                });
                            } else {
                                whatsapp.setVisibility(View.GONE);
                            }
                        } else {
                            whatsapp.setVisibility(View.GONE);
                        }
                    } else {
                        whatsapp.setVisibility(View.GONE);
                    }


                if (data.has("facebook")){
                    if (data.getString("facebook")!=null){
                        if (!data.getString("facebook").equals("")){
                            facebook.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try{
                                        openFacebook(CompanyDetails.this,data.getString("facebook"));
                                    }catch (JSONException e){}
                                }
                            });
                        }else {
                            facebook.setVisibility(View.GONE);
                        }
                    }else {
                        facebook.setVisibility(View.GONE);
                    }
                }else {
                    facebook.setVisibility(View.GONE);
                }

                if (data.has("instgram_link")){
                    if (data.getString("instgram_link")!=null){
                        if (!data.getString("instgram_link").equals("")){
                            instagram.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    try{
                                        openInstagram(CompanyDetails.this,data.getString("instgram_link"));
                                    }catch (JSONException e){}
                                }
                            });
                        }else {
                            instagram.setVisibility(View.GONE);
                        }
                    }else {
                        instagram.setVisibility(View.GONE);
                    }
                }else {
                    instagram.setVisibility(View.GONE);
                }
            }
        });
    }
    private static boolean appInstalledOrNot(String uri , Context c) {
        PackageManager pm = c.getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }

    private void openWhatsapp(String number){
        String new_number = "";
        //Starts with zero
        if (number.length()==10){
            new_number = number.substring(1,number.length());
        }
        //Without zero
        else if (number.length()==9){
            new_number = number;
        }
        //Starts with +963
        else if (number.length() == 13){
            new_number = number.substring(4,number.length());
        }
        //Starts with 00963
        else if (number.length() == 14){
            new_number = number.substring(5,number.length());
        }
        //Starts with 009630
        else if (number.length() == 15){
            new_number = number.substring(6,number.length());
        }else {
            Toast.makeText(this, "الرقم خاطئ", Toast.LENGTH_SHORT).show();
            return;
        }
        String url = "https://api.whatsapp.com/send?phone=+963 "+new_number;
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    private void openInstagram(Context context,String url){
        final Intent intent = new Intent(Intent.ACTION_VIEW);
        try {
            if (context.getPackageManager().getPackageInfo("com.instagram.android", 0) != null) {
                if (url.endsWith("/")) {
                    url = url.substring(0, url.length() - 1);
                }
                final String username = url.substring(url.lastIndexOf("/") + 1);
                // http://stackoverflow.com/questions/21505941/intent-to-open-instagram-user-profile-on-android
                intent.setData(Uri.parse("http://instagram.com/_u/" + username));
                intent.setPackage("com.instagram.android");
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        intent.setData(Uri.parse(url));
        startActivity(intent);
    }

    private void openFacebook(Context context,String link){
        context.startActivity(newFacebookIntent(context.getPackageManager(),link));
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri = Uri.parse(url);
        try {
            ApplicationInfo applicationInfo = pm.getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                // http://stackoverflow.com/a/24547437/1048340
                uri = Uri.parse("fb://facewebmodal/f?href=" + url);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }
}
