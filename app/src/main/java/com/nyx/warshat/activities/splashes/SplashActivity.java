package com.nyx.warshat.activities.splashes;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.nyx.warshat.activities.homes.CustomerHome;
import com.nyx.warshat.activities.homes.WorkshopHome;
import com.nyx.warshat.activities.logins.LoginActivity;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.logins.RegisterWorkshopStepOneNew;
import com.nyx.warshat.activities.logins.SelectSignupType;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.logins.SignupAsCustomer;
import com.nyx.warshat.activities.logins.SignupWorkshopStepOne;
import com.nyx.warshat.activities.logins.WelcomeActivity;
import com.nyx.warshat.helper.SharedPrefManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SplashActivity extends GlobalActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
            findViewById(R.id.go_signup).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getIntent().getStringExtra("account_type")!=null){
                        if (getIntent().getStringExtra("account_type").equals("customer")){
                            startActivity(new Intent(SplashActivity.this, SignupAsCustomer.class));
                        }else {
                            startActivity(new Intent(SplashActivity.this, RegisterWorkshopStepOneNew.class));
                        }
                    }
                }
            });
            findViewById(R.id.go_signin).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                }
            });

    }
}
