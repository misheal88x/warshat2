package com.nyx.warshat.activities.infinites;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.StringArrayAdapter;
import com.nyx.warshat.adapters.WorkshopsAdapter;
import com.nyx.warshat.customes.EndlessScrollListener;
import com.nyx.warshat.customes.EndlessScrollView;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.ConnectionUtils;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.City;
import com.nyx.warshat.models.CityObject;
import com.nyx.warshat.models.RegionObject;
import com.nyx.warshat.models.Workshop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Arabs.ArabTextView;

public class CategoryWorkshops extends GlobalActivity implements EndlessScrollListener {

    public RecyclerView mainRecylceView;
    public View loading;
    public RecyclerView.Adapter adapter;
    public ArrayList items;
    public  View noItemsFound,no_more ,not_connected;
    public TextView no_items_found_text;
    public TextView title;
    EndlessScrollView mainScrollView;
    public FloatingActionButton fab;
    ArrayList<City> cities;


    private Spinner cities_spinner,regions_spinner;
    private List<CityObject> cities_list;
    private List<RegionObject> regions_list;
    private String[] cities_array,regions_array;
    private StringArrayAdapter cities_adapter,regions_adapter;
    private ArabTextView tv_region_hint;
    public String region_id="-1";

    boolean hasAd=false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_workshops);
        tv_region_hint = findViewById(R.id.region_hint);
        init_spinners();
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"ws_init").CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {

                JSONObject all = new JSONObject(response).getJSONObject("data");
                JSONArray cities_arr = all.getJSONArray("cities");
                if (cities_arr.length()>0){
                    for (int i = 0; i < cities_arr.length(); i++) {
                        JSONObject jo = cities_arr.getJSONObject(i);
                        List<RegionObject> rol = new ArrayList<>();
                        if (jo.getJSONArray("regions").length()>0){
                            for (int j = 0; j < jo.getJSONArray("regions").length(); j++) {
                                JSONObject rjo = jo.getJSONArray("regions").getJSONObject(j);
                                rol.add(new RegionObject(rjo.getInt("id"),
                                        rjo.getString("name"),
                                        rjo.getInt("city_id")));
                            }
                        }
                        cities_list.add(new CityObject(jo.getInt("id"),
                                jo.getString("name"),
                                rol));
                    }
                }
                cities_array = new String[cities_list.size()];
                cities_array[0] = "المدينة";
                if (cities_list.size()>1){
                    for (int i = 1; i < cities_list.size(); i++) {
                        cities_array[i] = cities_list.get(i).getName();
                    }
                }
                cities_adapter = new StringArrayAdapter(CategoryWorkshops.this,cities_array);
                cities_spinner.setAdapter(cities_adapter);
                init_spinners_events();
                findViewById(R.id.location_box).setVisibility(View.VISIBLE);
                findViewById(R.id.location_divider).setVisibility(View.VISIBLE);
            }
        });

        mainRecylceView = (RecyclerView)findViewById(R.id.recyclerview5);
        mainScrollView = (EndlessScrollView)findViewById(R.id.heart_scroll_view);
        mainScrollView.setScrollViewListener(this);
        noItemsFound= findViewById(R.id.no_items_found);
        not_connected= findViewById(R.id.no_connected_to_internet);
        no_items_found_text= (TextView) findViewById(R.id.no_items_found_text);
        no_more= findViewById(R.id.no_more_to_show);
        loading = findViewById(R.id.loading);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainScrollView.scrollTo(0,0);
            }
        });
        items = new ArrayList();
        mainRecylceView.setLayoutManager(new LinearLayoutManager(this));
        //mainRecylceView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        adapter= new WorkshopsAdapter(items,this);
        mainRecylceView.setAdapter(adapter);
        max=1000;

        findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Glide.with(this).load(getIntent().getStringExtra("icon")).into(((ImageView)findViewById(R.id.workshop_icon)));
        ((TextView)findViewById(R.id.name)).setText(getIntent().getStringExtra("name"));
        not_connected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retry();
            }
        });
    }

    public   void  fetchData(){
        loadMore();
    }

    public     int max=100000;
    public   int current=0;
    public String status="-1";

    public boolean loadingFlag = false;
    public   void loadMore(){
        loadingFlag=true;
        //if(current>0)findViewById(R.id.loading_pager).setVisibility(View.VISIBLE);
        noItemsFound.setVisibility(View.GONE);
        no_more.setVisibility(View.GONE);
        not_connected.setVisibility(View.GONE);
        Log.i("tttt", "loadMore: "+(getIntent().getBooleanExtra("is_emergency",false)?"on":"off"));
        Log.i("tttt", "loadMore: "+region_id);
        Log.i("tttt", "loadMore: "+region_id);
        BackgroundServices.getInstance(this)
                .setBaseUrl(APIUrl.SERVER + "workshops/all?cat_id="+getIntent().getStringExtra("cat_id")
                        +"&region_id=" + region_id
                        +"&critical_regions="+(getIntent().getBooleanExtra("is_emergency",false)?"on":"off")
                        +"&start="+current
                )
              .CallGet(
                new PostAction() {
                    @Override
                    public void whenFinished(String status ,String response) {
                        loading.setVisibility(View.GONE);
                        loadingFlag = false;
                        if (response.equals("")) {
                            not_connected.setVisibility(View.VISIBLE);
                        } else {
                            try {

                                findViewById(R.id.loading_pager).setVisibility(View.GONE);
                                String pic = new JSONObject(response).getJSONObject("data").getString("cat_ad");
                                if(!pic.trim().equals("")){
                                    setAd(pic);
                                }
                                        //
                                JSONArray data = new JSONObject(response).getJSONObject("data").getJSONArray("workshops");

                                if (data.length() > 0) {
                                    for (int i = 0; i < data.length(); i++)
                                        items.add(new Workshop(data.getJSONObject(i)));
                                    adapter.notifyDataSetChanged();
                                    current += 10;

                                }
                                mainScrollView.setVisibility(View.VISIBLE);
                                if (items.size() == 0) noItemsFound.setVisibility(View.VISIBLE);
                                else {
                                    if (data.length() == 0) {
                                        no_more.setVisibility(View.VISIBLE);
                                        max = items.size();
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
        );


    };

    void retry(){
        loading.setVisibility(View.VISIBLE);
      items.clear();
      adapter.notifyDataSetChanged();

        if(!ConnectionUtils.isNetworkAvailable(this)){
            not_connected.setVisibility(View.VISIBLE);
            not_connected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    retry();
                }
            });
            loading.setVisibility(View.GONE);
        }else{
            not_connected.setVisibility(View.GONE);
            fetchData();
        }
    }


    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();
        if(items.size()>=max){
            return;
        }
        if (!loadingFlag) {
            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                loadMore();
            }

        }
    }


    private void init_spinners(){
        cities_spinner = findViewById(R.id.select_city);
        regions_spinner = findViewById(R.id.select_region);
        cities_list = new ArrayList<>();
        regions_list = new ArrayList<>();
        cities_list.add(new CityObject());
        regions_list.add(new RegionObject());
        cities_array = new String[]{"المدينة"};
        regions_array = new String[]{"لمنطقة"};
        cities_adapter = new StringArrayAdapter(CategoryWorkshops.this,cities_array);
        regions_adapter = new StringArrayAdapter(CategoryWorkshops.this,regions_array);
        regions_spinner.setAdapter(regions_adapter);
        cities_spinner.setAdapter(cities_adapter);
    }

    private void init_spinners_events(){
        cities_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                regions_list.clear();
                regions_array = new String[]{"المنطقة"};
                regions_list.add(new RegionObject());
                regions_adapter = new StringArrayAdapter(CategoryWorkshops.this,regions_array);
                regions_spinner.setAdapter(regions_adapter);
                region_id = "-1";
                if (position>0){
                    CityObject co = cities_list.get(position);
                    if (co.getRegions()!=null){
                        if (co.getRegions().size()>0){
                            for (RegionObject ro : co.getRegions()){
                                regions_list.add(ro);
                            }
                            regions_array = new String[regions_list.size()];
                            regions_array[0] = "المنطقة";
                            for (int j = 1; j < regions_list.size(); j++) {
                                regions_array[j] = regions_list.get(j).getName();
                            }
                            regions_adapter = new StringArrayAdapter(CategoryWorkshops.this,regions_array);
                            regions_spinner.setAdapter(regions_adapter);
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        regions_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int i, long id) {
                long pos = parent.getItemIdAtPosition(i);
                int position = Integer.valueOf(String.valueOf(pos));
                if (position>0){
                    tv_region_hint.setVisibility(View.GONE);
                    mainRecylceView.setVisibility(View.VISIBLE);
                    region_id = String.valueOf(regions_list.get(position).getId());
                    retry();
                }else {
                    tv_region_hint.setVisibility(View.VISIBLE);
                    mainRecylceView.setVisibility(View.GONE);
                    not_connected.setVisibility(View.GONE);
                    no_more.setVisibility(View.GONE);
                    noItemsFound.setVisibility(View.GONE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    void setAd(String pic){
        if(hasAd)return;
        hasAd=true;
        Glide.with(this)
                .load(pic).into((ImageView)findViewById(R.id.ad));
        findViewById(R.id.ad).setVisibility(View.VISIBLE);
    }
}
