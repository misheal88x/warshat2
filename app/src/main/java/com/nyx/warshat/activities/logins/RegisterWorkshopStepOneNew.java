package com.nyx.warshat.activities.logins;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.nyx.warshat.APIsClass.AccountAPIsClass;
import com.nyx.warshat.Intrafaces.IFailure;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;

import Arabs.ArabButton;
import Arabs.ArabEditText;

public class RegisterWorkshopStepOneNew extends GlobalActivity {

    private ArabEditText edt_fullname,edt_mobile,edt_email,edt_password,edt_c_password;
    private ArabButton btn_next;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_workshop_step_one_new);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //EditText
        edt_fullname = findViewById(R.id.fullname);
        edt_mobile = findViewById(R.id.mobile_number);
        edt_email = findViewById(R.id.email);
        edt_password = findViewById(R.id.password);
        edt_c_password = findViewById(R.id.confirm_password);
        //Button
        btn_next = findViewById(R.id.next);
    }

    private void init_events() {
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_fullname.getText().toString().trim().equals("")){
                    edt_fullname.setHintTextColor(Color.RED);
                    edt_fullname.requestFocus();
                    return;
                }
                if (edt_mobile.getText().toString().trim().equals("")){
                    edt_mobile.setHintTextColor(Color.RED);
                    edt_mobile.requestFocus();
                    return;
                }
                if(!edt_mobile.getText().toString().trim().startsWith("09") || edt_mobile.getText().toString().trim().length()!=10){
                    edt_mobile.setHintTextColor(Color.RED);
                    edt_mobile.requestFocus();
                    Toast.makeText(RegisterWorkshopStepOneNew.this, getString(R.string.phone_09), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_email.getText().toString().trim().equals("")){
                    edt_email.setHintTextColor(Color.RED);
                    edt_email.requestFocus();
                    return;
                }
                if (!isValidEmail(edt_email.getText().toString().trim())){
                    edt_email.setHintTextColor(Color.RED);
                    edt_email.requestFocus();
                    Toast.makeText(RegisterWorkshopStepOneNew.this, "يجب عليك كتابة البريد الإلكتروني بالطريقة الصحيحة", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_password.getText().toString().trim().equals("")){
                    edt_password.setHintTextColor(Color.RED);
                    edt_password.requestFocus();
                    return;
                }
                if (edt_password.getText().toString().trim().length()<8){
                    Toast.makeText(RegisterWorkshopStepOneNew.this, getString(R.string.pass_hint), Toast.LENGTH_SHORT).show();
                    edt_password.requestFocus();
                    edt_password.setHintTextColor(Color.RED);
                    return;
                }
                if (edt_c_password.getText().toString().trim().equals("")){
                    edt_c_password.setHintTextColor(Color.RED);
                    edt_c_password.requestFocus();
                    return;
                }
                if (!edt_c_password.getText().toString().trim().equals(edt_password.getText().toString().trim())){
                    Toast.makeText(RegisterWorkshopStepOneNew.this, "قم بتأكيد كلمة المرور بالشكل الصحيح", Toast.LENGTH_SHORT).show();
                    edt_c_password.requestFocus();
                    return;
                }
                /*
                Intent intent = new Intent(RegisterWorkshopStepOneNew.this,RegisterWorkshopStepTwoNew.class);
                intent.putExtra("full_name",edt_fullname.getText().toString());
                intent.putExtra("mobile",edt_mobile.getText().toString());
                intent.putExtra("email",edt_email.getText().toString());
                intent.putExtra("password",edt_password.getText().toString());
                startActivity(intent);
                **/
                checkPhoneNumber();

            }
        });
    }

    private void init_activity() {
    }
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void checkPhoneNumber(){
        btn_next.setEnabled(false);
        AccountAPIsClass.check_phone(RegisterWorkshopStepOneNew.this,
                edt_mobile.getText().toString(),
                edt_email.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        btn_next.setEnabled(true);
                    }

                    @Override
                    public void onNotActivated() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        btn_next.setEnabled(true);
                        Intent intent = new Intent(RegisterWorkshopStepOneNew.this,RegisterWorkshopStepTwoNew.class);
                        intent.putExtra("full_name",edt_fullname.getText().toString());
                        intent.putExtra("mobile",edt_mobile.getText().toString());
                        intent.putExtra("email",edt_email.getText().toString());
                        intent.putExtra("password",edt_password.getText().toString());
                        startActivity(intent);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(RegisterWorkshopStepOneNew.this, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                        btn_next.setEnabled(true);
                    }
                });

        /*
        BackgroundServices.getInstance(RegisterWorkshopStepOneNew.this)
                .setBaseUrl(APIUrl.SERVER + "check-username")
                .addPostParam("phone", edt_mobile.getText().toString())
                .addPostParam("email", edt_email.getText().toString())
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        btn_next.setEnabled(true);
                        if (status.equals("401")){
                            Toast.makeText(RegisterWorkshopStepOneNew.this, "رقم الجوال موجود مسبقا", Toast.LENGTH_SHORT).show();
                            return;
                        } else if (!status.equals("402")) {
                            Toast.makeText(RegisterWorkshopStepOneNew.this, "البريد الإلكتروني موجود مسبقا", Toast.LENGTH_SHORT).show();
                            return;
                        }else if (!status.equals("200")) {
                            Intent intent = new Intent(RegisterWorkshopStepOneNew.this,RegisterWorkshopStepTwoNew.class);
                            intent.putExtra("full_name",edt_fullname.getText().toString());
                            intent.putExtra("mobile",edt_mobile.getText().toString());
                            intent.putExtra("email",edt_email.getText().toString());
                            intent.putExtra("password",edt_password.getText().toString());
                            startActivity(intent);
                        } else {
                            Toast.makeText(RegisterWorkshopStepOneNew.this,
                                    new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                **/
    }

}
