package com.nyx.warshat.activities.details;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;

import Arabs.ArabButton;

public class AddRequestStepTwo extends GlobalActivity {

    private ImageButton btn_back;
    private ArabButton btn_next;
    public CheckBox[] boxes;
    private int num_selected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request_step_two);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        btn_back = findViewById(R.id.go_back);
        btn_next = findViewById(R.id.next_btn);
        boxes = new CheckBox[]{
                (CheckBox) findViewById(R.id.c1),
                (CheckBox) findViewById(R.id.c2),
                (CheckBox) findViewById(R.id.c3),
                (CheckBox) findViewById(R.id.c4),
                (CheckBox) findViewById(R.id.c5),
        };
    }

    private void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String options = "";
                for(int i=0;i<boxes.length;i++)
                    if(boxes[i].isChecked())options+=boxes[i].getText()+" ,";
                Intent intent = new Intent(AddRequestStepTwo.this,AddRequestStepThree.class);
                if (options.equals("")){
                    Toast.makeText(AddRequestStepTwo.this, "يجب اختيار توقيت واحد على الأقل", Toast.LENGTH_SHORT).show();
                }else {
                    intent.putExtra("dates",getIntent().getStringExtra("dates"));
                    intent.putExtra("times",options);
                    intent.putExtra("id",getIntent().getStringExtra("id"));
                    startActivity(intent);
                }
                
            }
        });
    }

    private void init_activity() {
    }
}
