package com.nyx.warshat.activities.logins;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.warshat.APIsClass.AccountAPIsClass;
import com.nyx.warshat.Intrafaces.IFailure;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.helper.SharedPrefManager;
import com.nyx.warshat.models.SelectedRegion;
import com.nyx.warshat.models.SelectedRegionsForSend;
import com.nyx.warshat.models.TempUserObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import Arabs.ArabButton;
import Arabs.ArabEditText;

public class ConfirmAccountActivity extends GlobalActivity {

    private ImageButton btn_back;
    private ArabEditText edt_code;
    private ArabButton btn_confirm;
    private ProgressBar pb_loading;
    private LinearLayout btn_resend_code;
    private Intent myIntent;
    private String phone_number = "";
    private ImageView img_resend_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_account);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //ImageButton
        btn_back = findViewById(R.id.go_back);
        //EditText
        edt_code = findViewById(R.id.confirm_account_code);
        //Button
        btn_confirm = findViewById(R.id.confirm_account_confirm);
        //ProgressBar
        pb_loading = findViewById(R.id.confirm_account_resend_progress);
        //LinearLayout
        btn_resend_code = findViewById(R.id.confirm_account_resend);
        //Intent
        myIntent = getIntent();
        //ImageView
        img_resend_icon = findViewById(R.id.confirm_account_resend_icon);
    }

    private void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_code.getText().toString().equals("")){
                    edt_code.requestFocus();
                    edt_code.setHintTextColor(Color.RED);
                    return;
                }
                callConfirmAPI(phone_number,edt_code.getText().toString().trim());
            }
        });
        btn_resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callResendCodeAPI(phone_number);
            }
        });
    }

    private void init_activity() {
        SharedPrefManager.getInstance(ConfirmAccountActivity.this).setAccountConfirmed(false);
        if (!SharedPrefManager.getInstance(ConfirmAccountActivity.this).getTempAccount().getCustomer_phone().equals("")||
                !SharedPrefManager.getInstance(ConfirmAccountActivity.this).getTempAccount().getWork_phone().equals("")){
            img_resend_icon.setVisibility(View.VISIBLE);
            pb_loading.setVisibility(View.GONE);
            btn_resend_code.setEnabled(true);
            btn_confirm.setEnabled(true);
            edt_code.setEnabled(true);
            if (SharedPrefManager.getInstance(ConfirmAccountActivity.this).getAccountType().equals("customer")){
                phone_number = SharedPrefManager.getInstance(ConfirmAccountActivity.this).getTempAccount().getCustomer_phone();
            }else {
                phone_number = SharedPrefManager.getInstance(ConfirmAccountActivity.this).getTempAccount().getWork_phone();
            }
        }else {
            if (myIntent.getStringExtra("user_type").equals("1")){
                SharedPrefManager.getInstance(ConfirmAccountActivity.this).setAccountType("customer");
                TempUserObject t = new TempUserObject();

                t.setCustomer_login_type("2");
                t.setCustomer_user_type("1");
                t.setCustomer_name(myIntent.getStringExtra("name"));
                t.setCustomer_phone(myIntent.getStringExtra("phone"));
                t.setCustomer_password(myIntent.getStringExtra("password"));
                t.setCustomer_c_password(myIntent.getStringExtra("c_password"));
                t.setCustomer_email(myIntent.getStringExtra("email"));

                SharedPrefManager.getInstance(ConfirmAccountActivity.this).setTempAccount(t);
            }else {
                SharedPrefManager.getInstance(ConfirmAccountActivity.this).setAccountType("work");
                TempUserObject t = new TempUserObject();

                t.setWork_full_name(myIntent.getStringExtra("full_name"));
                t.setWork_phone(myIntent.getStringExtra("phone"));
                t.setWork_email(myIntent.getStringExtra("email"));
                t.setWork_password(myIntent.getStringExtra("password"));
                t.setWork_w_full_name(myIntent.getStringExtra("w_full_name"));
                t.setWork_w_mobile(myIntent.getStringExtra("w_mobile"));
                t.setWork_w_land_line(myIntent.getStringExtra("w_land_line"));
                t.setWork_city_id(myIntent.getStringExtra("city_id"));
                t.setWork_region_id(myIntent.getStringExtra("region_id"));
                t.setWork_cat_id(myIntent.getStringExtra("cat_id"));
                t.setWork_desc(myIntent.getStringExtra("desc"));
                t.setWork_regions_json(myIntent.getStringExtra("regions_json"));
                t.setWork_urgent_regions_json(myIntent.getStringExtra("urgent_regions_json"));
                t.setWork_pic(myIntent.getStringExtra("pic"));

                SharedPrefManager.getInstance(ConfirmAccountActivity.this).setTempAccount(t);
            }
            phone_number = myIntent.getStringExtra("phone");
            callResendCodeAPI(phone_number);
        }
    }


    private void callConfirmAPI(String phone,String code){
        btn_confirm.setEnabled(false);
        AccountAPIsClass.confirmAccount(
                ConfirmAccountActivity.this,
                phone,
                code,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        btn_confirm.setEnabled(true);
                    }

                    @Override
                    public void onNotActivated() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            if (!SharedPrefManager.getInstance(ConfirmAccountActivity.this).getTempAccount().getCustomer_phone().equals("")||
                                    !SharedPrefManager.getInstance(ConfirmAccountActivity.this).getTempAccount().getWork_phone().equals("")){
                                if (SharedPrefManager.getInstance(ConfirmAccountActivity.this).getAccountType().equals("customer")){
                                    callCustomerRegisterAPI();
                                }else {
                                    callWorkshopRegisterAPI();
                                }
                            }else {
                                if (myIntent.getStringExtra("user_type").equals("1")) {
                                    callCustomerRegisterAPI();
                                } else {
                                    callWorkshopRegisterAPI();
                                }
                            }
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        btn_confirm.setEnabled(true);
                        Toast.makeText(ConfirmAccountActivity.this, "لا يوجد اتصال بالانترنت, الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callResendCodeAPI(String phone){
        img_resend_icon.setVisibility(View.GONE);
        pb_loading.setVisibility(View.VISIBLE);
        btn_resend_code.setEnabled(false);
        btn_confirm.setEnabled(false);
        edt_code.setEnabled(false);
        AccountAPIsClass.resendCode(ConfirmAccountActivity.this,
                phone,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        img_resend_icon.setVisibility(View.VISIBLE);
                        pb_loading.setVisibility(View.GONE);
                        btn_resend_code.setEnabled(true);
                        btn_confirm.setEnabled(true);
                        edt_code.setEnabled(true);
                    }

                    @Override
                    public void onNotActivated() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        img_resend_icon.setVisibility(View.VISIBLE);
                        pb_loading.setVisibility(View.GONE);
                        btn_resend_code.setEnabled(true);
                        btn_confirm.setEnabled(true);
                        edt_code.setEnabled(true);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_resend_icon.setVisibility(View.VISIBLE);
                        pb_loading.setVisibility(View.GONE);
                        btn_resend_code.setEnabled(true);
                        btn_confirm.setEnabled(true);
                        edt_code.setEnabled(true);
                        Toast.makeText(ConfirmAccountActivity.this, "لا يوجد اتصال بالانترنت, الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callCustomerRegisterAPI(){
        TempUserObject t = SharedPrefManager.getInstance(ConfirmAccountActivity.this).getTempAccount();
        BackgroundServices.getInstance(ConfirmAccountActivity.this).setBaseUrl(APIUrl.SERVER+"login")
                .addPostParam("login_type" ,t.getCustomer_login_type())
                .addPostParam("user_type" ,t.getCustomer_user_type())
                .addPostParam("name" ,t.getCustomer_name())
                .addPostParam("phone" ,t.getCustomer_phone())
                .addPostParam("password" ,t.getCustomer_password())
                .addPostParam("c_password" ,t.getCustomer_c_password())
                .addPostParam("email" ,t.getCustomer_email())
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        if(!status.equals("200")) {
                            Toast.makeText(ConfirmAccountActivity.this, new JSONObject(response).getString("message"),
                                    Toast.LENGTH_SHORT).show();
                        }
                        if(status.equals("200")) {
                            JSONObject data=new JSONObject(response).getJSONObject("data");
                            String token =data.getString("token");
                            String type ="1";
                            SharedPrefManager.getInstance(ConfirmAccountActivity.this).saveToken(token ,type);
                            SharedPrefManager.getInstance(ConfirmAccountActivity.this).savePhoneNumber(myIntent.getStringExtra("phone"));
                            Intent intent=new Intent(ConfirmAccountActivity.this , SelectSignupType.class);

                            TempUserObject tt = new TempUserObject();
                            tt.setCustomer_login_type("");
                            tt.setCustomer_user_type("");
                            tt.setCustomer_name("");
                            tt.setCustomer_phone("");
                            tt.setCustomer_password("");
                            tt.setCustomer_c_password("");
                            tt.setCustomer_email("");
                            tt.setWork_full_name("");
                            tt.setWork_phone("");
                            tt.setWork_email("");
                            tt.setWork_password("");
                            tt.setWork_w_full_name("");
                            tt.setWork_w_mobile("");
                            tt.setWork_w_land_line("");
                            tt.setWork_city_id("");
                            tt.setWork_region_id("");
                            tt.setWork_cat_id("");
                            tt.setWork_desc("");
                            tt.setWork_regions_json("");
                            tt.setWork_urgent_regions_json("");
                            tt.setWork_pic("");
                            SharedPrefManager.getInstance(ConfirmAccountActivity.this).setTempAccount(tt);
                            SharedPrefManager.getInstance(ConfirmAccountActivity.this).setAccountConfirmed(true);

                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                });
    }

    private void callWorkshopRegisterAPI(){
        TempUserObject t = SharedPrefManager.getInstance(ConfirmAccountActivity.this).getTempAccount();
    String fullname = t.getWork_full_name();
    final String phone = t.getWork_phone();
    String email = t.getWork_email();
    String password = t.getWork_password();
    String w_full_name = t.getWork_w_full_name();
    String w_mobile = t.getWork_w_mobile();
    String w_land_line = t.getWork_w_land_line();
    String city_id = t.getWork_city_id();
    String region_id = t.getWork_region_id();
    String cat_id = t.getWork_cat_id();
    String desc = t.getWork_desc();
    String regions_json = t.getWork_regions_json();
    String urgent_regions_json = t.getWork_urgent_regions_json();
    String pic = t.getWork_pic();

    SelectedRegion[] regions = new Gson().fromJson(regions_json,SelectedRegion[].class);
    SelectedRegion[] urgent_regions = new Gson().fromJson(urgent_regions_json,SelectedRegion[].class);
    List<SelectedRegionsForSend> regionsForSend = new ArrayList<>();
    for (SelectedRegion sr : regions){
        regionsForSend.add(new SelectedRegionsForSend(sr.getId(),0));
    }
    if (urgent_regions.length>0){
        for (SelectedRegion sr : urgent_regions){
            regionsForSend.add(new SelectedRegionsForSend(sr.getId(),1));
        }
    }
    String regions_for_send_json = new Gson().toJson(regionsForSend);


        BackgroundServices.getInstance(ConfirmAccountActivity.this).setBaseUrl(APIUrl.SERVER + "login")
                .addFile("cover",new File(pic))
                .addPostParam("login_type", "2")
                .addPostParam("user_type", "2")
                .addPostParam("username", phone)
                .addPostParam("phone", phone)
                .addPostParam("password", password)
                .addPostParam("name", fullname)
                .addPostParam("c_password", password)
                .addPostParam("landline", w_land_line)
                .addPostParam("work_phone", w_mobile)
                .addPostParam("regions", regions_for_send_json)
                .addPostParam("details", desc)
                .addPostParam("work_name", w_full_name)
                .addPostParam("region_id", region_id)
                .addPostParam("city_id", city_id)
                .addPostParam("email", email)
                .addPostParam("work_cat", cat_id)
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        Toast.makeText(ConfirmAccountActivity.this, new JSONObject(response).getString("message"),
                                Toast.LENGTH_SHORT).show();

                        if (status.equals("200")) {
                            JSONObject data = new JSONObject(response).getJSONObject("data");
                            String token = data.getString("token");
                            String type = "2";
                            SharedPrefManager.getInstance(ConfirmAccountActivity.this).saveToken(token, type);
                            SharedPrefManager.getInstance(ConfirmAccountActivity.this).savePhoneNumber(phone);

                            TempUserObject tt = new TempUserObject();
                            tt.setCustomer_login_type("");
                            tt.setCustomer_user_type("");
                            tt.setCustomer_name("");
                            tt.setCustomer_phone("");
                            tt.setCustomer_password("");
                            tt.setCustomer_c_password("");
                            tt.setCustomer_email("");
                            tt.setWork_full_name("");
                            tt.setWork_phone("");
                            tt.setWork_email("");
                            tt.setWork_password("");
                            tt.setWork_w_full_name("");
                            tt.setWork_w_mobile("");
                            tt.setWork_w_land_line("");
                            tt.setWork_city_id("");
                            tt.setWork_region_id("");
                            tt.setWork_cat_id("");
                            tt.setWork_desc("");
                            tt.setWork_regions_json("");
                            tt.setWork_urgent_regions_json("");
                            tt.setWork_pic("");
                            SharedPrefManager.getInstance(ConfirmAccountActivity.this).setTempAccount(tt);
                            SharedPrefManager.getInstance(ConfirmAccountActivity.this).setAccountConfirmed(true);

                            Intent intent = new Intent(ConfirmAccountActivity.this, SelectSignupType.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                });

    }

}
