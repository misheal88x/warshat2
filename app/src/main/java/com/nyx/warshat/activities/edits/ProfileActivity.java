package com.nyx.warshat.activities.edits;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.details.SendRequestToCompany;
import com.nyx.warshat.activities.homes.CustomerHome;
import com.nyx.warshat.activities.logins.ConfirmNewNumberActivity;
import com.nyx.warshat.activities.logins.SelectSignupType;
import com.nyx.warshat.activities.logins.SignupAsCustomer;
import com.nyx.warshat.activities.splashes.SplashActivity;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.ConnectionUtils;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.helper.SharedPrefManager;
import com.nyx.warshat.utilities.FooterHelper;

import org.json.JSONException;
import org.json.JSONObject;

import Arabs.ArabEditText;

public class ProfileActivity extends GlobalActivity {

    private String old_number = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        FooterHelper.initFooterForCustomer(this);

        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"my_profile").CallPost(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                if(status.equals("200")){
                    JSONObject data = new JSONObject(response).getJSONObject("data");


                    ( (TextView) findViewById(R.id.name)).setText(data.getString("name"));
                    ( (EditText) findViewById(R.id.username)).setText(data.getString("name"));
                    ( (EditText) findViewById(R.id.phone)).setText(data.getString("phone"));
                    ( (EditText) findViewById(R.id.email)).setText(data.getString("email"));
                    old_number = data.getString("phone");
                    //( (EditText) findViewById(R.id.address)).setText(data.getString("address"));


                    findViewById(R.id.go_logout).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPrefManager.getInstance(ProfileActivity.this).logout();
                            Intent intent=new Intent(ProfileActivity.this , SelectSignupType.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    });

                    findViewById(R.id.go_save).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
                                Toast.makeText(getApplicationContext(), "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
                                return;
                            }

                            EditText name = ((EditText)findViewById(R.id.username));
                            //EditText address = ((EditText)findViewById(R.id.address));
                            EditText phone = ((EditText)findViewById(R.id.phone));
                            EditText email = ((EditText)findViewById(R.id.email));

                            if(name.getText().toString().trim().equals("")){
                                name.setHintTextColor(Color.RED);
                                name.requestFocus();
                                return;
                            }
                            /*
                            if(address.getText().toString().trim().equals("")){
                                address.setHintTextColor(Color.RED);
                                address.requestFocus();
                                return;
                            }
                            **/
                            if(phone.getText().toString().trim().equals("")){
                                phone.setHintTextColor(Color.RED);
                                phone.requestFocus();
                                return;
                            }

                            if(!phone.getText().toString().trim().startsWith("09") || phone.getText().toString().trim().length()!=10){
                                phone.setHintTextColor(Color.RED);
                                phone.requestFocus();
                                Toast.makeText(ProfileActivity.this, getString(R.string.phone_09), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if(email.getText().toString().trim().equals("")){
                                email.setHintTextColor(Color.RED);
                                email.requestFocus();
                                return;
                            }
                            if (old_number.equals(((EditText) findViewById(R.id.phone)).getText().toString().trim())) {
                                v.setEnabled(false);
                                BackgroundServices.getInstance(ProfileActivity.this).setBaseUrl(APIUrl.SERVER + "update_profile")
                                        .addPostParam("name", ((EditText) findViewById(R.id.username)).getText().toString().trim())
                                        .addPostParam("phone", ((EditText) findViewById(R.id.phone)).getText().toString().trim())
                                        .addPostParam("address", "address")
                                        .addPostParam("email", email.getText().toString())
                                        .CallPost(new PostAction() {
                                            @Override
                                            public void whenFinished(String status, String response) throws JSONException {
                                                v.setEnabled(true);
                                                Toast.makeText(ProfileActivity.this, new JSONObject(response)
                                                        .getString("message"), Toast.LENGTH_SHORT).show();

                                            }
                                        });
                            }else {
                                Intent intent = new Intent(ProfileActivity.this,ConfirmNewNumberActivity.class);
                                intent.putExtra("name",((EditText) findViewById(R.id.username)).getText().toString().trim());
                                intent.putExtra("phone",((EditText) findViewById(R.id.phone)).getText().toString().trim());
                                intent.putExtra("email",email.getText().toString());
                                startActivity(intent);
                                finish();
                            }
                        }
                    });

                    findViewById(R.id.go_save_password).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
                                Toast.makeText(getApplicationContext(), "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            final ArabEditText old_pass = findViewById(R.id.old_password);
                            final ArabEditText passsword =findViewById(R.id.password);
                            final ArabEditText password2 = findViewById(R.id.password2);

                            if(old_pass.getText().toString().trim().equals("")){
                                passsword.setHintTextColor(Color.RED);
                                passsword.requestFocus();
                                return;
                            }

                            if(passsword.getText().toString().trim().equals("")){
                                passsword.setHintTextColor(Color.RED);
                                passsword.requestFocus();
                                return;
                            }
                            if(passsword.getText().toString().trim().length()<8){
                                passsword.setHintTextColor(Color.RED);
                                passsword.requestFocus();
                                Toast.makeText(ProfileActivity.this, getString(R.string.pass_hint), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if(!passsword.getText().toString().trim().equals(password2.getText().toString().trim())){
                                password2.setHintTextColor(Color.RED);
                                password2.requestFocus();
                                Toast.makeText(ProfileActivity.this, getString(R.string.pas_dznt_mtch), Toast.LENGTH_SHORT).show();
                                return;
                            }


                            v.setEnabled(false);
                            BackgroundServices.getInstance(ProfileActivity.this).setBaseUrl(APIUrl.SERVER+"update_profile")
                                    .addPostParam("password" ,  ( (EditText) findViewById(R.id.password)).getText().toString().trim())
                                    .addPostParam("old_password",old_pass.getText().toString().trim())
                                    .CallPost(new PostAction() {
                                        @Override
                                        public void whenFinished(String status, String response) throws JSONException {
                                            v.setEnabled(true);
                                            Toast.makeText(ProfileActivity.this, new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();
                                            if (status.equals("200")) {
                                                old_pass.setText("");
                                                passsword.setText("");
                                                password2.setText("");
                                            }
                                        }
                                    });
                        }
                    });

                    findViewById(R.id.loading).setVisibility(View.GONE);
                    findViewById(R.id.main).setVisibility(View.VISIBLE);
                }else{
                    SharedPrefManager.getInstance(ProfileActivity.this).logout();
                    Intent intent=new Intent(ProfileActivity.this , SelectSignupType.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(ProfileActivity.this , CustomerHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
