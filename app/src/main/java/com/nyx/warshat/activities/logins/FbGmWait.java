package com.nyx.warshat.activities.logins;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.helper.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

public class FbGmWait extends GlobalActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fb_gm_wait);
        BackgroundServices.getInstance(FbGmWait.this).setBaseUrl(APIUrl.SERVER+"login")
                .addPostParam("login_type" ,"5")
                .addPostParam("email" ,getIntent().getStringExtra("email"))
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        //  ((EditText)findViewById(R.id.name)).setText(response);
                        if(status.equals("500")) {
                            ((EditText)findViewById(R.id.name)).setText(response);
                        }

                        if(status.equals("200")) {
                            JSONObject data=new JSONObject(response).getJSONObject("data");
                            String token =data.getString("token");
                            SharedPrefManager.getInstance(FbGmWait.this).saveToken(token ,data.getString("user_type"));
                            Intent intent=new Intent(FbGmWait.this , SelectSignupType.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }else{
                            Intent ii = new Intent(FbGmWait.this ,SelectSignupType.class);
                            ii.putExtra("name" ,getIntent().getStringExtra("name"));
                            ii.putExtra("email" ,getIntent().getStringExtra("email"));
                            ii.putExtra("method" ,getIntent().getStringExtra("method"));
                            ii.putExtra("social_id" ,getIntent().getStringExtra("social_id"));
                            startActivity(ii);
                            finish();
                        }
                    }
                });
    }
}
