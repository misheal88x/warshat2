package com.nyx.warshat.activities.homes;

import android.content.Intent;
import android.graphics.Color;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalGreyActivity;
import com.nyx.warshat.activities.edits.NewEditWorkshop;
import com.nyx.warshat.adapters.RequestsAdapter;
import com.nyx.warshat.customes.EndlessScrollListener;
import com.nyx.warshat.customes.EndlessScrollView;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.ConnectionUtils;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.RequestItem;
import com.nyx.warshat.utilities.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class WorkshopHome extends GlobalGreyActivity implements EndlessScrollListener {
    int[] header_icons_blue = new int[]{R.drawable.new_blue_xxxhdpi, R.drawable.started_blue_xxxhdpi,
            R.drawable.finished_blue_xxxhdpi , R.drawable.cancelled_blue};
    int[] header_icons_gray = new int[]{R.drawable.new_requests_gray_xxxhdpi,
            R.drawable.under_construction_gray_xxxhdpi
            , R.drawable.finished_requests_gray_xxxhdpi , R.drawable.cancelled_gray};
    int[] header_image_views = new int[]{R.id.img1, R.id.img2, R.id.img3, R.id.img4};
    int[] header_text_views = new int[]{R.id.txt1, R.id.txt2, R.id.txt3, R.id.txt4};
//-----------


    public RecyclerView mainRecylceView;
    public View loading;
    public RecyclerView.Adapter adapter;
    public ArrayList items;
    public  View noItemsFound,no_more ,not_connected;
    public   TextView no_items_found_text;
    public TextView title;
    EndlessScrollView mainScrollView;
    public FloatingActionButton fab;


    void setActive(int index){
        if(!ConnectionUtils.isNetworkAvailable(this)){
            Toast.makeText(this, "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
            return;
        }
        if(loadingFlag){
           return;
        };
        for(int i=0;i<header_icons_blue.length;i++){
            ((ImageView)findViewById(header_image_views[i])).setImageDrawable(getResources().getDrawable(header_icons_gray[i]));
            ((TextView)findViewById(header_text_views[i])).setTextColor(Color.parseColor("#9d9fa2"));
        }
        ((ImageView)findViewById(header_image_views[index])).setImageDrawable(getResources().getDrawable(header_icons_blue[index]));
        ((TextView)findViewById(header_text_views[index])).setTextColor(getResources().getColor(R.color.colorPrimary));

        items.clear();
        adapter.notifyDataSetChanged();
        max=99999;
        current=0;

        if(index==0){
        status=RequestItem.REQUEST_NEW;

    }
        if(index==1){
            status=RequestItem.REQUEST_STARTED;

        }
        if(index==2){
            status=RequestItem.REQUEST_FINISHED;

        }
        if(index==3){
            status=RequestItem.REQUEST_CANCELLED;

        }
        loading.setVisibility(View.VISIBLE);

        retry();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workshop_home);
        findViewById(R.id.go_new).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setActive(0);
            }
        });
        String token = FirebaseInstanceId.getInstance().getToken();
        BaseFunctions.callUpdateTokenAPI(WorkshopHome.this,token);
        findViewById(R.id.go_started).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setActive(1);
            }
        });
        findViewById(R.id.go_finished).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setActive(2);
            }
        });
        findViewById(R.id.go_cancelled).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setActive(3);
            }
        });

        mainRecylceView = (RecyclerView)findViewById(R.id.recyclerview5);
        mainScrollView = (EndlessScrollView)findViewById(R.id.heart_scroll_view);
        mainScrollView.setScrollViewListener(this);
        noItemsFound= findViewById(R.id.no_items_found);
        not_connected= findViewById(R.id.no_connected_to_internet);
        no_items_found_text= (TextView) findViewById(R.id.no_items_found_text);
        no_more= findViewById(R.id.no_more_to_show);
        loading = findViewById(R.id.loading);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainScrollView.scrollTo(0,0);
            }
        });
        items = new ArrayList();
        mainRecylceView.setLayoutManager(new LinearLayoutManager(this));
        //mainRecylceView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        adapter= new RequestsAdapter(items,this);
        mainRecylceView.setAdapter(adapter);
        max=1000;
        setActive(0);
        findViewById(R.id.go_profile).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
            Toast.makeText(getApplicationContext(), "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
            return;
        }
        startActivity(new Intent(WorkshopHome.this , NewEditWorkshop.class));
    }
});
    }

    public   void  fetchData(){


        loadMore();



    };
    public     int max=100000;
    public   int current=0;
    public String status="-1";
    public boolean loadingFlag = false;
    public   void loadMore(){
        loadingFlag=true;
        if(current>0)findViewById(R.id.loading_pager).setVisibility(View.VISIBLE);
        noItemsFound.setVisibility(View.GONE);
        no_more.setVisibility(View.GONE);
        not_connected.setVisibility(View.GONE);
        BackgroundServices.getInstance(this)
                .setBaseUrl(APIUrl.SERVER + "requests/recieved" )
                .addPostParam("status" ,status)
                .addPostParam("start" ,current+"").CallPost(
                new PostAction() {
                    @Override
                    public void whenFinished(String status ,String response) {
                        loading.setVisibility(View.GONE);
                        loadingFlag = false;
                            if (response.equals("")) {
                                not_connected.setVisibility(View.VISIBLE);
                            } else {
                                try {

                                    findViewById(R.id.loading_pager).setVisibility(View.GONE);
                                    JSONArray data = new JSONObject(response).getJSONArray("data");
                                    if (data.length() > 0) {
                                        for (int i = 0; i < data.length(); i++)
                                            items.add(new RequestItem(data.getJSONObject(i)));
                                        adapter.notifyDataSetChanged();
                                        current += 10;

                                    }
                                    mainScrollView.setVisibility(View.VISIBLE);
                                    if (items.size() == 0) noItemsFound.setVisibility(View.VISIBLE);
                                    else {
                                        if (data.length() == 0) {
                                            no_more.setVisibility(View.VISIBLE);
                                            max = items.size();
                                        }
                                    }
                                } catch (JSONException e) {
                                    Log.i("kjkjkj", "whenFinished: "+e.getMessage());
                                    e.printStackTrace();
                                }
                            }

                    }
                }
        );


    };

    void retry(){
        loading.setVisibility(View.VISIBLE);

        if(!ConnectionUtils.isNetworkAvailable(this)){
            not_connected.setVisibility(View.VISIBLE);
            not_connected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    retry();
                }
            });
            loading.setVisibility(View.GONE);
        }else{
            not_connected.setVisibility(View.GONE);
            fetchData();
        }
    }


    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();
        if(items.size()>=max){
            return;
        }
        if (!loadingFlag) {
            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                loadMore();
            }

        }
    }

}
