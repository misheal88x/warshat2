package com.nyx.warshat.activities.logins;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.yalantis.ucrop.UCrop;

import java.io.File;

import Arabs.ArabButton;

public class RegisterWorkshopStepSeven extends GlobalActivity {

    private ArabButton btn_next,btn_previous;
    private ImageView img_image;

    private final static int SELECT_PHOTO = 12345;
    private final static int PLACE_PICKER_REQUEST = 999;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    String imagePath = "";
    Bitmap bitmap;
    boolean isUpdating = false;
    Uri croppedImg;
    private static final int PICK_FROM_GALLERY = 14436;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_workshop_step_seven);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //Button
        btn_next = findViewById(R.id.next);
        btn_previous = findViewById(R.id.previous);
        //ImageView
        img_image = findViewById(R.id.pick);
    }

    private void init_events() {
        img_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ActivityCompat.checkSelfPermission(RegisterWorkshopStepSeven.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(RegisterWorkshopStepSeven.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    } else {
                        pick();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (imagePath.equals("")){
                    Toast.makeText(RegisterWorkshopStepSeven.this, "يجب عليك اختيار أيقونة الشركة", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(RegisterWorkshopStepSeven.this,RegisterWorkshopStepThreeNew.class);
                intent.putExtra("full_name",getIntent().getStringExtra("full_name"));
                intent.putExtra("mobile",getIntent().getStringExtra("mobile"));
                intent.putExtra("email",getIntent().getStringExtra("email"));
                intent.putExtra("password",getIntent().getStringExtra("password"));
                intent.putExtra("w_full_name",getIntent().getStringExtra("w_full_name"));
                intent.putExtra("w_mobile",getIntent().getStringExtra("w_mobile"));
                intent.putExtra("w_land_line",getIntent().getStringExtra("w_land_line"));
                intent.putExtra("city_id",getIntent().getStringExtra("city_id"));
                intent.putExtra("region_id",getIntent().getStringExtra("region_id"));
                intent.putExtra("api_response",getIntent().getStringExtra("api_response"));
                intent.putExtra("pic",imagePath);
                startActivity(intent);

            }
        });

    }

    private void init_activity() {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pick();
                } else {
                    Toast.makeText(getApplicationContext(), "Not allowed to open gallery", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    void pick() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }

        startActivityForResult(photoPickerIntent, SELECT_PHOTO);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        final Uri resultUri = UCrop.getOutput(data);
                        try {
                            imagePath = resultUri.getPath();
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            bitmap = BitmapFactory.decodeFile(resultUri.getPath(), options);

                            ((ImageView) findViewById(R.id.pick)).setImageBitmap(bitmap);

                        } catch (Exception e) {
                            Log.i("errorshit", "onActivityResult: "+e.getMessage());
                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;


                case SELECT_PHOTO:
                    if (data != null) {
                        Uri pickedImage = data.getData();
                        String destinationFileName = "C_"+System.nanoTime() + ".jpg";


                        UCrop.of(pickedImage, Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(1, 1)
                                .withMaxResultSize(300, 300)
                                .start(RegisterWorkshopStepSeven.this);
                    }
                    break;
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);

        }


    }
}
