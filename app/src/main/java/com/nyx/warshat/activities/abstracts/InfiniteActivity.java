package com.nyx.warshat.activities.abstracts;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import android.os.Bundle;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


import com.nyx.warshat.R;
import com.nyx.warshat.customes.EndlessScrollListener;
import com.nyx.warshat.customes.EndlessScrollView;
import com.nyx.warshat.helper.ConnectionUtils;

import java.util.ArrayList;

public  abstract class InfiniteActivity extends GlobalActivity implements EndlessScrollListener {


    public RecyclerView mainRecylceView;
    public View loading;
    public RecyclerView.Adapter adapter;
    public  ArrayList items;
    public  View noItemsFound,no_more ,not_connected;
    public   TextView no_items_found_text;
     public TextView title;
    EndlessScrollView mainScrollView;
   public  FloatingActionButton fab , sortingFab , fab_new;

void retry(){
    loading.setVisibility(View.VISIBLE);

    if(!ConnectionUtils.isNetworkAvailable(this)){
        not_connected.setVisibility(View.VISIBLE);
        not_connected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retry();
            }
        });
        loading.setVisibility(View.GONE);
    }else{
        not_connected.setVisibility(View.GONE);
        fetchData();
    }
}


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_infinite);
        title= (TextView)findViewById(R.id.title);
        mainRecylceView = (RecyclerView)findViewById(R.id.recyclerview5);
        mainScrollView = (EndlessScrollView)findViewById(R.id.heart_scroll_view);
        mainScrollView.setScrollViewListener(this);
        noItemsFound= findViewById(R.id.no_items_found);
        not_connected= findViewById(R.id.no_connected_to_internet);
        no_items_found_text= (TextView) findViewById(R.id.no_items_found_text);
        no_more= findViewById(R.id.no_more_to_show);
        loading = findViewById(R.id.loading);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab_new = (FloatingActionButton) findViewById(R.id.fab_new);
        sortingFab = (FloatingActionButton) findViewById(R.id.order_by_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainScrollView.scrollTo(0,0);
            }
        });
        items = new ArrayList();
       retry();

    }









    public  abstract void  fetchData();
    public     int max=100000;
    public   int current=0;
    public boolean loadingFlag = false;
    public  abstract void loadMore();



    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();
        if(items.size()>=max){
            return;
        }
        if (!loadingFlag) {
            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                loadMore();
            }

        }
    }

}
