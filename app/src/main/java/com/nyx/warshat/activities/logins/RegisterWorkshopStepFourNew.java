package com.nyx.warshat.activities.logins;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.warshat.Intrafaces.ISelected;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.CustomSpinnerAdapter;
import com.nyx.warshat.adapters.RegionsAdapter;
import com.nyx.warshat.models.CityObject;
import com.nyx.warshat.models.RegionObject;
import com.nyx.warshat.models.SelectedRegion;
import com.nyx.warshat.utilities.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Arabs.ArabButton;

public class RegisterWorkshopStepFourNew extends GlobalActivity implements ISelected {

    private Spinner city_spinner;
    private List<String> city_list_string;
    private List<CityObject> city_list;
    private List<RegionObject> region_list;
    private List<SelectedRegion> selectedRegions;
    private CustomSpinnerAdapter city_adapter;
    private RecyclerView rv_regions;
    private RegionsAdapter regionsAdapter;
    private LinearLayoutManager regionsLayoutManager;
    private ArabButton btn_next,btn_previous;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_workshop_step_four_new);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //Spinner
        city_spinner = findViewById(R.id.cities_spinner);
        //RecyclerView
        rv_regions = findViewById(R.id.regions_reycler);
        //Button
        btn_next = findViewById(R.id.next);
        btn_previous = findViewById(R.id.previous);
        selectedRegions = new ArrayList<>();
    }

    private void init_events() {
        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = BaseFunctions.getSpinnerPosition(parent,position);
                if (city_list.size()>0){
                    CityObject co = city_list.get(pos);
                    if (co.getRegions()!=null){
                        if (co.getRegions().size()>0){
                            region_list = new ArrayList<>();
                            for (RegionObject ro : co.getRegions()){
                                region_list.add(ro);
                            }
                            rv_regions.setAdapter(new RegionsAdapter(
                                    RegisterWorkshopStepFourNew.this,
                                    region_list,
                                    selectedRegions, RegisterWorkshopStepFourNew.this));
                        }
                        else {
                            region_list = new ArrayList<>();
                            rv_regions.setAdapter(new RegionsAdapter(
                                    RegisterWorkshopStepFourNew.this,
                                    region_list,
                                    selectedRegions, RegisterWorkshopStepFourNew.this));
                        }
                    }else {
                        region_list = new ArrayList<>();
                        rv_regions.setAdapter(new RegionsAdapter(
                                RegisterWorkshopStepFourNew.this,
                                region_list,
                                selectedRegions, RegisterWorkshopStepFourNew.this));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = new Gson().toJson(selectedRegions);
                if (selectedRegions.size()==0){
                    Toast.makeText(RegisterWorkshopStepFourNew.this, "يجب عليك اختيار المناطق", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(RegisterWorkshopStepFourNew.this,
                        RegisterWorkshopStepFiveNew.class);
                intent.putExtra("full_name", getIntent().getStringExtra("full_name"));
                intent.putExtra("mobile", getIntent().getStringExtra("mobile"));
                intent.putExtra("email", getIntent().getStringExtra("email"));
                intent.putExtra("password", getIntent().getStringExtra("password"));
                intent.putExtra("w_full_name", getIntent().getStringExtra("w_full_name"));
                intent.putExtra("w_mobile", getIntent().getStringExtra("w_mobile"));
                intent.putExtra("w_land_line", getIntent().getStringExtra("w_land_line"));
                intent.putExtra("city_id", getIntent().getStringExtra("city_id"));
                intent.putExtra("region_id", getIntent().getStringExtra("region_id"));
                intent.putExtra("api_response", getIntent().getStringExtra("api_response"));
                intent.putExtra("cat_id", getIntent().getStringExtra("cat_id"));
                intent.putExtra("desc", getIntent().getStringExtra("desc"));
                intent.putExtra("pic", getIntent().getStringExtra("pic"));
                intent.putExtra("regions_json",new Gson().toJson(selectedRegions));
                    startActivity(intent);

            }
        });
    }

    private void init_activity() {
        init_spinner();
        init_recycler();
        try {
            JSONObject all = new JSONObject(getIntent().getStringExtra("api_response")).getJSONObject("data");
            JSONArray cities = all.getJSONArray("cities");
            if (cities!=null){
                if (cities.length()>0){
                    for (int i = 0; i < cities.length(); i++) {
                        List<RegionObject> temp = new ArrayList<>();
                        if (cities.getJSONObject(i).getJSONArray("regions")!=null){
                            if (cities.getJSONObject(i).getJSONArray("regions").length()>0){
                                for (int j = 0; j < cities.getJSONObject(i).getJSONArray("regions").length(); j++) {
                                    temp.add(new RegionObject(
                                            cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getInt("id"),
                                            cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getString("name"),
                                            cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getInt("city_id")
                                    ));
                                }
                            }
                        }
                        CityObject co = new CityObject(
                                cities.getJSONObject(i).getInt("id"),
                                cities.getJSONObject(i).getString("name"),
                                temp);
                        city_list.add(co);
                        city_list_string.add(co.getName());
                    }
                    BaseFunctions.init_spinner(RegisterWorkshopStepFourNew.this,city_spinner,city_adapter,city_list_string);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void init_spinner(){
        city_list_string = new ArrayList<>();
        city_list = new ArrayList<>();

        BaseFunctions.init_spinner(RegisterWorkshopStepFourNew.this,city_spinner,city_adapter,city_list_string);
    }

    private void init_recycler(){
        region_list = new ArrayList<>();
        regionsAdapter = new RegionsAdapter(RegisterWorkshopStepFourNew.this, region_list, selectedRegions, this);
        regionsLayoutManager = new LinearLayoutManager(RegisterWorkshopStepFourNew.this,
                LinearLayoutManager.VERTICAL,false);
        rv_regions.setLayoutManager(regionsLayoutManager);
        rv_regions.setAdapter(regionsAdapter);
    }



    @Override
    public void checked(int id) {
        selectedRegions.add(new SelectedRegion(id));
    }

    @Override
    public void not_checked(int id) {
        for (int i = 0; i < selectedRegions.size() ; i++) {
            if (selectedRegions.get(i).getId()==id){
                selectedRegions.remove(i);
            }
        }
    }
}
