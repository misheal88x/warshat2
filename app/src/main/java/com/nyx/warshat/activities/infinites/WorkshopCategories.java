package com.nyx.warshat.activities.infinites;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.searchs.SearchWorkshops;
import com.nyx.warshat.adapters.CategoryAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.ConnectionUtils;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.Category;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class WorkshopCategories extends GlobalActivity {



    void loadData(){
        if(!ConnectionUtils.isNetworkAvailable(this)) {
            findViewById(R.id.no_connected_to_internet).setVisibility(View.VISIBLE);
            findViewById(R.id.loading).setVisibility(View.GONE);

            return;
        }
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
findViewById(R.id.go_search).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent(WorkshopCategories.this,SearchWorkshops.class);
        intent.putExtra("is_emergency",getIntent().getBooleanExtra("is_emergency",false));
        startActivity(intent);
    }
});
        findViewById(R.id.no_connected_to_internet).setVisibility(View.GONE);
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"category/all?target=32&critical_regions="+
                (getIntent().getBooleanExtra("is_emergency",false)?"on":"off"))
                .CallGet(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        findViewById(R.id.loading).setVisibility(View.GONE);
                        JSONArray all = new JSONObject(response).getJSONArray("data");
                        ArrayList<Category> cats = new ArrayList<>();
                        for(int i=0;i<all.length();i++)cats.add(new Category(all.getJSONObject(i)));
                        RecyclerView rec = (RecyclerView)findViewById(R.id.main);
                        rec.setLayoutManager(new GridLayoutManager(WorkshopCategories.this ,2));
                        rec.setAdapter(new CategoryAdapter(cats ,WorkshopCategories.this ,
                                getIntent().getBooleanExtra("is_emergency",false)));
                    }
                });
    }
    //no_connected_to_internet
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_workshop_categories);
        findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
loadData();
findViewById(R.id.no_connected_to_internet).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        loadData();
    }
});
    }
}
