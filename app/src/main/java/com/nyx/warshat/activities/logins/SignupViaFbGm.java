package com.nyx.warshat.activities.logins;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.helper.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

public class SignupViaFbGm extends GlobalActivity {

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_via_fb_gm);
        ((TextView)findViewById(R.id.menam)).setText(getString(R.string.welcome) + " "+getIntent().getStringExtra("name"));

findViewById(R.id.continue_as_w).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        go("2" ,getIntent().getStringExtra("name") ,
                getIntent().getStringExtra("email") ,(EditText)findViewById(R.id.phone) ,v);
    }
});
        findViewById(R.id.continue_as_c).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                go("1" ,getIntent().getStringExtra("name") ,
                        getIntent().getStringExtra("email") ,(EditText)findViewById(R.id.phone) ,v);
            }
        });

    }

    void go(final String type , String name , String email , final EditText phone , View v){


        if(phone.getText().toString().trim().equals("")){
            phone.setHintTextColor(Color.RED);
            phone.requestFocus();
            return;
        }


        if(!phone.getText().toString().trim().startsWith("09") || phone.getText().toString().trim().length()!=10){
            phone.setHintTextColor(Color.RED);
            phone.requestFocus();
            Toast.makeText(SignupViaFbGm.this, getString(R.string.phone_09), Toast.LENGTH_SHORT).show();
            return;
        }
        findViewById(R.id.continue_as_w).setEnabled(false);
        findViewById(R.id.continue_as_c).setEnabled(false);


        BackgroundServices.getInstance(SignupViaFbGm.this).setBaseUrl(APIUrl.SERVER+"login")
                .addPostParam("login_type" ,"6")
                .addPostParam("user_type" ,type)
                .addPostParam("name" ,getIntent().getStringExtra("name"))
                .addPostParam("phone" ,phone.getText().toString().trim())
                .addPostParam("email" ,getIntent().getStringExtra("email"))

                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        //  ((EditText)findViewById(R.id.name)).setText(response);
                        if(status.equals("500")) {
                            ((EditText)findViewById(R.id.name)).setText(response);
                        }

                        if(status.equals("200")) {
                            JSONObject data=new JSONObject(response).getJSONObject("data");
                            String token =data.getString("token");
                            SharedPrefManager.getInstance(SignupViaFbGm.this).saveToken(token ,type);
                            SharedPrefManager.getInstance(SignupViaFbGm.this).savePhoneNumber(phone.getText().toString());
                            Intent intent=new Intent(SignupViaFbGm.this , SelectSignupType.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }else{
                            Toast.makeText(SignupViaFbGm.this, new JSONObject(response).getString("message"),
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
