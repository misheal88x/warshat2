package com.nyx.warshat.activities.services;

/**
 * Created by Luminance on 5/4/2018.
 */


import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.logins.SelectSignupType;
import com.nyx.warshat.helper.SharedPrefManager;


public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "FBE3LANI";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());

        Intent resultIntent = new Intent(getApplicationContext(), SelectSignupType.class);


        NotificationManager notificationManager;
        Notification myNotification;
        Context context = this;
        PendingIntent pendingIntent = PendingIntent.getActivity(
                context,
                1,
                resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        myNotification = new NotificationCompat.Builder(context)
                .setContentTitle(remoteMessage.getNotification().getTitle().toString())
                .setContentText(remoteMessage.getNotification().getBody().toString())
                .setWhen(System.currentTimeMillis())
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .setChannelId("testChannelId") // set channel id
                .setSmallIcon(R.mipmap.ic_launcher)
                .build();


        notificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify((int) System.currentTimeMillis(), myNotification);


    }

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);

            FirebaseMessaging.getInstance().subscribeToTopic("public");
    }

    private void storeRegIdInPref(String token) {
        Log.d("FBE3LANI","firebase token generated");
        SharedPrefManager.getInstance(this).saveData(token ,SharedPrefManager.FIREBASE_TOKEN);



    }

    /*
    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        String refreshedToken = s;
        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);
        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);
        // Notify UI that registration has completed, so the progress indicator can be hidden.

    }
    **/

}