package com.nyx.warshat.activities.logins;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.yalantis.ucrop.UCrop;

import java.io.File;

public class SignupWorkshopStepTwo extends GlobalActivity {
    private final static int SELECT_PHOTO = 12345;
    private final static int PLACE_PICKER_REQUEST = 999;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    String imagePath = "";
    Bitmap bitmap;
    boolean isUpdating = false;
    Uri croppedImg;
    private static final int PICK_FROM_GALLERY = 14436;





    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pick();
                } else {
                    Toast.makeText(getApplicationContext(), "Not allowed to open gallery", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }



    void pick() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }

        startActivityForResult(photoPickerIntent, SELECT_PHOTO);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("RESULTED ", requestCode + " and " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        Log.e("GOTCHA ", "yes");
                        final Uri resultUri = UCrop.getOutput(data);

                        try {
                            imagePath = resultUri.getPath();
                            BitmapFactory.Options options = new BitmapFactory.Options();
                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                            bitmap = BitmapFactory.decodeFile(resultUri.getPath(), options);

                                ((ImageView) findViewById(R.id.pic)).setImageBitmap(bitmap);

                        } catch (Exception e) {

                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                        }
                        // Do something with the bitmap
                        // At the end remember to close the cursor or you will end with the RuntimeException!
                    }
                    break;


                case SELECT_PHOTO:
                    if (data != null) {
                        Uri pickedImage = data.getData();
                        String destinationFileName = "C_"+System.nanoTime() + ".jpg";


                        UCrop.of(pickedImage, Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(1, 1)
                                .withMaxResultSize(300, 300)
                                .start(SignupWorkshopStepTwo.this);
                    }
                    break;
                case VerifyPhoneActivity.VERIVY_PHONE:
                    if(data.getBooleanExtra("result" ,true)){
                        Toast.makeText(this, "تم التحقق من الرقم بنجاح", Toast.LENGTH_SHORT).show();
                        is_phone_verified = true;
                    }

                    break;


            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.e("CROP ERR", cropError.getMessage());

        }


    }







    boolean is_phone_verified = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_workshop_step_two);


        findViewById(R.id.pic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ActivityCompat.checkSelfPermission(SignupWorkshopStepTwo.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SignupWorkshopStepTwo.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    } else {
                        pick();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        if(getIntent().getStringExtra("name")!=null)((EditText)findViewById(R.id.name)).setText(getIntent().getStringExtra("name"));
        findViewById(R.id.continue_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              if(!((CheckBox)findViewById(R.id.terms_accepting)).isChecked()){
                  Toast.makeText(SignupWorkshopStepTwo.this, getString(R.string.terms_acc),
                          Toast.LENGTH_SHORT).show();
                  findViewById(R.id.terms_accepting).requestFocus();
                  return;
              }

              if(imagePath.equals("")){
                  Toast.makeText(SignupWorkshopStepTwo.this, getString(R.string.select_pic),
                          Toast.LENGTH_SHORT).show();
                  findViewById(R.id.pic).requestFocus();

                  return;
              }
                EditText name = ((EditText)findViewById(R.id.name));
                EditText address = ((EditText)findViewById(R.id.address));
                EditText phone = ((EditText)findViewById(R.id.phone));
                EditText landline = ((EditText)findViewById(R.id.landline));
                EditText passsword = ((EditText)findViewById(R.id.password));
                EditText password2 = ((EditText)findViewById(R.id.password2));
                
                if(name.getText().toString().trim().equals("")){
                    name.setHintTextColor(Color.RED);
                    name.requestFocus();
                    return;
                }
                if(address.getText().toString().trim().equals("")){
                    address.setHintTextColor(Color.RED);
                    address.requestFocus();
                    return;
                }
                if(phone.getText().toString().trim().equals("")){
                    phone.setHintTextColor(Color.RED);
                    phone.requestFocus();
                    return;
                }

                if(!phone.getText().toString().trim().startsWith("09") || phone.getText().toString().trim().length()!=10){
                    phone.setHintTextColor(Color.RED);
                    phone.requestFocus();
                    Toast.makeText(SignupWorkshopStepTwo.this, getString(R.string.phone_09), Toast.LENGTH_SHORT).show();
                    return;
                }


                if(landline.getText().toString().trim().equals("")){
                    landline.setHintTextColor(Color.RED);
                    landline.requestFocus();
                    return;
                }
                if(passsword.getText().toString().trim().equals("")){
                    passsword.setHintTextColor(Color.RED);
                    passsword.requestFocus();
                    return;
                }
                if(passsword.getText().toString().trim().length()<8){
                    passsword.setHintTextColor(Color.RED);
                    passsword.requestFocus();
                    Toast.makeText(SignupWorkshopStepTwo.this, getString(R.string.pass_hint), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!passsword.getText().toString().trim().equals(password2.getText().toString().trim())){
                    password2.setHintTextColor(Color.RED);
                    password2.requestFocus();
                    Toast.makeText(SignupWorkshopStepTwo.this, getString(R.string.pas_dznt_mtch), Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!is_phone_verified){
                    Toast.makeText(SignupWorkshopStepTwo.this, "قم بالتحقق من رقم الهاتف", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(SignupWorkshopStepTwo.this, VerifyPhoneActivity.class);
                    startActivityForResult(i, VerifyPhoneActivity.VERIVY_PHONE);
                    return;
                }

                Intent ii=new Intent(SignupWorkshopStepTwo.this ,SignupWorkshopStepThree.class);
             ii.putExtra("name" ,name.getText().toString().trim());
             ii.putExtra("phone" ,phone.getText().toString().trim());
             ii.putExtra("landline" ,landline.getText().toString().trim());
             ii.putExtra("address" ,address.getText().toString().trim());
             ii.putExtra("password" ,passsword.getText().toString().trim());
             ii.putExtra("work_field" ,getIntent().getStringExtra("work_field"));
                ii.putExtra("method" ,getIntent().getStringExtra("method"));
                ii.putExtra("social_id" ,getIntent().getStringExtra("social_id"));
                ii.putExtra("email" ,getIntent().getStringExtra("email"));


             ii.putExtra("pic" ,imagePath);

                startActivity(ii);
            }
        });
    }
}
