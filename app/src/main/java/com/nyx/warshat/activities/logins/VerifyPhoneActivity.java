package com.nyx.warshat.activities.logins;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.nyx.warshat.R;

public class VerifyPhoneActivity extends AppCompatActivity {
    public static final int VERIVY_PHONE = 98769;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_phone);
        findViewById(R.id.go_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",true);
                setResult(Activity.RESULT_OK,returnIntent);
                finish();
            }
        });

    }
}
