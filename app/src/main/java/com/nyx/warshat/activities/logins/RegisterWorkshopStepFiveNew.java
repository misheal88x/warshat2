package com.nyx.warshat.activities.logins;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;

import Arabs.ArabButton;
import Arabs.ArabCheckbox;

public class RegisterWorkshopStepFiveNew extends GlobalActivity {

    private ArabCheckbox chk_yes,chk_no;
    private ArabButton btn_next,btn_previous;
    private int selected_choice = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_workshop_step_five_new);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //Checkbox
        chk_yes = findViewById(R.id.yes);
        chk_no = findViewById(R.id.no);
        //Button
        btn_next = findViewById(R.id.next);
        btn_previous = findViewById(R.id.previous);
    }

    private void init_events() {
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        chk_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    selected_choice = 2;
                    chk_yes.setChecked(false);
                    btn_next.setText("تسجيل");
                }
            }
        });
        chk_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    selected_choice = 1;
                    chk_no.setChecked(false);
                    btn_next.setText("التالي");
                }
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_choice == 0){
                    Toast.makeText(RegisterWorkshopStepFiveNew.this, "يجب عليك اختيار أحد الخيارين أولا", Toast.LENGTH_SHORT).show();
                    return;
                }
                else if (selected_choice == 1){
                    Intent intent = new Intent(RegisterWorkshopStepFiveNew.this,RegisterWorkshopStepSixNew.class);
                    intent.putExtra("full_name", getIntent().getStringExtra("full_name"));
                    intent.putExtra("mobile", getIntent().getStringExtra("mobile"));
                    intent.putExtra("email", getIntent().getStringExtra("email"));
                    intent.putExtra("password", getIntent().getStringExtra("password"));
                    intent.putExtra("w_full_name", getIntent().getStringExtra("w_full_name"));
                    intent.putExtra("w_mobile", getIntent().getStringExtra("w_mobile"));
                    intent.putExtra("w_land_line", getIntent().getStringExtra("w_land_line"));
                    intent.putExtra("city_id", getIntent().getStringExtra("city_id"));
                    intent.putExtra("region_id", getIntent().getStringExtra("region_id"));
                    intent.putExtra("api_response", getIntent().getStringExtra("api_response"));
                    intent.putExtra("cat_id", getIntent().getStringExtra("cat_id"));
                    intent.putExtra("desc", getIntent().getStringExtra("desc"));
                    intent.putExtra("regions_json",getIntent().getStringExtra("regions_json"));
                    intent.putExtra("pic", getIntent().getStringExtra("pic"));
                    startActivity(intent);
                }
                else {
                    Intent intent = new Intent(RegisterWorkshopStepFiveNew.this,ConfirmAccountActivity.class);
                    intent.putExtra("full_name", getIntent().getStringExtra("full_name"));
                    intent.putExtra("user_type","2");
                    intent.putExtra("phone", getIntent().getStringExtra("mobile"));
                    intent.putExtra("email", getIntent().getStringExtra("email"));
                    intent.putExtra("password", getIntent().getStringExtra("password"));
                    intent.putExtra("w_full_name", getIntent().getStringExtra("w_full_name"));
                    intent.putExtra("w_mobile", getIntent().getStringExtra("w_mobile"));
                    intent.putExtra("w_land_line", getIntent().getStringExtra("w_land_line"));
                    intent.putExtra("city_id", getIntent().getStringExtra("city_id"));
                    intent.putExtra("region_id", getIntent().getStringExtra("region_id"));
                    intent.putExtra("api_response", getIntent().getStringExtra("api_response"));
                    intent.putExtra("cat_id", getIntent().getStringExtra("cat_id"));
                    intent.putExtra("desc", getIntent().getStringExtra("desc"));
                    intent.putExtra("regions_json",getIntent().getStringExtra("regions_json"));
                    intent.putExtra("pic", getIntent().getStringExtra("pic"));
                    intent.putExtra("urgent_regions_json","[]");
                    startActivity(intent);
                }
            }
        });
    }

    private void init_activity() {
    }
}
