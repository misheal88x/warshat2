package com.nyx.warshat.activities.logins;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.splashes.SplashActivity;

public class WelcomeActivity extends GlobalActivity {
    private String[] texts ;
    private int[] pics  ,indicators;
    private int index=0;
    private ImageView img_1,img_2,img_3,img_4,img_5;
    private void set(){
        if(index<0){
            finish();
            return;
        }

    if(index>pics.length-1){
        Intent intent = new Intent(WelcomeActivity.this,SplashActivity.class);
        intent.putExtra("account_type","customer");
        startActivity(intent);
        finish();
        return;
        /*
        Intent ii = new Intent(WelcomeActivity.this , SignupAsCustomer.class);
//        ii.putExtra("method" ,getIntent().getStringExtra("method"));
//        ii.putExtra("social_id" ,getIntent().getStringExtra("social_id"));
//        ii.putExtra("email" ,getIntent().getStringExtra("email"));
//        ii.putExtra("name" ,getIntent().getStringExtra("name"));

        startActivity(ii);
        finish();
        return;
**/
    }
    if(index==pics.length-1){
        ((TextView) findViewById(R.id.go_next)).setText("متابعة");

    }else
        ((TextView) findViewById(R.id.go_next)).setText("التالي");


    ((TextView)findViewById(R.id.info)).setText(texts[index]);
    ((ImageView)findViewById(R.id.pic)).setImageDrawable(getResources().getDrawable(pics[index]));

    //((ImageView)findViewById(R.id.ind)).setImageDrawable(getResources().getDrawable(indicators[index]));

        setIndicators(index);
}

    private void setIndicators(int i){
        ImageView[] imgs = new ImageView[]{img_1,img_2,img_3,img_4,img_5};
        imgs[i].setImageResource(R.drawable.ic_yellow_circle);
        for (int j = 0; j < 5; j++) {
            if (j!=i){
                imgs[j].setImageResource(R.drawable.ic_blue_circle);
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        img_1 = findViewById(R.id.slider_image_1);
        img_2 = findViewById(R.id.slider_image_2);
        img_3 = findViewById(R.id.slider_image_3);
        img_4 = findViewById(R.id.slider_image_4);
        img_5 = findViewById(R.id.slider_image_5);
       texts  =new String[]{
                "تصليحة يتيح لك طلب ورشات لتنفيذ أعمال الصيانة بأنواعها",
                "اضغط زر طلب ورشة و اختر مجال عمل الورشة",
                "اختر الورشة المناسبة لك و تواصل معها بشكل مباشر",
                "في حالات الطوارئ اضغط زر \"لدي حالة طارئة\"",
                "ثم اختيار نوع الورشة لتظهر لك الورشات المستعدة لتلبية حالات الطوارئ",
        };
      pics =new int[]{R.drawable.intro_1 ,R.drawable.intro_2 ,R.drawable.intro_3,R.drawable.intro_4,R.drawable.intro_5 };
        indicators =new int[]{R.drawable.dots_1 ,R.drawable.dots_2 ,R.drawable.dots_3 ,R.drawable.dots_4 ,R.drawable.dots_5 };

findViewById(R.id.go_next).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        index++;
        set();
    }
});
        findViewById(R.id.go_prev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index--;
                set();
            }
        });
set();

    }
}
