package com.nyx.warshat.activities.homes;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.firebase.iid.FirebaseInstanceId;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.infinites.CompanyCategories;
import com.nyx.warshat.activities.infinites.WorkshopCategories;
import com.nyx.warshat.activities.searchs.SearchWorkshops;
import com.nyx.warshat.utilities.BaseFunctions;
import com.nyx.warshat.utilities.FooterHelper;

public class CustomerHome extends GlobalActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_home);
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.i("generated_fcm_token", "onCreate: "+token);
        BaseFunctions.callUpdateTokenAPI(CustomerHome.this,token);
        findViewById(R.id.go_request_Workshop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomerHome.this , WorkshopCategories.class);
                intent.putExtra("is_emergency",false);
                startActivity(intent);
            }
        });   findViewById(R.id.go_emergency).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomerHome.this , WorkshopCategories.class);
                intent.putExtra("is_emergency",true);
                startActivity(intent);
            }
        });  findViewById(R.id.go_companies).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerHome.this , CompanyCategories.class));
            }
        });
        findViewById(R.id.go_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CustomerHome.this,SearchWorkshops.class);
                intent.putExtra("is_emergency",false);
                startActivity(intent);
            }
        });
        //EditText editText  = (EditText)findViewById(R.id.search_input) ;
        FooterHelper.initFooterForCustomer(this);
//        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
//                    Intent ii = new Intent(CustomerHome.this , SearchResults.class);
//                    ii.putExtra("name" ,v.getText().toString().trim());
//                    startActivity(ii);
//                   // performSearch();
//                    v.setText("");
//
//                    return true;
//                }
//                return false;
//            }
//        });
    }
}
