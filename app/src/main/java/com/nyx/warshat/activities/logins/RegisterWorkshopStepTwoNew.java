package com.nyx.warshat.activities.logins;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.StringArrayAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.ConnectionUtils;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.CityObject;
import com.nyx.warshat.models.RegionObject;
import com.nyx.warshat.utilities.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Arabs.ArabButton;
import Arabs.ArabEditText;

public class RegisterWorkshopStepTwoNew extends GlobalActivity {
    private static JSONArray cities ;
    private ArabEditText edt_full_name,edt_phone_number,edt_land_line;
    private Spinner city_spinner,regions_spinner;
    private ArabButton btn_next,btn_previous,btn_reconnect;

    private List<RegionObject> regions_list;
    private List<CityObject> cities_list;
    private List<String> regions_list_string;
    private String selected_region_id = "",selected_city_id = "";
    private String api_response = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_workshop_step_two_new);
        init_views();
        init_events();
        init_activity();

    }

    private void init_views() {
        //EditText
        edt_full_name = findViewById(R.id.w_full_name);
        edt_phone_number = findViewById(R.id.w_mobile_number);
        edt_land_line = findViewById(R.id.w_land_line);
        //Spinner
        city_spinner = findViewById(R.id.city_spinner);
        regions_spinner = findViewById(R.id.region_spinner);
        //Button
        btn_previous = findViewById(R.id.previous);
        btn_next = findViewById(R.id.next);
        btn_reconnect = findViewById(R.id.reconnect);

    }

    private void init_events() {
        btn_reconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ConnectionUtils.isNetworkAvailable(RegisterWorkshopStepTwoNew.this))
                    init();
                else
                    findViewById(R.id.reconnect).setVisibility(View.VISIBLE);
            }
        });
        cities_list = new ArrayList<>();
        regions_list = new ArrayList<>();
        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = BaseFunctions.getSpinnerPosition(parent,position);
                if (cities_list.size()>0){
                    CityObject city = cities_list.get(pos);
                    if (city.getRegions().size()>0){
                        regions_list = new ArrayList<>();
                        regions_list_string = new ArrayList<>();
                        for (RegionObject ro : city.getRegions()){
                            regions_list.add(ro);
                            regions_list_string.add(ro.getName());
                        }
                        String[] temp = new String[0];
                        if (regions_list_string.size() > 0){
                            temp = new String[regions_list_string.size()];
                            for (int i = 0; i < regions_list_string.size(); i++) {
                                temp[i] = regions_list_string.get(i);
                            }
                        }
                        regions_spinner.setAdapter(new StringArrayAdapter(RegisterWorkshopStepTwoNew.this,
                                temp));
                    }else {
                        selected_region_id = "";
                        String[] temp = new String[0];
                        regions_spinner.setAdapter(new StringArrayAdapter(RegisterWorkshopStepTwoNew.this,
                                temp));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        regions_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = BaseFunctions.getSpinnerPosition(parent,position);
                if (regions_list.size()>0){
                    selected_region_id = String.valueOf(regions_list.get(pos).getId());
                }else {
                    selected_region_id = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edt_full_name.getText().toString().trim().equals("")){
                    edt_full_name.setHintTextColor(Color.RED);
                    edt_full_name.requestFocus();
                    return;
                }
                if (edt_phone_number.getText().toString().trim().equals("")){
                    edt_phone_number.setHintTextColor(Color.RED);
                    edt_phone_number.requestFocus();
                    return;
                }
                if(!edt_phone_number.getText().toString().trim().startsWith("09") || edt_phone_number.getText().toString().trim().length()!=10){
                    edt_phone_number.setHintTextColor(Color.RED);
                    edt_phone_number.requestFocus();
                    Toast.makeText(RegisterWorkshopStepTwoNew.this, getString(R.string.phone_09), Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_land_line.getText().toString().trim().equals("")){
                    edt_land_line.setHintTextColor(Color.RED);
                    edt_land_line.requestFocus();
                    return;
                }
                if (selected_region_id.equals("")){
                    Toast.makeText(RegisterWorkshopStepTwoNew.this, "يجب عليك اختيار المنطقة قبل المتابعة", Toast.LENGTH_SHORT).show();
                    return;
                }
                try {
                    selected_city_id = cities.getJSONObject( city_spinner.getSelectedItemPosition()).getString("id");

                    Intent intent = new Intent(RegisterWorkshopStepTwoNew.this,RegisterWorkshopStepSeven.class);
                    intent.putExtra("full_name",getIntent().getStringExtra("full_name"));
                    intent.putExtra("mobile",getIntent().getStringExtra("mobile"));
                    intent.putExtra("email",getIntent().getStringExtra("email"));
                    intent.putExtra("password",getIntent().getStringExtra("password"));
                    intent.putExtra("w_full_name",edt_full_name.getText().toString());
                    intent.putExtra("w_mobile",edt_phone_number.getText().toString());
                    intent.putExtra("w_land_line",edt_land_line.getText().toString());
                    intent.putExtra("city_id",selected_city_id);
                    intent.putExtra("region_id",selected_region_id);
                    intent.putExtra("api_response",api_response);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void init_activity() {
        if(ConnectionUtils.isNetworkAvailable(this))
            init();
        else{
            btn_reconnect.setVisibility(View.VISIBLE);
        }
        if(cities!=null){
            String[] c_names = new String[cities.length()];

            for (int i = 0; i < cities.length(); i++) {
                try {
                    List<RegionObject> temp = new ArrayList<>();
                    if (cities.getJSONObject(i).getJSONArray("regions").length()>0){
                        for (int j = 0; j <cities.getJSONObject(i).getJSONArray("regions").length() ; j++) {
                            temp.add(new RegionObject(
                                    cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getInt("id"),
                                    cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getString("name"),
                                    cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getInt("city_id")
                            ));
                        }
                    }
                    cities_list.add(new CityObject(
                            cities.getJSONObject(i).getInt("id"),
                            cities.getJSONObject(i).getString("name"),
                            temp
                    ));
                    c_names[i] = cities.getJSONObject(i).getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            city_spinner.setAdapter(new StringArrayAdapter(RegisterWorkshopStepTwoNew.this, c_names));
        }else {
            city_spinner.setAdapter(new StringArrayAdapter(RegisterWorkshopStepTwoNew.this, new String[]{"المدينة"}));
            regions_spinner.setAdapter(new StringArrayAdapter(RegisterWorkshopStepTwoNew.this, new String[]{"المنطقة"}));
            init();
        }
    }

    private void init(){
        btn_reconnect.setVisibility(View.GONE);
        btn_next.setEnabled(false);
        final ProgressDialog dialog = new ProgressDialog(RegisterWorkshopStepTwoNew.this);
        dialog.setMessage("يرجى الانتظار..");
        dialog.setCancelable(false);
        dialog.show();
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"ws_init?filter=c32").CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                dialog.cancel();
                if(status.equals("500") || response.equals("")){
                    findViewById(R.id.reconnect).setVisibility(View.VISIBLE);

                    int maxLogSize = 5000;
                    for(int i = 0; i <= response.length() / maxLogSize; i++) {
                        int start = i * maxLogSize;
                        int end = (i+1) * maxLogSize;
                        end = end > response.length() ? response.length() : end;
                        Log.d("LONGAGO", response.substring(start, end));
                    }


                }else {
                    btn_next.setEnabled(true);
                    api_response = response;
                    JSONObject all = new JSONObject(response).getJSONObject("data");
                    cities = all.getJSONArray("cities");
                    regions_list_string = new ArrayList<>();
                    regions_list = new ArrayList<>();
                    cities_list = new ArrayList<>();
                    String[] c_names = new String[cities.length()];
                    String[] regions = new String[0];
                    for (int i = 0; i < cities.length(); i++) {
                        List<RegionObject> temp = new ArrayList<>();
                        if (cities.getJSONObject(i).getJSONArray("regions").length()>0){
                            for (int j = 0; j <cities.getJSONObject(i).getJSONArray("regions").length() ; j++) {
                                temp.add(new RegionObject(
                                        cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getInt("id"),
                                        cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getString("name"),
                                        cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getInt("city_id")
                                ));
                            }
                        }
                        cities_list.add(new CityObject(
                                cities.getJSONObject(i).getInt("id"),
                                cities.getJSONObject(i).getString("name"),
                                temp
                        ));
                        c_names[i] = cities.getJSONObject(i).getString("name");
                    }
                    city_spinner.setAdapter(new
                            StringArrayAdapter(RegisterWorkshopStepTwoNew.this, c_names));
                    regions_spinner.setAdapter(new
                            StringArrayAdapter(RegisterWorkshopStepTwoNew.this, regions));
                }

            }
        });
    }
}
