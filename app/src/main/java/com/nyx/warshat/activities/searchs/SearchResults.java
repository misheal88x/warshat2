package com.nyx.warshat.activities.searchs;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.CompaniesAdapter;
import com.nyx.warshat.adapters.WorkshopsAdapter;
import com.nyx.warshat.customes.EndlessScrollListener;
import com.nyx.warshat.customes.EndlessScrollView;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.ConnectionUtils;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.Workshop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import Arabs.ArabTextView;

public class SearchResults  extends GlobalActivity implements EndlessScrollListener {


    public RecyclerView mainRecylceView;
    public View loading;
    public RecyclerView.Adapter adapter;
    public ArrayList items;
    public  View noItemsFound,no_more ,not_connected;
    public TextView no_items_found_text;
    public TextView title;
    EndlessScrollView mainScrollView;
    public FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        if (getIntent().getStringExtra("type").equals("workshop")){
            ((ArabTextView)findViewById(R.id.name)).setText("بحث الورشات");
            ((ArabTextView)findViewById(R.id.loading_title)).setText("جاري تحميل الورشات");
            ((ArabTextView)findViewById(R.id.no_data_title)).setText("لم يتم العثور على ورشات");
        }else {
            ((ArabTextView)findViewById(R.id.name)).setText("بحث الشركات");
            ((ArabTextView)findViewById(R.id.loading_title)).setText("جاري تحميل الشركات");
            ((ArabTextView)findViewById(R.id.no_data_title)).setText("لم يتم العثور على شركات");
        }
        mainRecylceView = (RecyclerView)findViewById(R.id.recyclerview5);
        mainScrollView = (EndlessScrollView)findViewById(R.id.heart_scroll_view);
        mainScrollView.setScrollViewListener(this);
        noItemsFound= findViewById(R.id.no_items_found);
        not_connected= findViewById(R.id.no_connected_to_internet);
        no_items_found_text= (TextView) findViewById(R.id.no_items_found_text);
        no_more= findViewById(R.id.no_more_to_show);
        loading = findViewById(R.id.loading);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainScrollView.scrollTo(0,0);
            }
        });
        items = new ArrayList();
        mainRecylceView.setLayoutManager(new LinearLayoutManager(this));
        //mainRecylceView.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
        if (getIntent().getStringExtra("type").equals("workshop")){
            adapter= new WorkshopsAdapter(items,this);
        }else {
            adapter= new CompaniesAdapter(items,this);
        }
        mainRecylceView.setAdapter(adapter);
        max=1000;
        retry();
        findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        not_connected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                retry();
            }
        });
    }

    public   void  fetchData(){


        loadMore();



    };
    public     int max=100000;
    public   int current=0;
    public String status="-1";
    public boolean loadingFlag = false;
    public   void loadMore(){
        loadingFlag=true;
        if(current>0)findViewById(R.id.loading_pager).setVisibility(View.VISIBLE);
        noItemsFound.setVisibility(View.GONE);
        no_more.setVisibility(View.GONE);
        not_connected.setVisibility(View.GONE);
        String query = "name="+getIntent().getStringExtra("name");
        if(getIntent().getStringExtra("cat_id")!=null)query+="&cat_id="+getIntent().getStringExtra("cat_id");
        if(getIntent().getStringExtra("city_id")!=null)query+="&city_id="+getIntent().getStringExtra("city_id");
        if(getIntent().getStringExtra("brands")!=null)query+="&brands="+getIntent().getStringExtra("brands");
        if (getIntent().getStringExtra("type").equals("workshop")){
            query+="&region_id="+getIntent().getStringExtra("region_id");
            query+="&critical_regions="+(getIntent().getBooleanExtra("is_emergency",false)?"on":"off");
        }
        if (getIntent().getStringExtra("trades")!=null){
            if (!getIntent().getStringExtra("trades").equals("0")){
                query="brand_id="+getIntent().getStringExtra("trades");
            }
        }
        Log.i("jgjfhg", "loadMore: "+query);

        BackgroundServices.getInstance(this)
                .setBaseUrl(APIUrl.SERVER + "companies/search?"+query+"&start="+current
                )
                .CallGet(
                        new PostAction() {
                            @Override
                            public void whenFinished(String status ,String response) {
                                loading.setVisibility(View.GONE);
                                loadingFlag = false;
                                if (response.equals("")) {
                                    not_connected.setVisibility(View.VISIBLE);
                                } else {
                                    try {

                                        findViewById(R.id.loading_pager).setVisibility(View.GONE);

                                        //
                                        JSONArray data = new JSONObject(response).getJSONArray("data");

                                        if (data.length() > 0) {
                                            for (int i = 0; i < data.length(); i++)
                                                items.add(new Workshop(data.getJSONObject(i)));
                                            adapter.notifyDataSetChanged();
                                            current += 10;

                                        }
                                        mainScrollView.setVisibility(View.VISIBLE);
                                        if (items.size() == 0) noItemsFound.setVisibility(View.VISIBLE);
                                        else {
                                            if (data.length() == 0) {
                                                no_more.setVisibility(View.VISIBLE);
                                                max = items.size();
                                            }
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        }
                );


    };

    void retry(){
        loading.setVisibility(View.VISIBLE);

        if(!ConnectionUtils.isNetworkAvailable(this)){
            not_connected.setVisibility(View.VISIBLE);
            not_connected.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    retry();
                }
            });
            loading.setVisibility(View.GONE);
        }else{
            not_connected.setVisibility(View.GONE);
            fetchData();
        }
    }


    @Override
    public void onScrollChanged(EndlessScrollView scrollView, int x, int y, int oldx, int oldy) {
        if (y > 400) fab.show();
        else
            fab.hide();
        if(items.size()>=max){
            return;
        }
        if (!loadingFlag) {
            // We take the last son in the scrollview
            View view = scrollView.getChildAt(scrollView.getChildCount() - 1);
            int distanceToEnd = (view.getBottom() - (scrollView.getHeight() + scrollView.getScrollY()));
            // if diff is zero, then the bottom has been reached
            if (distanceToEnd == 0) {
                loadMore();
            }

        }
    }
}
