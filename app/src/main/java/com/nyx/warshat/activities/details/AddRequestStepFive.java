package com.nyx.warshat.activities.details;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.warshat.APIsClass.AccountAPIsClass;
import com.nyx.warshat.APIsClass.RequestsAPIsClass;
import com.nyx.warshat.Intrafaces.IFailure;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.infinites.CustomerRequests;
import com.nyx.warshat.models.SelectedDate;

import java.util.ArrayList;
import java.util.List;

import Arabs.ArabButton;
import Arabs.ArabEditText;

public class AddRequestStepFive extends GlobalActivity {

    private ImageButton btn_back;
    private ArabButton btn_next;
    private ArabEditText edt_code;
    private ProgressBar pb_loading;
    private LinearLayout btn_resend_code;
    private ImageView img_resend_icon;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request_step_five);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //ImageButton
        btn_back = findViewById(R.id.go_back);
        //Button
        btn_next = findViewById(R.id.confirm_account_confirm);
        //EditText
        edt_code = findViewById(R.id.confirm_account_code);
        //ProgressBar
        pb_loading = findViewById(R.id.confirm_account_resend_progress);
        //LinearLayout
        btn_resend_code = findViewById(R.id.confirm_account_resend);
        //ImageView
        img_resend_icon = findViewById(R.id.confirm_account_resend_icon);
    }

    private void init_events() {
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dates = getIntent().getStringExtra("dates");
                String times = getIntent().getStringExtra("times");
                String desc = getIntent().getStringExtra("desc");
                String images = getIntent().getStringExtra("images");
                if (edt_code.getText().toString().equals("")){
                    edt_code.requestFocus();
                    edt_code.setHintTextColor(Color.RED);
                    return;
                }
                callConfirmAPI();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_resend_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callResendCodeAPI();
            }
        });
    }

    private void init_activity() {
        callResendCodeAPI();
    }

    private void callResendCodeAPI(){
        img_resend_icon.setVisibility(View.GONE);
        pb_loading.setVisibility(View.VISIBLE);
        btn_resend_code.setEnabled(false);
        btn_next.setEnabled(false);
        edt_code.setEnabled(false);
        AccountAPIsClass.resendCode(AddRequestStepFive.this,
                getIntent().getStringExtra("phone"),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        img_resend_icon.setVisibility(View.VISIBLE);
                        pb_loading.setVisibility(View.GONE);
                        btn_resend_code.setEnabled(true);
                        btn_next.setEnabled(true);
                        edt_code.setEnabled(true);
                    }

                    @Override
                    public void onNotActivated() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        img_resend_icon.setVisibility(View.VISIBLE);
                        pb_loading.setVisibility(View.GONE);
                        btn_resend_code.setEnabled(true);
                        btn_next.setEnabled(true);
                        edt_code.setEnabled(true);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        img_resend_icon.setVisibility(View.VISIBLE);
                        pb_loading.setVisibility(View.GONE);
                        btn_resend_code.setEnabled(true);
                        btn_next.setEnabled(true);
                        edt_code.setEnabled(true);
                        Toast.makeText(AddRequestStepFive.this, "لا يوجد اتصال بالانترنت, الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callConfirmAPI(){
        btn_next.setEnabled(false);
        AccountAPIsClass.confirmAccount(
                AddRequestStepFive.this,
                getIntent().getStringExtra("phone"),
                edt_code.getText().toString().trim(),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        btn_next.setEnabled(true);
                    }

                    @Override
                    public void onNotActivated() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        if (json!=null){
                            callSendRequestAPI();
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        btn_next.setEnabled(true);
                        Toast.makeText(AddRequestStepFive.this, "لا يوجد اتصال بالانترنت, الرجاء إعادة المحاولة", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callSendRequestAPI(){
        btn_next.setEnabled(false);
        String dates = getIntent().getStringExtra("dates");
        List<SelectedDate> selectedDates = new ArrayList<>();
        String[] dates_array = new Gson().fromJson(dates,String[].class);
        for (String s : dates_array){
            selectedDates.add(new SelectedDate(s,getIntent().getStringExtra("times")));
        }
        Log.i("dtesss", "callSendRequestAPI: "+new Gson().toJson(selectedDates));
        String[] images = new Gson().fromJson(getIntent().getStringExtra("images"),String[].class);
        List<String> images_list = new ArrayList<>();

        for (String s : images) {
            images_list.add(s);
        }

        RequestsAPIsClass.addRequest(AddRequestStepFive.this,
                getIntent().getStringExtra("id"),
                "0.0",
                "0.0",
                getIntent().getStringExtra("phone"),
                new Gson().toJson(selectedDates),
                getIntent().getStringExtra("desc"),
                images_list,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        btn_next.setEnabled(true);
                        Toast.makeText(AddRequestStepFive.this, "حدث خطأ ما", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNotActivated() {
                        btn_next.setEnabled(true);
                    }

                    @Override
                    public void onResponse(Object json) {
                        btn_next.setEnabled(true);
                        Toast.makeText(AddRequestStepFive.this, getString(R.string.sent_succesfully), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(AddRequestStepFive.this , CustomerRequests.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra("is_from_notifications",false);
                        startActivity(intent);
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        btn_next.setEnabled(true);
                        Toast.makeText(AddRequestStepFive.this, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                    }
                });

        /*
        BackgroundServices bg =    BackgroundServices.getInstance(AddRequestStepFive.this)
                .setBaseUrl(APIUrl.SERVER+"requests/new");
        for (String s : images)
            bg.addFile("request_pic[]" ,new File(s));
        bg.addPostParam("to_id" ,getIntent().getStringExtra("id"))
                .addPostParam("lat" ,"0.0")
                .addPostParam("req_phone" ,getIntent().getStringExtra("phone"))
                .addPostParam("lng" ,"0.0")
                .addPostParam("dates" ,new Gson().toJson(selectedDates))
                .addPostParam("problem_details" ,getIntent().getStringExtra("desc"))
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        btn_next.setEnabled(true);
                        if(status.equals("200")){
                            Toast.makeText(AddRequestStepFive.this, getString(R.string.sent_succesfully), Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(AddRequestStepFive.this , CustomerRequests.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }else{
                            Toast.makeText(AddRequestStepFive.this, response, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                **/

    }
}
