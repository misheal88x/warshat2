package com.nyx.warshat.activities.edits;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.warshat.Intrafaces.ISelected;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.CustomSpinnerAdapter;
import com.nyx.warshat.adapters.RegionsAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.CityObject;
import com.nyx.warshat.models.RegionObject;
import com.nyx.warshat.models.SelectedRegion;
import com.nyx.warshat.models.SelectedRegionsForSend;
import com.nyx.warshat.utilities.BaseFunctions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import Arabs.ArabButton;

public class EditWorkshopRegionsStepOne extends GlobalActivity implements ISelected {

    private Spinner city_spinner;
    private List<String> city_list_string;
    private List<CityObject> city_list;
    private List<RegionObject> region_list;
    private List<SelectedRegion> selectedRegions;
    private CustomSpinnerAdapter city_adapter;
    private RecyclerView rv_regions;
    private RegionsAdapter regionsAdapter;
    private LinearLayoutManager regionsLayoutManager;
    private ArabButton btn_next,btn_previous;
    private String api_response = "";
    private ArabButton btn_reconnect;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_workshop_regions_step_one);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //Spinner
        city_spinner = findViewById(R.id.cities_spinner);
        //RecyclerView
        rv_regions = findViewById(R.id.regions_reycler);
        //Button
        btn_next = findViewById(R.id.next);
        btn_previous = findViewById(R.id.previous);
        selectedRegions = new ArrayList<>();
        btn_reconnect = findViewById(R.id.reconnect);
        String in_regions = getIntent().getStringExtra("regions");
        if (in_regions != null){
            SelectedRegionsForSend[] temp = new Gson().fromJson(in_regions,SelectedRegionsForSend[].class);
            if (temp.length>0){
                for (int i = 0; i < temp.length; i++) {
                    if (temp[i].getE() == 0){
                        selectedRegions.add(new SelectedRegion(temp[i].getId()));
                    }
                }
            }
        }
    }

    private void init_events() {
        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int pos = BaseFunctions.getSpinnerPosition(parent,position);
                if (city_list.size()>0){
                    CityObject co = city_list.get(pos);
                    if (co.getRegions()!=null){
                        if (co.getRegions().size()>0){
                            region_list = new ArrayList<>();
                            for (RegionObject ro : co.getRegions()){
                                region_list.add(ro);
                            }
                            rv_regions.setAdapter(new RegionsAdapter(
                                    EditWorkshopRegionsStepOne.this,
                                    region_list,
                                    selectedRegions, EditWorkshopRegionsStepOne.this));
                        }
                        else {
                            region_list = new ArrayList<>();
                            rv_regions.setAdapter(new RegionsAdapter(
                                    EditWorkshopRegionsStepOne.this,
                                    region_list,
                                    selectedRegions, EditWorkshopRegionsStepOne.this));
                        }
                    }else {
                        region_list = new ArrayList<>();
                        rv_regions.setAdapter(new RegionsAdapter(
                                EditWorkshopRegionsStepOne.this,
                                region_list,
                                selectedRegions, EditWorkshopRegionsStepOne.this));
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_reconnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                init();
            }
        });
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String s = new Gson().toJson(selectedRegions);
                if (selectedRegions.size()==0){
                    Toast.makeText(EditWorkshopRegionsStepOne.this, "يجب عليك اختيار المناطق", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent intent = new Intent(EditWorkshopRegionsStepOne.this,EditWorkshopRegionsStepTwo.class);
                intent.putExtra("selected_regions",s);
                intent.putExtra("previous_selected_regions",getIntent().getStringExtra("regions"));
                intent.putExtra("api_response",api_response);
                startActivity(intent);
                finish();
            }
        });
    }

    private void init_activity() {
        init_spinner();
        init_recycler();

        init();
    }

    private void init_spinner(){
        city_list_string = new ArrayList<>();
        city_list = new ArrayList<>();

        BaseFunctions.init_spinner(EditWorkshopRegionsStepOne.this,city_spinner,city_adapter,city_list_string);
    }

    private void init_recycler(){
        region_list = new ArrayList<>();
        regionsAdapter = new RegionsAdapter(EditWorkshopRegionsStepOne.this, region_list, selectedRegions, this);
        regionsLayoutManager = new LinearLayoutManager(EditWorkshopRegionsStepOne.this,
                LinearLayoutManager.VERTICAL,false);
        rv_regions.setLayoutManager(regionsLayoutManager);
        rv_regions.setAdapter(regionsAdapter);
    }

    @Override
    public void checked(int id) {
        selectedRegions.add(new SelectedRegion(id));
    }

    @Override
    public void not_checked(int id) {
        for (int i = 0; i < selectedRegions.size() ; i++) {
            if (selectedRegions.get(i).getId()==id){
                selectedRegions.remove(i);
            }
        }
    }

    private void init(){
        btn_reconnect.setVisibility(View.GONE);
        btn_next.setEnabled(false);
        final ProgressDialog dialog = new ProgressDialog(EditWorkshopRegionsStepOne.this);
        dialog.setMessage("يرجى الانتظار..");
        dialog.setCancelable(false);
        dialog.show();
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"ws_init").CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                dialog.cancel();
                if(status.equals("500") || response.equals("")){
                    findViewById(R.id.reconnect).setVisibility(View.VISIBLE);

                    int maxLogSize = 5000;
                    for(int i = 0; i <= response.length() / maxLogSize; i++) {
                        int start = i * maxLogSize;
                        int end = (i+1) * maxLogSize;
                        end = end > response.length() ? response.length() : end;
                        Log.d("LONGAGO", response.substring(start, end));
                    }


                }else {
                    btn_next.setEnabled(true);
                    api_response = response;
                    try {
                        JSONObject all = new JSONObject(response).getJSONObject("data");
                        JSONArray cities = all.getJSONArray("cities");
                        if (cities!=null){
                            if (cities.length()>0){
                                for (int i = 0; i < cities.length(); i++) {
                                    List<RegionObject> temp = new ArrayList<>();
                                    if (cities.getJSONObject(i).getJSONArray("regions")!=null){
                                        if (cities.getJSONObject(i).getJSONArray("regions").length()>0){
                                            for (int j = 0; j < cities.getJSONObject(i).getJSONArray("regions").length(); j++) {
                                                temp.add(new RegionObject(
                                                        cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getInt("id"),
                                                        cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getString("name"),
                                                        cities.getJSONObject(i).getJSONArray("regions").getJSONObject(j).getInt("city_id")
                                                ));
                                            }
                                        }
                                    }
                                    CityObject co = new CityObject(
                                            cities.getJSONObject(i).getInt("id"),
                                            cities.getJSONObject(i).getString("name"),
                                            temp);
                                    city_list.add(co);
                                    city_list_string.add(co.getName());
                                }
                                BaseFunctions.init_spinner(EditWorkshopRegionsStepOne.this,city_spinner,city_adapter,city_list_string);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}
