package com.nyx.warshat.activities.logins;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;

import com.facebook.share.Share;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.homes.CustomerHome;
import com.nyx.warshat.activities.homes.WorkshopHome;
import com.nyx.warshat.activities.splashes.SplashActivity;
import com.nyx.warshat.helper.SharedPrefManager;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class SelectSignupType extends GlobalActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PackageInfo info;
        try {
            info = getPackageManager().getPackageInfo("com.nyx.warshat", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md;
                md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String something = new String(Base64.encode(md.digest(), 0));
                //String something = new String(Base64.encodeBytes(md.digest()));
                Log.e("warshat hash key", something);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("name not found", e1.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("no such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("exception", e.toString());
        }
        if(!SharedPrefManager.getInstance(this).isLoggedIn()) {
            if (SharedPrefManager.getInstance(SelectSignupType.this).getAccountConfirmed()) {
                setContentView(R.layout.activity_select_signup_type);
                findViewById(R.id.go_customer).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        Intent ii = new Intent(SelectSignupType.this, WelcomeActivity.class);
//                String method = "0";
//                String social_id="";
//                String email="";
//                String name="";
//                if(getIntent().getStringExtra("method")!=null)method=getIntent().getStringExtra("method");
//                if(getIntent().getStringExtra("social_id")!=null)social_id=getIntent().getStringExtra("social_id");
//                if(getIntent().getStringExtra("email")!=null)email=getIntent().getStringExtra("email");
//                if(getIntent().getStringExtra("name")!=null)name=getIntent().getStringExtra("name");
//                ii.putExtra("method" ,method);
//                ii.putExtra("social_id" ,social_id);
//                ii.putExtra("email" ,email);
//                ii.putExtra("name" ,name);
                        startActivity(ii);
                    }
                });
                findViewById(R.id.go_workshop).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent ii = new Intent(SelectSignupType.this, WelcomeActivityWorkshop.class);
//                String method = "0";
//                String social_id="";
//                String email="";
//                String name="";
//                if(getIntent().getStringExtra("method")!=null)method=getIntent().getStringExtra("method");
//                if(getIntent().getStringExtra("social_id")!=null)social_id=getIntent().getStringExtra("social_id");
//                if(getIntent().getStringExtra("email")!=null)email=getIntent().getStringExtra("email");
//                if(getIntent().getStringExtra("name")!=null)name=getIntent().getStringExtra("name");
//                ii.putExtra("method" ,method);
//                ii.putExtra("social_id" ,social_id);
//                ii.putExtra("email" ,email);
//                ii.putExtra("name" ,name);
                        startActivity(ii);
                    }
                });
            }else {
                Intent intent = new Intent(SelectSignupType.this,ConfirmAccountActivity.class);
                startActivity(intent);
                finish();
            }
        }else{
            if(SharedPrefManager.getInstance(this).isCustomer()){
                    startActivity(new Intent(SelectSignupType.this , CustomerHome.class));
                    finish();
                }else{
                    startActivity(new Intent(SelectSignupType.this , WorkshopHome.class));
                    finish();
                }
        }
    }
}
