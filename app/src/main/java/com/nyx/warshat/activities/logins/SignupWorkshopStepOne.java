package com.nyx.warshat.activities.logins;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;

public class SignupWorkshopStepOne extends GlobalActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_workshop_step_one);
        findViewById(R.id.continue_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String work_field = "";
                if(((CheckBox)findViewById(R.id.main)).isChecked())work_field="1";
                if(((CheckBox)findViewById(R.id.mng)).isChecked())work_field="2";
                if(((CheckBox)findViewById(R.id.main)).isChecked() &&
                        ((CheckBox)findViewById(R.id.mng)).isChecked())work_field="3";

                if(work_field.equals("")){
                    Toast.makeText(SignupWorkshopStepOne.this, getString(R.string.plz_select_foeld)
                         , Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent ii =new Intent(SignupWorkshopStepOne.this ,SignupWorkshopStepTwo.class);
                ii.putExtra("work_field" ,work_field);

                ii.putExtra("method" ,getIntent().getStringExtra("method"));
                ii.putExtra("social_id" ,getIntent().getStringExtra("social_id"));
                ii.putExtra("email" ,getIntent().getStringExtra("email"));
                ii.putExtra("name" ,getIntent().getStringExtra("name"));


                startActivity(ii);
            }
        });
    }
}
