package com.nyx.warshat.activities.details;

import android.content.Intent;
import android.graphics.Color;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.homes.CustomerHome;
import com.nyx.warshat.adapters.ProductsAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SendRequestToCompany extends GlobalActivity {
ArrayList items;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_request_to_company);


        findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String id = getIntent().getStringExtra("id");
        items=(ArrayList)getIntent().getSerializableExtra("items");
        // ((TextView)findViewById(R.id.name)).setText(getIntent().getStringExtra("name"));
        ((TextView)findViewById(R.id.address)).setText(getIntent().getStringExtra("address"));
        ((TextView)findViewById(R.id.name2)).setText(getIntent().getStringExtra("name"));
        Glide.with(this).load(getIntent().getStringExtra("icon")).into(
                ((ImageView)findViewById(R.id.pic)));
        Glide.with(this).load(getIntent().getStringExtra("icon")).into(
                ((ImageView)findViewById(R.id.icon)));


        RecyclerView list = (RecyclerView)findViewById(R.id.pics_list);
        list.setLayoutManager(new GridLayoutManager(SendRequestToCompany.this ,3));
        ProductsAdapter adapter =new ProductsAdapter(items
                ,SendRequestToCompany.this);
        list.setAdapter(adapter);

        findViewById(R.id.go_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                EditText d = (EditText)findViewById(R.id.details);
                if(d.getText().toString().trim().equals("")){
                    d.setHintTextColor(Color.RED);
                    d.requestFocus();
                    return;
                }



                v.setEnabled(false);
                String json = new Gson().toJson(items);
                BackgroundServices.getInstance(SendRequestToCompany.this)
                        .setBaseUrl(APIUrl.SERVER+"company/contact")
                        .addPostParam("to_id" ,getIntent().getStringExtra("id"))
                        .addPostParam("problem_details" ,d.getText().toString().trim())
                        .addPostParam("products" ,json)
                        .CallPost(new PostAction() {
                            @Override
                            public void whenFinished(String status, String response) throws JSONException {
                                Toast.makeText(SendRequestToCompany.this,
                                        new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();

                                if(status.equals("200")){
                                    Toast.makeText(SendRequestToCompany.this, getString(R.string.sent_succesfully), Toast.LENGTH_SHORT).show();
                                    Intent intent=new Intent(SendRequestToCompany.this , CustomerHome.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        });


            }
        });
        //   retrive(id);
    }
}
