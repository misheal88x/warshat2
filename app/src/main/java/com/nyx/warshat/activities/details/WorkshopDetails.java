package com.nyx.warshat.activities.details;

import android.content.Intent;
import android.net.Uri;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.GalleryAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import Arabs.ArabTextView;

public class WorkshopDetails extends GlobalActivity {

    private LinearLayout lyt_phone_number,lyt_land_line;
    private ArabTextView tv_phone_number,tv_land_line;
    private ImageView pic;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workshop_details);
        findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        lyt_phone_number = findViewById(R.id.phone_number_lyt);
        lyt_land_line = findViewById(R.id.land_line_lyt);
        tv_phone_number = findViewById(R.id.phone_number_txt);
        tv_land_line = findViewById(R.id.land_line_txt);
        pic = findViewById(R.id.pic);

        String id = getIntent().getStringExtra("id");
        ((TextView)findViewById(R.id.name)).setText(getIntent().getStringExtra("name"));
        ((TextView)findViewById(R.id.name2)).setText(getIntent().getStringExtra("name"));
        try {
            Glide.with(this).load(getIntent().getStringExtra("icon"))
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big))
                    .into(
                    ((ImageView) findViewById(R.id.pic)));
        }catch (Exception e){}
        try {
            Glide.with(this).load(getIntent().getStringExtra("icon"))
                    .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big))
                    .into(
                    ((ImageView) findViewById(R.id.icon)));
        }catch (Exception e){}

        retrive(id);
    }


    void retrive(final String id){
        BackgroundServices.getInstance(this)
                .setBaseUrl(APIUrl.SERVER+"get_workshop?id="+id).CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                findViewById(R.id.loading).setVisibility(View.GONE);
                final JSONObject data = new JSONObject(response).getJSONObject("data");
                if (data.getString("workshop_city_name")!=null&&data.getString("workshop_region_name")!=null){
                    if (!data.getString("workshop_region_name").equals("")){
                        ((ArabTextView)findViewById(R.id.address)).setText(data.getString("workshop_city_name")+", "+data.getString("workshop_region_name"));
                    }else {
                        findViewById(R.id.address_layout).setVisibility(View.GONE);
                    }
                }else {
                    findViewById(R.id.address_layout).setVisibility(View.GONE);
                }
                ((TextView)findViewById(R.id.about)).setText(data.getString("details"));
                ((TextView)findViewById(R.id.jobs_count)).setText(data.getString("count_jobs"));
                findViewById(R.id.loading).setVisibility(View.GONE);
                findViewById(R.id.main).setVisibility(View.VISIBLE);

                if (data.getString("phone")!=null&&!data.getString("phone").equals("")){
                    tv_phone_number.setText("00963-"+data.getString("phone"));
                }else {
                    lyt_phone_number.setVisibility(View.GONE);
                }
                if(data.getString("landline") != null && !data.getString("landline").equals("")){
                    tv_land_line.setText("00963-"+data.getString("landline"));
                }else {
                    lyt_phone_number.setVisibility(View.GONE);
                }
                try{
                    Glide.with(WorkshopDetails.this).load(data.getString("profile_pic")).into(pic);
                }catch (Exception e){}
                JSONArray array = new JSONObject(response).getJSONObject("data").getJSONArray("gallery");
               ArrayList imaages = new ArrayList<>();
               if (array.length()==0){
                   findViewById(R.id.gallery_title).setVisibility(View.GONE);
               }
                for(int i=0;i< array.length();i++)
                    imaages.add(new Image(array.getJSONObject(i)));

                RecyclerView list = (RecyclerView)findViewById(R.id.pics_list);
                list.setLayoutManager(new GridLayoutManager(WorkshopDetails.this ,4));
                GalleryAdapter    adapter =new GalleryAdapter(imaages ,WorkshopDetails.this ,false);
                list.setAdapter(adapter);
               final float rating1 = (float)data.getDouble("response_time_rating");
                final   float rating2 = (float)data.getDouble("performance_rating");
                final   float rating3 = (float)data.getDouble("quality_rating");
                final   float rating4 = (float)data.getDouble("support_rating");

                ((RatingBar)findViewById(R.id.rating1)).setRating(rating1);
                ((RatingBar)findViewById(R.id.rating2)).setRating(rating2);
                ((RatingBar)findViewById(R.id.rating3)).setRating(rating3);
                ((RatingBar)findViewById(R.id.rating4)).setRating(rating4);
                float total_rate = (rating1+rating2+rating3+rating4)/4;
                if (total_rate == 0f){
                    ((RatingBar)findViewById(R.id.rating)).setRating(total_rate);
                    ((TextView)findViewById(R.id.rating_txt)).setText("0");
                }else {
                    NumberFormat nf = NumberFormat.getNumberInstance(Locale.US);
                    DecimalFormat f = (DecimalFormat) nf;
                    f.applyPattern("##.0");
                    String formattedValue = f.format(total_rate);
                    String formatted_rate = String.valueOf(total_rate);
                    ((RatingBar)findViewById(R.id.rating)).setRating(total_rate);
                    ((TextView)findViewById(R.id.rating_txt)).setText(formattedValue);
                }

                findViewById(R.id.call).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_DIAL);
                        try {
                            callIntent.setData(Uri.parse("tel: "+data.getString("phone")));
                            startActivity(callIntent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

                findViewById(R.id.contact).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {



                        Intent ii=new Intent(WorkshopDetails.this, AddRequestStepOne.class);
                        ii.putExtra("id" ,getIntent().getStringExtra("id"));
                        ii.putExtra("name" ,getIntent().getStringExtra("name"));
                        ii.putExtra("icon" ,getIntent().getStringExtra("icon"));
                        ii.putExtra("rating" ,(float)((rating1+rating2+rating3+rating4)/4));
                        startActivity(ii);
                    }
                });


            }
        });
    }
}
