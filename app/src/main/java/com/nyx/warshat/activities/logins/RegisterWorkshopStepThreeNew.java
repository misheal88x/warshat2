package com.nyx.warshat.activities.logins;

import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.StringArrayAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Arabs.ArabButton;
import Arabs.ArabEditText;

public class RegisterWorkshopStepThreeNew extends GlobalActivity {

    private JSONArray categories;
    private Spinner cats_spinner;
    private ArabEditText edt_desc;
    private ArabButton btn_previous,btn_next;
    private String selected_cat_id = "";
    private boolean no_cats = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_workshop_step_three_new);
        init_views();
        init_events();
        init_activity();
        init_spinner();
    }

    private void init_views() {
        //Spinner
        cats_spinner = findViewById(R.id.cat_spinner);
        //EditText
        edt_desc = findViewById(R.id.w_desc);
        //Button
        btn_next = findViewById(R.id.next);
        btn_previous = findViewById(R.id.previous);
    }

    private void init_events() {
        if (getIntent().getStringExtra("api_response") != null) {
            if (!getIntent().getStringExtra("api_response").equals("")) {
                try {
                    JSONObject all = new JSONObject(getIntent().getStringExtra("api_response")).getJSONObject("data");
                    categories = all.getJSONArray("categories");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        cats_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (categories != null) {
                    if (categories.length() > 0) {
                        try {
                            selected_cat_id = categories.getJSONObject(position).getString("id");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_cat_id.equals("")) {
                    Toast.makeText(RegisterWorkshopStepThreeNew.this, "يجب عليك اختيار مجال عمل الشركة", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_desc.getText().toString().trim().equals("")) {
                    edt_desc.setHintTextColor(Color.RED);
                    edt_desc.requestFocus();
                    return;
                }
                Intent intent = new Intent(RegisterWorkshopStepThreeNew.this, RegisterWorkshopStepFourNew.class);
                intent.putExtra("full_name", getIntent().getStringExtra("full_name"));
                intent.putExtra("mobile", getIntent().getStringExtra("mobile"));
                intent.putExtra("email", getIntent().getStringExtra("email"));
                intent.putExtra("password", getIntent().getStringExtra("password"));
                intent.putExtra("w_full_name", getIntent().getStringExtra("w_full_name"));
                intent.putExtra("w_mobile", getIntent().getStringExtra("w_mobile"));
                intent.putExtra("w_land_line", getIntent().getStringExtra("w_land_line"));
                intent.putExtra("city_id", getIntent().getStringExtra("city_id"));
                intent.putExtra("region_id", getIntent().getStringExtra("region_id"));
                intent.putExtra("api_response", getIntent().getStringExtra("api_response"));
                intent.putExtra("pic", getIntent().getStringExtra("pic"));
                intent.putExtra("cat_id", selected_cat_id);
                intent.putExtra("desc", edt_desc.getText().toString());
                startActivity(intent);
            }
        });
        btn_previous.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

            private void init_activity () {

            }

            private void init_spinner () {

                try {
                    if (categories.length() > 0) {
                        no_cats = false;
                        String[] c_names = new String[categories.length()];
                        for (int i = 0; i < categories.length(); i++) {
                            c_names[i] = categories.getJSONObject(i).getString("name");
                        }
                        cats_spinner.setAdapter(new
                                StringArrayAdapter(RegisterWorkshopStepThreeNew.this, c_names));
                    } else {
                        String[] c_names = new String[]{"مجال عمل الورشة"};
                        cats_spinner.setAdapter(new
                                StringArrayAdapter(RegisterWorkshopStepThreeNew.this, c_names));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

}
