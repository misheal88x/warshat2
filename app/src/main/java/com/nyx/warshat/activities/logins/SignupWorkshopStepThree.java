package com.nyx.warshat.activities.logins;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.warshat.ErrorActivity;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.SelectedRegionsAdapter;
import com.nyx.warshat.adapters.StringArrayAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.ConnectionUtils;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.helper.SharedPrefManager;
import com.nyx.warshat.models.ApiRegion;
import com.nyx.warshat.models.City;
import com.nyx.warshat.models.Region;
import com.nyx.warshat.models.SelectableRegion;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class SignupWorkshopStepThree extends GlobalActivity {
 JSONArray categories;
 ArrayList<City> cities;
     ArrayList<SelectableRegion> selected_regions;
    RecyclerView regions_list ;
    SelectedRegionsAdapter regions_adapter;



    void loadRegions(int city_index){
        Log.d("LOADING_REGIONS"  , cities.get(city_index).getName());
      selected_regions .clear();
        regions_adapter.notifyDataSetChanged();
      ArrayList<Region> regions = cities.get(city_index).getRegions();
      for(Region r : regions)selected_regions.add(new SelectableRegion(r.getId() ,r.getName() ,false ,false));
      regions_adapter.notifyDataSetChanged();

    }


    void init(){
    findViewById(R.id.reconnect).setVisibility(View.GONE);
    findViewById(R.id.continue_btn).setEnabled(false);
        selected_regions = new ArrayList<>();
    String filter="c1";
    if(getIntent().getStringExtra("work_field")!=null) {
        if (getIntent().getStringExtra("work_field").equals("1")) filter = "c1";
        if (getIntent().getStringExtra("work_field").equals("2")) filter = "c2";
        if (getIntent().getStringExtra("work_field").equals("3")) filter = "c3";
    }
    BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"ws_init?filter="+filter).CallGet(new PostAction() {
        @Override
        public void whenFinished(String status, String response) throws JSONException {
            findViewById(R.id.loading).setVisibility(View.GONE);

            try {
                if (status.equals("500") || response.equals("")) {
                    findViewById(R.id.reconnect).setVisibility(View.VISIBLE);

                    int maxLogSize = 5000;
                    for (int i = 0; i <= response.length() / maxLogSize; i++) {
                        int start = i * maxLogSize;
                        int end = (i + 1) * maxLogSize;
                        end = end > response.length() ? response.length() : end;
                        Log.d("LONGAGO", response.substring(start, end));
                    }


                } else {
                    findViewById(R.id.cats_regions_view).setVisibility(View.VISIBLE);
                    regions_list = findViewById(R.id.regions_list);

                    regions_adapter = new SelectedRegionsAdapter(selected_regions, SignupWorkshopStepThree.this);
                    regions_list.setLayoutManager(new LinearLayoutManager(SignupWorkshopStepThree.this));
                    regions_list.setAdapter(regions_adapter);
                    findViewById(R.id.continue_btn).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            Gson gson = new Gson();

                            ArrayList<ApiRegion> res = new ArrayList<>();
                            for (SelectableRegion sr : selected_regions)
                                if (sr.isSelected() || sr.isEmergency()) {
                                    res.add(new ApiRegion(sr.getId(), sr.isEmergency() ? "1" : "0"));
                                }
                            String regions = gson.toJson(res);//[{"e":"1","id":"2"}]
                            Log.d("RRG", regions);
                            String work_cat = "-1";
                            String city_id = "-1";
                            try {
                                work_cat = categories.getJSONObject(((Spinner) findViewById(R.id.select_cat)).getSelectedItemPosition()).getString("id");
                                city_id = cities.get(((Spinner) findViewById(R.id.select_city)).getSelectedItemPosition()).getId();
                            } catch (Exception g) {
                                Toast.makeText(SignupWorkshopStepThree.this, "لا يمكن المتابعة بدون اختيار فئة و مدينة", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            String details = ((EditText) findViewById(R.id.about)).getText().toString().trim();

                            if (details.equals("")) {
                                ((EditText) findViewById(R.id.about)).setHintTextColor(Color.RED);
                                ((EditText) findViewById(R.id.about)).requestFocus();
                                return;
                            }
                            v.setEnabled(false);

                            BackgroundServices.getInstance(SignupWorkshopStepThree.this).setBaseUrl(APIUrl.SERVER + "login")
                                    .addPostParam("login_type", "2")
                                    .addPostParam("user_type", "2")
                                    .addPostParam("name", getIntent().getStringExtra("name"))
                                    .addPostParam("phone", getIntent().getStringExtra("phone"))
                                    .addPostParam("address", getIntent().getStringExtra("address"))
                                    .addPostParam("password", getIntent().getStringExtra("password"))
                                    .addPostParam("c_password", getIntent().getStringExtra("password"))
                                    .addPostParam("landline", getIntent().getStringExtra("landline"))
                                    .addPostParam("work_field", getIntent().getStringExtra("work_field"))
                                    .addPostParam("work_cat", work_cat)
                                    .addPostParam("city_id", city_id)
                                    .addPostParam("regions", regions)
                                    .addPostParam("details", details)
                                    .addPostParam("email", "")
                                    .addFile("cover", new File(getIntent().getStringExtra("pic")))
                                    .CallPost(new PostAction() {
                                        @Override
                                        public void whenFinished(String status, String response) throws JSONException {
                                            v.setEnabled(true);
                                            Toast.makeText(SignupWorkshopStepThree.this, new JSONObject(response).getString("message"),
                                                    Toast.LENGTH_SHORT).show();

                                            if (status.equals("200")) {
                                                JSONObject data = new JSONObject(response).getJSONObject("data");
                                                String token = data.getString("token");
                                                String type = "2";
                                                SharedPrefManager.getInstance(SignupWorkshopStepThree.this).saveToken(token, type);
                                                SharedPrefManager.getInstance(SignupWorkshopStepThree.this).savePhoneNumber(getIntent().getStringExtra("phone"));
                                                Intent intent = new Intent(SignupWorkshopStepThree.this, SelectSignupType.class);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                startActivity(intent);
                                                finish();
                                            }
                                        }
                                    });


                        }
                    });
                    findViewById(R.id.continue_btn).setEnabled(true);
                    findViewById(R.id.info).setVisibility(View.GONE);
                    JSONObject all = new JSONObject(response).getJSONObject("data");

                    cities = new ArrayList<>();
                    JSONArray cities_arr = all.getJSONArray("cities");
                    categories = all.getJSONArray("categories");
                    String[] c_names = new String[cities_arr.length()];
                    String[] cats_names = new String[categories.length()];
                    for (int i = 0; i < cities_arr.length(); i++) {
                        cities.add(new City(cities_arr.getJSONObject(i)));

                    }
                    for (int i = 0; i < cities.size(); i++) {
                        c_names[i] = cities.get(i).getName();

                    }
                    for (int i = 0; i < categories.length(); i++)
                        cats_names[i] = categories.getJSONObject(i).getString("name");
                    ((Spinner) findViewById(R.id.select_cat)).setAdapter(new
                            StringArrayAdapter(SignupWorkshopStepThree.this, cats_names));
                    ((Spinner) findViewById(R.id.select_city)).setAdapter(new
                            StringArrayAdapter(SignupWorkshopStepThree.this, c_names));
                    ((Spinner) findViewById(R.id.select_city)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            Log.d("Cityc_CHNG", position + "");
                            loadRegions(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    loadRegions(0);
                }

            }
            catch (Exception e){
                Intent i = new Intent(getApplicationContext() , ErrorActivity.class);
                i.putExtra("error" ,e.getMessage());
                startActivity(i);
            }
        }
    });
}
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_workshop_step_three);
    if(ConnectionUtils.isNetworkAvailable(this))
        init();
    else{
        findViewById(R.id.reconnect).setVisibility(View.VISIBLE);
        findViewById(R.id.reconnect).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ConnectionUtils.isNetworkAvailable(SignupWorkshopStepThree.this))
                    init();
                else
                    findViewById(R.id.reconnect).setVisibility(View.VISIBLE);
            }
        });
    }
    init();
//    if(cities!=null && categories!=null){
//        selected_regions = new ArrayList<>();
//        findViewById(R.id.info).setVisibility(View.GONE);
//        String[] c_names = new String[cities.size()];
//        String[] cats_names = new String[categories.length()];
//        for (int i = 0; i < cities.size(); i++) {
//                c_names[i] = cities.get(i).getName();
//
//        }
//        for (int i = 0; i < categories.length(); i++) {
//            try {
//                cats_names[i] = categories.getJSONObject(i).getString("name");
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//        }
//        ((Spinner) findViewById(R.id.select_cat)).setAdapter(new
//                StringArrayAdapter(SignupWorkshopStepThree.this, cats_names));
//        ((Spinner) findViewById(R.id.select_city)).setAdapter(new
//                StringArrayAdapter(SignupWorkshopStepThree.this, c_names));
//        loadRegions(0);
//    }else {
//        ((Spinner) findViewById(R.id.select_cat)).setAdapter(new
//                StringArrayAdapter(SignupWorkshopStepThree.this, new String[]{getString(R.string.work_cat)}));
//        ((Spinner) findViewById(R.id.select_city)).setAdapter(new
//                StringArrayAdapter(SignupWorkshopStepThree.this, new String[]{getString(R.string.work_city)}));
//        init();
//    }




    }
}
