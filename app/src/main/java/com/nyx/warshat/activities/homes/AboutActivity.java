package com.nyx.warshat.activities.homes;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.utilities.FooterHelper;

import org.json.JSONException;
import org.json.JSONObject;

public class AboutActivity extends GlobalActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        FooterHelper.initFooterForCustomer(this);
        ((TextView)findViewById(R.id.name)).setText(R.string.about);

        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"ws_init")
                .CallGet(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        if(status.equals("200")){
                            if (new JSONObject(response).has("data")){
                                if (new JSONObject(response).getJSONObject("data").has("about_app")){
                                    if (new JSONObject(response).getJSONObject("data").getString("about_app")!=null){
                                        ((TextView)findViewById(R.id.text)).setText(new JSONObject(response).getJSONObject("data").getString("about_app"));
                                    }
                                }
                            }
                        }
                        findViewById(R.id.loading).setVisibility(View.GONE);
                        findViewById(R.id.main).setVisibility(View.VISIBLE);
                    }
                });
findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        finish();
    }
});

    }

    @Override
    public void onBackPressed() {
        Intent intent=new Intent(AboutActivity.this , CustomerHome.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }
}
