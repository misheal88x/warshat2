package com.nyx.warshat.activities.logins;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.splashes.SplashActivity;

public class WelcomeActivityWorkshop extends GlobalActivity {
    String[] texts ;
    int[] pics  ,indicators;
    int index=0;
    private ImageView img_1,img_2,img_3,img_4,img_5;
    void set(){
        if(index<0){
            finish();
            return;
        }

        if(index>pics.length-1){
            Intent intent = new Intent(WelcomeActivityWorkshop.this,SplashActivity.class);
            intent.putExtra("account_type","workshop");
            startActivity(intent);
            finish();
            return;
            /*
            Intent ii = new Intent(WelcomeActivityWorkshop.this , SignupWorkshopStepOne.class);
//        ii.putExtra("method" ,getIntent().getStringExtra("method"));
//        ii.putExtra("social_id" ,getIntent().getStringExtra("social_id"));
//        ii.putExtra("email" ,getIntent().getStringExtra("email"));
//        ii.putExtra("name" ,getIntent().getStringExtra("name"));

            startActivity(ii);
            finish();
            return;
**/
        }
        if(index==pics.length-1){
            ((TextView) findViewById(R.id.go_next)).setText("متابعة");

        }else
            ((TextView) findViewById(R.id.go_next)).setText("التالي");


        ((TextView)findViewById(R.id.info)).setText(texts[index]);
        ((ImageView)findViewById(R.id.pic)).setImageDrawable(getResources().getDrawable(pics[index]));

        //((ImageView)findViewById(R.id.ind)).setImageDrawable(getResources().getDrawable(indicators[index]));

        setIndicators(index);
    }

    private void setIndicators(int i){
        ImageView[] imgs = new ImageView[]{img_1,img_2,img_3,img_4};
        imgs[i].setImageResource(R.drawable.ic_yellow_circle);
        for (int j = 0; j < 4; j++) {
            if (j!=i){
                imgs[j].setImageResource(R.drawable.ic_blue_circle);
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        img_1 = findViewById(R.id.slider_image_1);
        img_2 = findViewById(R.id.slider_image_2);
        img_3 = findViewById(R.id.slider_image_3);
        img_4 = findViewById(R.id.slider_image_4);
        img_5 = findViewById(R.id.slider_image_5);
        img_5.setVisibility(View.GONE);
        texts  =new String[]{
               "أعرض ورشتك أمام مئات المستخدمين الباحثين عن خدمات الصيانة",
               "أنشئ صفحة تعريفية لورشتك مع صور ومعلومات",
               "ستظهر ورشتك للمستخدمين عندما يبحثون في اختصاص ورشتك",
               "سيتواصل المستخدمون معكم بشكل مباشر بعد اختيارهم لورشتك",
        };
        pics =new int[]{R.drawable.intro_1_w ,R.drawable.intro_2_w ,R.drawable.intro_3_w,R.drawable.intro_4_w};
        indicators =new int[]{R.drawable.dots_1 ,R.drawable.dots_2 ,R.drawable.dots_3 ,R.drawable.dots_4};

        findViewById(R.id.go_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index++;
                set();
            }
        });
        findViewById(R.id.go_prev).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                index--;
                set();
            }
        });
        set();

    }
}
