package com.nyx.warshat.activities.details;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.infinites.CustomerRequests;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;

import org.json.JSONException;
import org.json.JSONObject;

public class RateWorkshop extends GlobalActivity {

    private TextView tv_address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_workshop);
        tv_address = findViewById(R.id.address);
        String city = "";
        String region = "";
        if (getIntent().getStringExtra("city")!=null){
            if (!getIntent().getStringExtra("city").equals("")){
                city = getIntent().getStringExtra("city");
            }
        }
        if (getIntent().getStringExtra("region")!=null){
            if (!getIntent().getStringExtra("region").equals("")){
                region = getIntent().getStringExtra("region");
            }
        }
        if (!city.equals("")&&!region.equals("")) {
            tv_address.setText(city + ", " + region);
        }
        ((RatingBar)findViewById(R.id.rating)).setRating(getIntent().getFloatExtra("rating",0f));
        findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
       final String id = getIntent().getStringExtra("id");
        ((TextView)findViewById(R.id.name2)).setText(getIntent().getStringExtra("name"));
        Glide.with(this).load(getIntent().getStringExtra("icon"))
                .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big)).into(
                ((ImageView)findViewById(R.id.pic)));
        Glide.with(this).load(getIntent().getStringExtra("problem_pic"))
                .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big)).into(
                ((ImageView)findViewById(R.id.problem_pic)));
        ((RatingBar)findViewById(R.id.rating)).setRating(getIntent().getFloatExtra("rating" ,0.0f));
        findViewById(R.id.go_send).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                String notes = ((EditText)findViewById(R.id.notes)).getText().toString().trim();
                String rating1 = ((RatingBar)findViewById(R.id.rating1)).getRating()+"";
                String rating2 = ((RatingBar)findViewById(R.id.rating2)).getRating()+"";
                String rating3 = ((RatingBar)findViewById(R.id.rating3)).getRating()+"";
                String rating4 = ((RatingBar)findViewById(R.id.rating4)).getRating()+"";
                v.setEnabled(false);
                BackgroundServices.getInstance(RateWorkshop.this).setBaseUrl(APIUrl.SERVER+"workshops/rate")
                        .addPostParam("workshop_id" ,id)
                        .addPostParam("request_id" ,getIntent().getStringExtra("request_id"))
                        .addPostParam("notes" ,notes)
                        .addPostParam("response_time_rating" ,rating1)
                        .addPostParam("performance_rating" ,rating2)
                        .addPostParam("quality_rating" ,rating3)
                        .addPostParam("support_rating" ,rating4)
                        .CallPost(new PostAction() {
                            @Override
                            public void whenFinished(String status, String response) throws JSONException {
                                Log.i("rerer", "whenFinished: "+response);
                                Toast.makeText(RateWorkshop.this, new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(RateWorkshop.this , CustomerRequests.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intent.putExtra("is_from_notifications",false);
                                startActivity(intent);
                                finish();
                            }
                        });

            }
        });

    }
}
