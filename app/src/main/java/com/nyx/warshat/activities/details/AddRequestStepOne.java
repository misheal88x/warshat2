package com.nyx.warshat.activities.details;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.google.gson.Gson;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import Arabs.ArabButton;

public class AddRequestStepOne extends GlobalActivity {

    private ImageButton btn_back;
    private ArabButton btn_next;
    //private MaterialCalendarView calendarView;
    private CalendarView calendarView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request_step_one);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //ImageButton
        btn_back = findViewById(R.id.go_back);
        //Button
        btn_next = findViewById(R.id.next_btn);
        //CalendarView
        //calendarView = findViewById(R.id.calendarView);
        calendarView = findViewById(R.id.calendarView);

        //calendarView.setDateSelected(CalendarDay.today(),true);

    }

    private void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Calendar> selectedDates = calendarView.getSelectedDates();
                if (selectedDates.size()==0){
                    Toast.makeText(AddRequestStepOne.this, "الرجاء اختيار يوم واحد على الأقل", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
                    List<String> datesString = new ArrayList<>();
                    for (Calendar c : selectedDates){
                        Date date = c.getTime();
                        String date1 = format1.format(date);
                        datesString.add(date1);
                    }
                    Intent intent = new Intent(AddRequestStepOne.this,AddRequestStepTwo.class);
                    intent.putExtra("dates",new Gson().toJson(datesString));
                    intent.putExtra("id",getIntent().getStringExtra("id"));
                    startActivity(intent);
                }
            }
        });
    }

    private void init_activity() {
        //calendarView.setLeftArrow(R.drawable.ic_cal_next);
        //calendarView.setRightArrow(R.drawable.ic_cal_back);
    }
}
