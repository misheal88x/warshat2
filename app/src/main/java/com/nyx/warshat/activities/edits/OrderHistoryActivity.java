package com.nyx.warshat.activities.edits;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.details.RequestDetails;

public class OrderHistoryActivity extends GlobalActivity {

    private TextView name;
    private ImageButton go_back;
    private RecyclerView recycler;
    private TextView right,left,center;
    private LinearLayout loading,no_data,no_internet;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //Toolbar
        name = findViewById(R.id.name);
        go_back = findViewById(R.id.go_back);
        //RecyclerView
        recycler = findViewById(R.id.history_recycler);
        //TextView
        right = findViewById(R.id.right_btn);
        center = findViewById(R.id.center_btn);
        left = findViewById(R.id.left_btn);
        //LinearLayouts
        loading = findViewById(R.id.loading);
        no_data = findViewById(R.id.no_items_found);
        no_internet = findViewById(R.id.no_connected_to_internet);

    }

    private void init_events() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(OrderHistoryActivity.this, RequestDetails.class);
                intent.putExtra("")
            }
        });
    }

    private void init_activity() {
        String id = getIntent().getStringExtra("id");
        name.setText(getString(R.string.request)+" "+id);
    }
}
