package com.nyx.warshat.activities.edits;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.logins.SelectSignupType;
import com.nyx.warshat.adapters.GalleryAdapter;
import com.nyx.warshat.adapters.SelectedRegionsAdapterEditable;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.ConnectionUtils;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.helper.SharedPrefManager;
import com.nyx.warshat.models.ApiRegion;
import com.nyx.warshat.models.City;
import com.nyx.warshat.models.Image;
import com.nyx.warshat.models.SelectableRegion;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

public class EditWorkshop extends GlobalActivity {

    private final static int SELECT_PHOTO = 12345;
    private final static int PLACE_PICKER_REQUEST = 999;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    String imagePath = "";
    //Bitmap bitmap;
    boolean isUpdating = false;
    Uri croppedImg;
    private static final int PICK_FROM_GALLERY = 14436;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pick();
                } else {
                    Toast.makeText(getApplicationContext(), "Not allowed to open gallery", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }



    void pick() {
        if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
            Toast.makeText(getApplicationContext(), "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
            return;
        }
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }

        startActivityForResult(photoPickerIntent, SELECT_PHOTO);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("RESULTED ", requestCode + " and " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        Log.e("GOTCHA ", "yes");
                        final Uri resultUri = UCrop.getOutput(data);
                        Log.e("Picsaved1 in : ", resultUri.toString());
                        Log.e("Picsaved2 in : ", resultUri.getPath());
                        Log.e("Picsaved3 in : ", resultUri.getEncodedPath());

                        try {
                            imagePath = resultUri.getPath();
                            findViewById(R.id.go_upload).setEnabled(false);

                            BackgroundServices.getInstance(this)
                                    .setBaseUrl(APIUrl.SERVER+"gallery/new").addFile("cover" ,new File(imagePath)).CallPost(new PostAction() {
                                @Override
                                public void whenFinished(String status, String response) throws JSONException {

                                    findViewById(R.id.go_upload).setEnabled(true);

                                    Toast.makeText(EditWorkshop.this, new JSONObject(response)
                                            .getString("message"), Toast.LENGTH_SHORT).show();
                                if(status.equals("200")){
                                     imaages.add(new Image(new JSONObject(response)
                                            .getJSONObject("data").getString("id") ,resultUri.getPath()));
                                    adapter.notifyDataSetChanged();
                                }

                                }

                            });
                        //    ((ImageView) findViewById(R.id.offer_preview_pic)).setImageBitmap(bitmap);
                         //   findViewById(R.id.offer_preview_pic).setVisibility(View.VISIBLE);
                         //   Toast.makeText(getApplicationContext(), R.string.pic_selected_succesfully, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {

                            Toast.makeText(getApplicationContext(), "error"+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        // Do something with the bitmap
                        // At the end remember to close the cursor or you will end with the RuntimeException!
                    }
                    break;


                case SELECT_PHOTO:
                    if (data != null) {
                        Uri pickedImage = data.getData();
                        String destinationFileName = "C_"+System.nanoTime() + ".jpg";


                        UCrop.of(pickedImage, Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(1, 1)
                                .withMaxResultSize(480, 480)
                                .start(EditWorkshop.this);
                    }
                    break;
                case  980:

                    Bundle extras = data.getExtras();
                    ArrayList<SelectableRegion> selected = ( ArrayList<SelectableRegion>)extras.getSerializable("selected");
                    Log.d("HBO"  , selected.size()+"");
                    findViewById(R.id.new_regions).setEnabled(false);
                    ArrayList<ApiRegion> res = new ArrayList<>();
                    for (SelectableRegion sr : selected)
                        if (sr.isSelected() || sr.isEmergency()) {
                            res.add(new ApiRegion(sr.getId(), sr.isEmergency() ? "1" : "0"));
                        }
                    String regions = new Gson().toJson(res);//[{"e":"1","id":"2"}]
                    Log.d("RRG", regions);
                    BackgroundServices.getInstance(EditWorkshop.this).setBaseUrl(APIUrl.SERVER+"update_profile")
                            .addPostParam("regions" ,  regions)
                            .CallPost(new PostAction() {
                                @Override
                                public void whenFinished(String status, String response) throws JSONException {
                                    findViewById(R.id.new_regions).setEnabled(true);
                                    Toast.makeText(EditWorkshop.this, new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();
                                    getGallery(id);
                                }
                            });

                    break;


            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.e("CROP ERR", cropError.getMessage());

        }


    }








String id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_workshop);
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"my_profile").CallPost(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                if(status.equals("200")){
                    final JSONObject data = new JSONObject(response).getJSONObject("data");




                        id = data.getString("id");
                    ( (TextView) findViewById(R.id.name)).setText(data.getString("name"));
                    ( (EditText) findViewById(R.id.username)).setText(data.getString("name"));
                    ( (EditText) findViewById(R.id.phone)).setText(data.getString("phone"));
                    ( (EditText) findViewById(R.id.landline)).setText(data.getString("landline"));
                    ( (EditText) findViewById(R.id.details)).setText(data.getString("details"));
                    ( (EditText) findViewById(R.id.address)).setText(data.getString("address"));
                    Glide.with(EditWorkshop.this).load(data.getString("profile_pic"))
                            .apply(RequestOptions.circleCropTransform())
                            .into(
                            ((ImageView)findViewById(R.id.cover_pic))
                    );


                    findViewById(R.id.go_logout).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPrefManager.getInstance(EditWorkshop.this).logout();
                            Intent intent=new Intent(EditWorkshop.this , SelectSignupType.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        }
                    });



                    findViewById(R.id.loading).setVisibility(View.GONE);
                    findViewById(R.id.main).setVisibility(View.VISIBLE);

                    findViewById(R.id.go_save).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {

                            if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
                                Toast.makeText(getApplicationContext(), "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            EditText name = ((EditText)findViewById(R.id.username));
                            EditText address = ((EditText)findViewById(R.id.address));
                            EditText phone = ((EditText)findViewById(R.id.phone));
                            EditText landline = ((EditText)findViewById(R.id.landline));
                            EditText details = ((EditText)findViewById(R.id.details));

                            if(name.getText().toString().trim().equals("")){
                                name.setHintTextColor(Color.RED);
                                name.requestFocus();
                                return;
                            }
                            if(address.getText().toString().trim().equals("")){
                                address.setHintTextColor(Color.RED);
                                address.requestFocus();
                                return;
                            }
                            if(phone.getText().toString().trim().equals("")){
                                phone.setHintTextColor(Color.RED);
                                phone.requestFocus();
                                return;
                            }

                            if(!phone.getText().toString().trim().startsWith("09") || phone.getText().toString().trim().length()!=10){
                                phone.setHintTextColor(Color.RED);
                                phone.requestFocus();
                                Toast.makeText(EditWorkshop.this, getString(R.string.phone_09), Toast.LENGTH_SHORT).show();
                                return;
                            }
                            if(landline.getText().toString().trim().equals("")){
                                landline.setHintTextColor(Color.RED);
                                landline.requestFocus();
                                return;
                            }  if(details.getText().toString().trim().equals("")){
                                details.setHintTextColor(Color.RED);
                                details.requestFocus();
                                return;
                            }


                            v.setEnabled(false);
                            BackgroundServices.getInstance(EditWorkshop.this).setBaseUrl(APIUrl.SERVER+"update_profile")
                                    .addPostParam("name" ,  ( (EditText) findViewById(R.id.username)).getText().toString().trim())
                                    .addPostParam("phone" ,  ( (EditText) findViewById(R.id.phone)).getText().toString().trim())
                                    .addPostParam("address" ,  ( (EditText) findViewById(R.id.address)).getText().toString().trim())
                                    .addPostParam("landline" ,  ( (EditText) findViewById(R.id.landline)).getText().toString().trim())
                                    .addPostParam("details" ,  ( (EditText) findViewById(R.id.details)).getText().toString().trim())
                                    .CallPost(new PostAction() {
                                @Override
                                public void whenFinished(String status, String response) throws JSONException {
                                    v.setEnabled(true);
                                    Toast.makeText(EditWorkshop.this, new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                    });

                    findViewById(R.id.go_save_password).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(final View v) {
                            if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
                                Toast.makeText(getApplicationContext(), "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
                                return;
                            }
                            EditText passsword = ((EditText)findViewById(R.id.password));
                            EditText password2 = ((EditText)findViewById(R.id.password2));

                            if(passsword.getText().toString().trim().equals("")){
                                passsword.setHintTextColor(Color.RED);
                                passsword.requestFocus();
                                return;
                            }
                            if(passsword.getText().toString().trim().length()<8){
                                passsword.setHintTextColor(Color.RED);
                                passsword.requestFocus();
                                Toast.makeText(EditWorkshop.this, getString(R.string.pass_hint), Toast.LENGTH_SHORT).show();
                                return;
                            }

                            if(!passsword.getText().toString().trim().equals(password2.getText().toString().trim())){
                                password2.setHintTextColor(Color.RED);
                                password2.requestFocus();
                                Toast.makeText(EditWorkshop.this, getString(R.string.pas_dznt_mtch), Toast.LENGTH_SHORT).show();
                                return;
                            }


                            v.setEnabled(false);
                            BackgroundServices.getInstance(EditWorkshop.this).setBaseUrl(APIUrl.SERVER+"update_profile")
                                    .addPostParam("password" ,  ( (EditText) findViewById(R.id.password)).getText().toString().trim())
                                    .CallPost(new PostAction() {
                                        @Override
                                        public void whenFinished(String status, String response) throws JSONException {
                                            v.setEnabled(true);
                                            Toast.makeText(EditWorkshop.this, new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();

                                        }
                                    });
                        }
                    });

                    getGallery(data.getString("id"));



                }
            }
        });

    }

    ArrayList<SelectableRegion> selected_regions;
    RecyclerView regions_list ;
    SelectedRegionsAdapterEditable regions_adapter;

    ArrayList<Image> imaages;
    GalleryAdapter adapter;
    void getGallery(String id){
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"get_workshop?id="+id).CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, final String response) throws JSONException {




                findViewById(R.id.loading_gallery).setVisibility(View.GONE);
                findViewById(R.id.portfolio_view).setVisibility(View.VISIBLE);
                findViewById(R.id.regions_view).setVisibility(View.VISIBLE);





                findViewById(R.id.go_upload).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            if (ActivityCompat.checkSelfPermission(EditWorkshop.this,
                                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(EditWorkshop.this,
                                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                        PICK_FROM_GALLERY);
                            } else {
                                pick();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
                if(status.equals("500")){
                    ((TextView)findViewById(R.id.error_log)).setText(response);
                }
                if(status.equals("200")){
                    JSONArray array = new JSONObject(response).getJSONObject("data").getJSONArray("gallery");
                    JSONArray curr_reg = new JSONObject(response).getJSONObject("data").getJSONArray("regions");
                    selected_regions =new ArrayList<>();
                    regions_list = findViewById(R.id.regions_list);

                    regions_adapter = new SelectedRegionsAdapterEditable(selected_regions, EditWorkshop.this);
                    regions_list.setLayoutManager(new LinearLayoutManager(EditWorkshop.this));

                    for(int i=0;i<curr_reg.length();i++)
                        selected_regions.add(new SelectableRegion(curr_reg.getJSONObject(i).getString("id")
                                ,curr_reg.getJSONObject(i).getString("name") ,true ,
                                curr_reg.getJSONObject(i).getString("service_type").equals("1")));

                    regions_list.setAdapter(regions_adapter);
                    regions_adapter.notifyDataSetChanged();







                    findViewById(R.id.new_regions).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent ii = new Intent(EditWorkshop.this ,SelectRegionsActivity.class);
                            ArrayList<City> cities = new ArrayList<>();
                            try {
                                JSONArray cities_arr =  new JSONObject(response).getJSONObject("data").getJSONArray("system_cities");
                                for (int i = 0; i < cities_arr.length(); i++) {
                                    cities.add(new City(cities_arr.getJSONObject(i)));

                                }
                                ii.putExtra("cities" ,cities);
                                startActivityForResult(ii ,980);



                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                     imaages = new ArrayList<>();
                    for(int i=0;i< array.length();i++)imaages.add(new Image(array.getJSONObject(i)));

                    RecyclerView list = (RecyclerView)findViewById(R.id.pics_list);
                    list.setLayoutManager(new GridLayoutManager(EditWorkshop.this ,2));
                    adapter =new GalleryAdapter(imaages ,EditWorkshop.this ,true);
                    list.setAdapter(adapter);





                }
            }
        });
    }
}
