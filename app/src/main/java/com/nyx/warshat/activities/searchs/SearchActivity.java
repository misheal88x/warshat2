package com.nyx.warshat.activities.searchs;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.StringArrayAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class SearchActivity extends GlobalActivity {
    static JSONArray cities ,categories,brands;

    void init(){
        findViewById(R.id.reconnect).setVisibility(View.GONE);
        findViewById(R.id.go_search).setEnabled(false);
        final ProgressDialog dialog = new ProgressDialog(SearchActivity.this);
        dialog.setMessage("الرجاء الانتظار..");
        dialog.setCancelable(false);
        dialog.show();
        findViewById(R.id.main).setVisibility(View.GONE);
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"ws_init").CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                dialog.cancel();
                findViewById(R.id.main).setVisibility(View.VISIBLE);
                if(response.equals("")){
                    findViewById(R.id.reconnect).setVisibility(View.VISIBLE);
                }else {
                    findViewById(R.id.go_search).setEnabled(true);
                    JSONObject all = new JSONObject(response).getJSONObject("data");
                    categories = new JSONArray();
                    cities = all.getJSONArray("cities");
                    if (all.has("brands")){
                        if (all.getJSONArray("brands")!=null){
                            brands = new JSONArray();
                            JSONObject j = new JSONObject();
                            j.put("id","0");
                            j.put("name",getResources().getString(R.string.trade_mark));
                            j.put("created_at","2020-01-01");
                            brands.put(j);
                            for (int i = 0; i < all.getJSONArray("brands").length(); i++) {
                                brands.put(all.getJSONArray("brands").getJSONObject(i));
                            }
                        }else {
                            brands = new JSONArray();
                            JSONObject j = new JSONObject();
                            j.put("id","0");
                            j.put("name",getResources().getString(R.string.trade_mark));
                            j.put("created_at","2020-01-01");
                            brands.put(j);
                        }
                    }else {
                        brands = new JSONArray();
                        JSONObject j = new JSONObject();
                        j.put("id","0");
                        j.put("name",getResources().getString(R.string.trade_mark));
                        j.put("created_at","2020-01-01");
                        brands.put(j);
                    }
                    JSONArray temp = all.getJSONArray("categories");
                    for (int i = 0; i <temp.length() ; i++) {
                        if (temp.getJSONObject(i).getString("target_type").equals("5")){
                            categories.put(temp.getJSONObject(i));
                        }
                    }
                    //categories = all.getJSONArray("categories");
                    String[] c_names = new String[cities.length()];
                    String[] cats_names = new String[categories.length()];
                    String[] brands_names = new String[brands.length()];
                    for (int i = 0; i < cities.length(); i++)
                        c_names[i] = cities.getJSONObject(i).getString("name");
                    for (int i = 0; i < categories.length(); i++)
                        cats_names[i] = categories.getJSONObject(i).getString("name");
                    for (int i = 0; i < brands.length(); i++)
                        brands_names[i] = brands.getJSONObject(i).getString("name");
                    ((Spinner) findViewById(R.id.select_cat)).setAdapter(new
                            StringArrayAdapter(SearchActivity.this, cats_names));
                    ((Spinner) findViewById(R.id.select_city)).setAdapter(new
                            StringArrayAdapter(SearchActivity.this, c_names));
                    ((Spinner) findViewById(R.id.select_trade_mark)).setAdapter(new
                            StringArrayAdapter(SearchActivity.this, brands_names));

                    findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });


                    findViewById(R.id.go_search).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String name = ((EditText)findViewById(R.id.cname)).getText().toString().trim();

                            try {
                                String cat_id=categories.getJSONObject(  ((Spinner)
                                        findViewById(R.id.select_cat)).getSelectedItemPosition()).getString("id");
                                String city_id=cities.getJSONObject(  ((Spinner)
                                        findViewById(R.id.select_city)).getSelectedItemPosition()).getString("id");
                                String trades=brands.getJSONObject(  ((Spinner)
                                        findViewById(R.id.select_trade_mark)).getSelectedItemPosition()).getString("id");
                                Intent ii = new Intent(SearchActivity.this ,SearchResults.class);
                                ii.putExtra("name" ,name);
                                ii.putExtra("cat_id" ,cat_id);
                                ii.putExtra("city_id" ,city_id);
                                ii.putExtra("trades" ,trades);
                                ii.putExtra("type","company");
                                startActivity(ii);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.i("errrrr", "onClick: "+e.getMessage());
                            }
                        }
                    });
                }

            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        if(cities!=null && categories!=null){
            String[] c_names = new String[cities.length()];
            String[] cats_names = new String[categories.length()];
            for (int i = 0; i < cities.length(); i++) {
                try {
                    c_names[i] = cities.getJSONObject(i).getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            for (int i = 0; i < categories.length(); i++) {
                try {
                    cats_names[i] = categories.getJSONObject(i).getString("name");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            ((Spinner) findViewById(R.id.select_cat)).setAdapter(new
                    StringArrayAdapter(SearchActivity.this, cats_names));
            ((Spinner) findViewById(R.id.select_city)).setAdapter(new
                    StringArrayAdapter(SearchActivity.this, c_names));
            findViewById(R.id.go_search).setEnabled(true);

        }else {
            ((Spinner) findViewById(R.id.select_cat)).setAdapter(new
                    StringArrayAdapter(SearchActivity.this, new String[]{getString(R.string.work_cat)}));
            ((Spinner) findViewById(R.id.select_city)).setAdapter(new
                    StringArrayAdapter(SearchActivity.this, new String[]{getString(R.string.work_city)}));
            init();
        }

    }
}
