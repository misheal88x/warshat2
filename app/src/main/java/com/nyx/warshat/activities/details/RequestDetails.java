package com.nyx.warshat.activities.details;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import androidx.fragment.app.FragmentActivity;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.homes.WorkshopHome;
import com.nyx.warshat.adapters.GalleryAdapter;
import com.nyx.warshat.adapters.RequestDatesAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.helper.SharedPrefManager;
import com.nyx.warshat.models.Image;
import com.nyx.warshat.models.RequestItem;
import com.nyx.warshat.models.SelectedDate;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import Arabs.ArabTextView;

public class RequestDetails  extends FragmentActivity  {
    private ArabTextView edt_desc,tv_times;
    private String req_phone = "";

    ArrayList<SelectedDate> selected_dates = new ArrayList<>();
    ArrayList<Image> imaages=new ArrayList<>();
    GalleryAdapter adapter;



    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.from_right_in, R.anim.from_left_out);
        Locale locale = new Locale(SharedPrefManager.getInstance(this).getSavedLanguage());
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_details);
        edt_desc = findViewById(R.id.desc);
        tv_times = findViewById(R.id.times);
        if (!getIntent().getBooleanExtra("show_options",false)){
            findViewById(R.id.go_set_finished).setVisibility(View.GONE);
            findViewById(R.id.go_contact_customer).setVisibility(View.GONE);
            findViewById(R.id.go_set_started).setVisibility(View.GONE);
        }
        findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String id = getIntent().getStringExtra("id");
        ((TextView)findViewById(R.id.name)).setText(getString(R.string.request)+" "+id);
        retrive(id);

    }



    void retrive(final String id){
        Log.i("request_id", "retrive: "+id);
        BackgroundServices.getInstance(this)
                .setBaseUrl(APIUrl.SERVER+"requests/details?id="+id).CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {


//                int maxLogSize = 5000;
//                for(int i = 0; i <= response.length() / maxLogSize; i++) {
//                    int start = i * maxLogSize;
//                    int end = (i+1) * maxLogSize;
//                    end = end > response.length() ? response.length() : end;
//                    Log.d("SAXAFON", response.substring(start, end));
//                }




              findViewById(R.id.loading).setVisibility(View.GONE);
               final JSONObject data = new JSONObject(response).getJSONObject("data");
                if (data.getString("problem_details")!=null){
                    if (!data.getString("problem_details").equals("")){
                        edt_desc.setText(data.getString("problem_details"));
                    }
                }
                selected_dates = new ArrayList<>();
                List<String> dates_list = new ArrayList<>();
                String times = "";
                imaages = new ArrayList<>();
                RecyclerView list = (RecyclerView)findViewById(R.id.pics_list);
                list.setLayoutManager(new GridLayoutManager(RequestDetails.this ,3));
                adapter =new GalleryAdapter(imaages ,RequestDetails.this ,false);
                list.setAdapter(adapter);


     JSONArray dates = data.getJSONArray("req_dates");
     JSONArray pics = data.getJSONArray("pics");
     for(int i=0;i<pics.length();i++)
         imaages.add(new Image("" ,pics.getJSONObject(i).getString("pic")));
     adapter.notifyDataSetChanged();

                for(int i=0;i<dates.length();i++) {
                    dates_list.add(dates.getJSONObject(i).getString("val"));
                    times = dates.getJSONObject(i).getString("period");
                    //selected_dates.add(new SelectedDate(dates.getJSONObject(i).getString("val"),
                      //      dates.getJSONObject(i).getString("period")));
                }

                RecyclerView date_recycler = (RecyclerView)findViewById(R.id.dates_list);
                date_recycler.setLayoutManager(new GridLayoutManager(RequestDetails.this,2));
                RequestDatesAdapter dates_adapter = new RequestDatesAdapter(RequestDetails.this,dates_list);
                //date_list.setLayoutManager(new LinearLayoutManager(RequestDetails.this));
                //DatesAdapter dates_adapter = new DatesAdapter(selected_dates ,RequestDetails.this);

                date_recycler.setAdapter(dates_adapter);
                dates_adapter.notifyDataSetChanged();
                tv_times.setText(times.replace(",","\n"));

                ((TextView)findViewById(R.id.username)).setText(data.getString("customer_name"));
                ((TextView)findViewById(R.id.phone)).setText(data.getString("req_phone"));
                if (data.getString("req_phone")!=null&&!data.getString("req_phone").equals("")){
                    req_phone = data.getString("req_phone");
                }else {
                    req_phone = data.getString("customer_phone");
                }
                //((TextView)findViewById(R.id.details)).setText(data.getString("problem_details"));
                if (data.getString("workshop_region_name")!=null&&data.getString("workshop_city_name")!=null){
                    if (!data.getString("workshop_region_name").equals("")){
                        ((ArabTextView)findViewById(R.id.details)).setText(data.getString("workshop_city_name")+" - "+
                                data.getString("workshop_region_name"));
                    }else {
                        findViewById(R.id.details).setVisibility(View.GONE);
                    }
                }else {
                    findViewById(R.id.details).setVisibility(View.GONE);
                }
//                int days = data.getInt("days_till_deadline");
//                if(days==1)((RadioButton)findViewById(R.id.option1)).setChecked(true);
//              else  if(days==2)((RadioButton)findViewById(R.id.option2)).setChecked(true);
//              else ((RadioButton)findViewById(R.id.option3)).setChecked(true);
//                ((RadioButton)findViewById(R.id.pm)).setChecked(data.getString("deadline_period").equals("1"));

if(data.getString("status").equals(RequestItem.REQUEST_NEW)){



    if (getIntent().getBooleanExtra("show",false)) {
        findViewById(R.id.go_set_started).setVisibility(View.VISIBLE);
    }
    findViewById(R.id.go_set_started).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            v.setEnabled(false);
            try {
                BackgroundServices.getInstance(RequestDetails.this).setBaseUrl(APIUrl.SERVER + "requests/update")
                        .addPostParam("request_id", id)
                        .addPostParam("status", "1")
                        .CallPost(new PostAction() {
                            @Override
                            public void whenFinished(String status, String response) throws JSONException {
                                Intent intent = new Intent(RequestDetails.this, WorkshopHome.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();

                            }
                        });
            }catch (Exception e){
                Toast.makeText(RequestDetails.this, "عذرا, لقد حدث خطأ", Toast.LENGTH_SHORT).show();
            }
        }
    });
    if (getIntent().getBooleanExtra("show",false)) {
        findViewById(R.id.go_contact_customer).setVisibility(View.VISIBLE);
    }
    findViewById(R.id.go_contact_customer).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            try {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel: "+req_phone));
                startActivity(callIntent);
            } catch (Exception e) {
                Toast.makeText(RequestDetails.this, "يوجد خطأ في رقم الزبون", Toast.LENGTH_SHORT).show();
            }
        }
    });
}else if(data.getString("status").equals(RequestItem.REQUEST_STARTED)) {
    if (getIntent().getBooleanExtra("show",false)) {
        findViewById(R.id.go_set_finished).setVisibility(View.VISIBLE);
    }
    findViewById(R.id.go_set_finished).setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(final View v) {

            v.setEnabled(false);
            BackgroundServices.getInstance(RequestDetails.this).setBaseUrl(APIUrl.SERVER+"requests/update")
                    .addPostParam("request_id" ,id)
                    .addPostParam("status" ,"3")
                    .CallPost(new PostAction() {
                        @Override
                        public void whenFinished(String status, String response) throws JSONException {
                            Intent intent=new Intent(RequestDetails.this , WorkshopHome.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
//                                int maxLogSize = 1000;
//                                for(int i = 0; i <= response.length() / maxLogSize; i++) {
//                                    int start = i * maxLogSize;
//                                    int end = (i+1) * maxLogSize;
//                                    end = end > response.length() ? response.length() : end;
//                                    Log.d("SAXAFON", response.substring(start, end));
//                                }
                        }
                    });


        }
    });
}

/*

                SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                        .findFragmentById(R.id.map);
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {

                        GoogleMap  mMap = googleMap;

                        // Add a marker in Sydney and move the camera
                        LatLng sydney = null;
                        try {
                            sydney = new LatLng(data.getDouble("lat"), data.getDouble("lng"));
                            mMap.addMarker(new MarkerOptions().position(sydney).title(getString(R.string.customer_location)));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                });
                **/
                Glide.with(RequestDetails.this).load(data.getString("request_pic"))
                        .apply(RequestOptions.placeholderOf(R.drawable.ic_circle_user_big))
                        .into(
                        ((ImageView)findViewById(R.id.pic)));

                findViewById(R.id.loading).setVisibility(View.GONE);
                findViewById(R.id.main).setVisibility(View.VISIBLE);

            }
        });
    }


}
