package com.nyx.warshat.activities.details;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.warshat.APIsClass.RequestsAPIsClass;
import com.nyx.warshat.Intrafaces.IFailure;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.infinites.CustomerRequests;
import com.nyx.warshat.helper.SharedPrefManager;
import com.nyx.warshat.models.SelectedDate;

import java.util.ArrayList;
import java.util.List;

import Arabs.ArabButton;
import Arabs.ArabCheckbox;
import Arabs.ArabEditText;

public class AddRequestStepFour extends GlobalActivity {

    private ImageButton btn_back;
    private ArabButton btn_next;
    private ArabCheckbox chk_mine,chk_other;
    private ArabEditText edt_phone_number;
    private int selected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_request_step_four);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //ImageButton
        btn_back = findViewById(R.id.go_back);
        //Button
        btn_next = findViewById(R.id.next_btn);
        //Checkbox
        chk_mine = findViewById(R.id.mine);
        chk_other = findViewById(R.id.other);
        //EditText
        edt_phone_number = findViewById(R.id.phone);
    }

    private void init_events() {
        chk_mine.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    chk_other.setChecked(false);
                    btn_next.setText("إرسال الطلبية");
                    selected = 1;
                    edt_phone_number.setVisibility(View.GONE);
                }
            }
        });
        chk_other.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    chk_mine.setChecked(false);
                    btn_next.setText("التالي");
                    selected = 2;
                    edt_phone_number.setVisibility(View.VISIBLE);
                }
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected == 0){
                    Toast.makeText(AddRequestStepFour.this, "يجب  عليك اختيار احد الخيارين السابقين", Toast.LENGTH_SHORT).show();
                    return;
                } else if (selected == 1){
                    callSendRequestAPI();
                }else {
                    if (edt_phone_number.getText().toString().equals("")){
                        Toast.makeText(AddRequestStepFour.this, "يجب عليك كتابة رقم الموبايل الجديد", Toast.LENGTH_SHORT).show();
                        return;
                    }
                    Intent intent = new Intent(AddRequestStepFour.this,AddRequestStepFive.class);
                    intent.putExtra("dates",getIntent().getStringExtra("dates"));
                    intent.putExtra("times",getIntent().getStringExtra("times"));
                    intent.putExtra("desc",getIntent().getStringExtra("desc"));
                    intent.putExtra("images",getIntent().getStringExtra("images"));
                    intent.putExtra("phone",edt_phone_number.getText().toString().trim());
                    intent.putExtra("id",getIntent().getStringExtra("id"));
                    startActivity(intent);
                }
            }
        });
    }

    private void init_activity() {
        chk_mine.setText("الرقم المؤكد في حسابي ("+SharedPrefManager.getInstance(AddRequestStepFour.this).getPhoneNumber()+")");
    }

    private void callSendRequestAPI(){
        btn_next.setEnabled(false);
        String dates = getIntent().getStringExtra("dates");
        List<SelectedDate> selectedDates = new ArrayList<>();
        String[] dates_array = new Gson().fromJson(dates,String[].class);
        for (String s : dates_array){
            selectedDates.add(new SelectedDate(s,getIntent().getStringExtra("times")));
        }
        String[] images = new Gson().fromJson(getIntent().getStringExtra("images"),String[].class);
        List<String> images_list = new ArrayList<>();

        if (images.length>0) {
            for (String s : images) {
                images_list.add(s);
            }
        }
        /*
        Log.i("request_params", "callSendRequestAPI: "+"request_pic[] "+s);
        Log.i("request_params", "callSendRequestAPI: "+"to_id "+getIntent().getStringExtra("id"));
        Log.i("request_params", "callSendRequestAPI: "+"lat "+"0.0");
        Log.i("request_params", "callSendRequestAPI: "+"lng "+"0.0");
        Log.i("request_params", "callSendRequestAPI: "+"req_phone "+SharedPrefManager.getInstance(AddRequestStepFour.this).getPhoneNumber());
        Log.i("request_params", "callSendRequestAPI: "+"dates "+new Gson().toJson(selectedDates));
        Log.i("request_params", "callSendRequestAPI: "+"problem_details "+getIntent().getStringExtra("desc"));
**/
        RequestsAPIsClass.addRequest(AddRequestStepFour.this,
                getIntent().getStringExtra("id"),
                "0.0",
                "0.0",
                SharedPrefManager.getInstance(AddRequestStepFour.this).getPhoneNumber(),
                new Gson().toJson(selectedDates),
                getIntent().getStringExtra("desc"),
                images_list,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        btn_next.setEnabled(true);
                        Toast.makeText(AddRequestStepFour.this, "حدث خطأ ما", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onNotActivated() {
                        btn_next.setEnabled(true);
                    }

                    @Override
                    public void onResponse(Object json) {
                        btn_next.setEnabled(true);
                        Toast.makeText(AddRequestStepFour.this, getString(R.string.sent_succesfully), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(AddRequestStepFour.this , CustomerRequests.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        intent.putExtra("is_from_notifications",false);
                        startActivity(intent);
                        finish();
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        btn_next.setEnabled(true);
                        Toast.makeText(AddRequestStepFour.this, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                    }
                });
        /*
        BackgroundServices bg =    BackgroundServices.getInstance(AddRequestStepFour.this)
                .setBaseUrl(APIUrl.SERVER+"requests/new");
        for (String s : images)
            bg.addFile("request_pic[]" ,new File(s));
        bg.addPostParam("to_id" ,getIntent().getStringExtra("id"))
                .addPostParam("lat" ,"0.0")
                .addPostParam("req_phone" ,SharedPrefManager.getInstance(AddRequestStepFour.this).getPhoneNumber())
                .addPostParam("lng" ,"0.0")
                .addPostParam("dates" ,new Gson().toJson(selectedDates))
                .addPostParam("problem_details" ,getIntent().getStringExtra("desc"))
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        btn_next.setEnabled(true);
                        if(status.equals("200")){
                            Toast.makeText(AddRequestStepFour.this, getString(R.string.sent_succesfully), Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(AddRequestStepFour.this , CustomerRequests.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }else{
                            Toast.makeText(AddRequestStepFour.this, response, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                **/

    }
}
