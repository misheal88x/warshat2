package com.nyx.warshat.activities.logins;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.nyx.warshat.APIsClass.AccountAPIsClass;
import com.nyx.warshat.Intrafaces.IFailure;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;

import Arabs.ArabButton;
import Arabs.ArabCheckbox;
import Arabs.ArabTextView;

public class AcceptPrivacyActivity extends GlobalActivity {

    private ImageButton btn_back;
    private ArabTextView tv_text;
    private ArabCheckbox chk_accept;
    private ArabButton btn_ok;
    private ScrollView scrollView;
    private LinearLayout lyt_loading,lyt_no_items_found,lyt_no_connection;
    private boolean isAccpetChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accept_privacy);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //ImageButton
        btn_back = findViewById(R.id.go_back);
        //TextView
        tv_text = findViewById(R.id.accept_privacy_text);
        //CheckBox
        chk_accept = findViewById(R.id.accept_privacy_check);
        //Button
        btn_ok = findViewById(R.id.accept_privacy_ok);
        //ScrollView
        scrollView = findViewById(R.id.accept_privacy_scroll);
        //LinearLayout
        lyt_loading = findViewById(R.id.loading);
        lyt_no_items_found = findViewById(R.id.no_items_found);
        lyt_no_connection = findViewById(R.id.no_connected_to_internet);
    }

    private void init_events() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        chk_accept.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                isAccpetChecked = isChecked;
            }
        });
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isAccpetChecked){
                    Toast.makeText(AcceptPrivacyActivity.this, "يجب عليك الموافقة على شروط الخصوصية", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra("privacy", true);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });
        lyt_no_connection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lyt_no_connection.setVisibility(View.GONE);
                callAPI();
            }
        });
    }

    private void init_activity() {
        callAPI();
    }

    private void callAPI(){
        lyt_loading.setVisibility(View.VISIBLE);
        scrollView.setVisibility(View.GONE);
        AccountAPIsClass.getPrivacy(AcceptPrivacyActivity.this,
                new IResponse() {
                    @Override
                    public void onResponse() {
                        scrollView.setVisibility(View.GONE);
                        lyt_no_connection.setVisibility(View.VISIBLE);
                        lyt_loading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onNotActivated() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        lyt_loading.setVisibility(View.GONE);
                        if (json!=null){
                            String j = new Gson().toJson(json);
                            String success = new Gson().fromJson(j,String.class);
                            if (success!=null){
                                tv_text.setText(success);
                                scrollView.setVisibility(View.VISIBLE);
                            }else {
                                lyt_no_items_found.setVisibility(View.VISIBLE);
                            }
                        }else {
                            lyt_no_connection.setVisibility(View.VISIBLE);
                        }
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        scrollView.setVisibility(View.GONE);
                        lyt_no_connection.setVisibility(View.VISIBLE);
                        lyt_loading.setVisibility(View.GONE);
                    }
                });
    }


}
