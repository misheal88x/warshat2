package com.nyx.warshat.activities.logins;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nyx.warshat.APIsClass.AccountAPIsClass;
import com.nyx.warshat.Intrafaces.IFailure;
import com.nyx.warshat.Intrafaces.IResponse;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;

import Arabs.ArabButton;

public class SignupAsCustomer extends GlobalActivity {

    private Button btn_accept_privacy;
    private boolean privacy_accepted = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_as_customer);
        btn_accept_privacy = findViewById(R.id.accept_privacy_btn);
        btn_accept_privacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupAsCustomer.this,AcceptPrivacyActivity.class);
                startActivityForResult(intent,1);
            }
        });
     //   if(getIntent().getStringExtra("name")!=null)((EditText)findViewById(R.id.name)).setText(getIntent().getStringExtra("name"));
        //Log.d("CIJ" ,getIntent().getStringExtra("method"));
        findViewById(R.id.continue_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                /*
                if(!((CheckBox)findViewById(R.id.terms_accepting)).isChecked()){
                    Toast.makeText(SignupAsCustomer.this, getString(R.string.terms_acc),
                            Toast.LENGTH_SHORT).show();
                    findViewById(R.id.terms_accepting).requestFocus();
                    return;
                }
**/
                EditText name = ((EditText)findViewById(R.id.name));
                //EditText address = ((EditText)findViewById(R.id.address));
                EditText email = ((EditText)findViewById(R.id.email));
                final EditText phone = ((EditText)findViewById(R.id.phone));
                EditText passsword = ((EditText)findViewById(R.id.password));
                EditText password2 = ((EditText)findViewById(R.id.password2));

                if (!privacy_accepted){
                    Toast.makeText(SignupAsCustomer.this, "يجب الموافقة على شروط الخصوصية", Toast.LENGTH_SHORT).show();
                    return;
                }
                if(name.getText().toString().trim().equals("")){
                    name.setHintTextColor(Color.RED);
                    name.requestFocus();
                    return;
                }
                if(email.getText().toString().trim().equals("")){
                    email.setHintTextColor(Color.RED);
                    email.requestFocus();
                    return;
                }
                if (!isValidEmail(email.getText().toString().trim())){
                    email.setHintTextColor(Color.RED);
                    email.requestFocus();
                    Toast.makeText(SignupAsCustomer.this, "يجب عليك كتابة البريد الإلكتروني بالطريقة الصحيحة", Toast.LENGTH_SHORT).show();
                    return;
                }
                /*
                if(address.getText().toString().trim().equals("")){
                    address.setHintTextColor(Color.RED);
                    address.requestFocus();
                    return;
                }
                **/
                if(phone.getText().toString().trim().equals("")){
                    phone.setHintTextColor(Color.RED);
                    phone.requestFocus();
                    return;
                }


                if(passsword.getText().toString().trim().equals("")){
                    passsword.setHintTextColor(Color.RED);
                    passsword.requestFocus();
                    return;
                }
                if(passsword.getText().toString().trim().length()<8){
                    passsword.setHintTextColor(Color.RED);
                    passsword.requestFocus();
                    Toast.makeText(SignupAsCustomer.this, getString(R.string.pass_hint), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!phone.getText().toString().trim().startsWith("09") || phone.getText().toString().trim().length()!=10){
                    phone.setHintTextColor(Color.RED);
                    phone.requestFocus();
                    Toast.makeText(SignupAsCustomer.this, getString(R.string.phone_09), Toast.LENGTH_SHORT).show();
                    return;
                }
                if(!passsword.getText().toString().trim().equals(password2.getText().toString().trim())){
                    password2.setHintTextColor(Color.RED);
                    password2.requestFocus();
                    Toast.makeText(SignupAsCustomer.this, getString(R.string.pas_dznt_mtch), Toast.LENGTH_SHORT).show();
                    return;
                }

                /*
                v.setEnabled(false);
                Intent intent = new Intent(SignupAsCustomer.this,ConfirmAccountActivity.class);
                intent.putExtra("login_type","2");
                intent.putExtra("user_type","1");
                intent.putExtra("name",name.getText().toString());
                intent.putExtra("phone",phone.getText().toString());
                intent.putExtra("password",passsword.getText().toString());
                intent.putExtra("c_password",passsword.getText().toString());
                intent.putExtra("email",email.getText().toString());
                startActivity(intent);
                **/

                checkPhoneNumber();
                /*
                BackgroundServices.getInstance(SignupAsCustomer.this).setBaseUrl(APIUrl.SERVER+"login")
                        .addPostParam("login_type" ,"2")
                        .addPostParam("user_type" ,"1")
                        .addPostParam("name" ,name.getText().toString().trim())
                        .addPostParam("phone" ,phone.getText().toString().trim())
                        //.addPostParam("address" ,address.getText().toString().trim())
                        .addPostParam("password" ,passsword.getText().toString().trim())
                        .addPostParam("c_password" ,passsword.getText().toString().trim())
                        .addPostParam("email" ,email.getText().toString().trim())

                        .CallPost(new PostAction() {
                            @Override
                            public void whenFinished(String status, String response) throws JSONException {
                              //  ((EditText)findViewById(R.id.name)).setText(response);
                                v.setEnabled(true);

                                if(!status.equals("200")) {
                                    Toast.makeText(SignupAsCustomer.this, new JSONObject(response).getString("message"),
                                            Toast.LENGTH_SHORT).show();
                                }
                                if(status.equals("200")) {
                                    JSONObject data=new JSONObject(response).getJSONObject("data");
                                    String token =data.getString("token");
                                    String type ="1";
                                    SharedPrefManager.getInstance(SignupAsCustomer.this).saveToken(token ,type);
                                    Intent intent = new Intent(SignupAsCustomer.this,ConfirmAccountActivity.class);
                                    intent.putExtra("phone_number",phone.getText().toString());
                                    startActivity(intent);
                                    /*
                                    Intent intent=new Intent(SignupAsCustomer.this , SelectSignupType.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    finish();

                                }
                            }
                        });
                    **/


            }
        });
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 1){
            if(resultCode == Activity.RESULT_OK){
                privacy_accepted=data.getBooleanExtra("privacy",false);
            }
        }
    }

    private void checkPhoneNumber(){
        final ArabButton btn = ((ArabButton)findViewById(R.id.continue_btn));
        btn.setEnabled(false);
        final EditText name = ((EditText)findViewById(R.id.name));
        final EditText email = ((EditText)findViewById(R.id.email));
        final EditText phone = ((EditText)findViewById(R.id.phone));
        final EditText passsword = ((EditText)findViewById(R.id.password));
        AccountAPIsClass.check_phone(SignupAsCustomer.this,
                phone.getText().toString(),
                email.getText().toString(),
                new IResponse() {
                    @Override
                    public void onResponse() {
                        btn.setEnabled(true);
                    }

                    @Override
                    public void onNotActivated() {

                    }

                    @Override
                    public void onResponse(Object json) {
                        Intent intent = new Intent(SignupAsCustomer.this,ConfirmAccountActivity.class);
                        intent.putExtra("login_type","2");
                        intent.putExtra("user_type","1");
                        intent.putExtra("name",name.getText().toString());
                        intent.putExtra("phone",phone.getText().toString());
                        intent.putExtra("password",passsword.getText().toString());
                        intent.putExtra("c_password",passsword.getText().toString());
                        intent.putExtra("email",email.getText().toString());
                        startActivity(intent);
                    }
                }, new IFailure() {
                    @Override
                    public void onFailure() {
                        Toast.makeText(SignupAsCustomer.this, "لا يوجد اتصال بالانترنت", Toast.LENGTH_SHORT).show();
                        btn.setEnabled(true);
                    }
                });

    }
}
