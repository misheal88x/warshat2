package com.nyx.warshat.activities.edits;

import android.app.Activity;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.nyx.warshat.R;
import com.nyx.warshat.adapters.SelectedRegionsAdapter;
import com.nyx.warshat.adapters.StringArrayAdapter;
import com.nyx.warshat.models.City;
import com.nyx.warshat.models.Region;
import com.nyx.warshat.models.SelectableRegion;

import java.util.ArrayList;

public class SelectRegionsActivity extends Activity {
    ArrayList<SelectableRegion> selected_regions;
    RecyclerView regions_list ;
    SelectedRegionsAdapter regions_adapter;
    ArrayList<City> cities;



    void loadRegions(int city_index){
        Log.d("LOADING_REGIONS"  , cities.get(city_index).getName());
        selected_regions .clear();
        regions_adapter.notifyDataSetChanged();
        ArrayList<Region> regions = cities.get(city_index).getRegions();
        for(Region r : regions)selected_regions.add(new SelectableRegion(r.getId() ,r.getName() ,false ,false));
        regions_adapter.notifyDataSetChanged();

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_regions);
        selected_regions = new ArrayList<>();
        regions_list = findViewById(R.id.regions_list);

        regions_adapter = new SelectedRegionsAdapter(selected_regions, SelectRegionsActivity.this);
        regions_list.setLayoutManager(new LinearLayoutManager(SelectRegionsActivity.this));
        regions_list.setAdapter(regions_adapter);
cities = (ArrayList<City>) getIntent().getSerializableExtra("cities");
        String[] c_names = new String[cities.size()];
        for (int i = 0; i < cities.size(); i++) {
            c_names[i] = cities.get(i).getName();

        }

        ((Spinner) findViewById(R.id.select_city)).setAdapter(new
                StringArrayAdapter(SelectRegionsActivity.this, c_names));
        ((Spinner) findViewById(R.id.select_city)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                Log.d("Cityc_CHNG", position + "");
                loadRegions(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
loadRegions(0);
findViewById(R.id.go).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent = new Intent();
        intent.putExtra("selected",selected_regions);
        setResult(RESULT_OK,intent);
        finish();
    }
});

    }
}
