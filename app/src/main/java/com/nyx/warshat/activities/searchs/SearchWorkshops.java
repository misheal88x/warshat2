package com.nyx.warshat.activities.searchs;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.adapters.StringArrayAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.City;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchWorkshops  extends GlobalActivity {
    static JSONArray  categories;
    ArrayList<City> cities;
    String region_id="-1";
    void init(){
        final ProgressDialog dialog = new ProgressDialog(SearchWorkshops.this);
        dialog.setMessage("الرجاء الانتظار..");
        dialog.setCancelable(false);
        dialog.show();
        findViewById(R.id.main).setVisibility(View.GONE);
        findViewById(R.id.reconnect).setVisibility(View.GONE);
        findViewById(R.id.go_search).setEnabled(false);
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"ws_init?filter=c32").CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                dialog.cancel();
                findViewById(R.id.main).setVisibility(View.VISIBLE);
                if(response.equals("")){
                    findViewById(R.id.reconnect).setVisibility(View.VISIBLE);
                }else {
                    //findViewById(R.id.loading).setVisibility(View.GONE);
                    findViewById(R.id.main).setVisibility(View.VISIBLE);
                    findViewById(R.id.go_search).setEnabled(true);
                    JSONObject all = new JSONObject(response).getJSONObject("data");

                    categories = all.getJSONArray("categories");

                    String[] cats_names = new String[categories.length()];

                    cities = new ArrayList<>();
                    JSONArray cities_arr = all.getJSONArray("cities");
                    String[] c_names = new String[cities_arr.length()];
                    for (int i = 0; i < cities_arr.length(); i++) {
                        cities.add(new City(cities_arr.getJSONObject(i)));

                    }
                    for (int i = 0; i < cities.size(); i++) {
                        c_names[i] = cities.get(i).getName();

                    }
                    ((Spinner) findViewById(R.id.select_city)).setAdapter(new
                            StringArrayAdapter(SearchWorkshops.this, c_names));
                    ((Spinner) findViewById(R.id.select_city)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                            final  City c = cities.get(position);
                            String[] r_names = new String[c.getRegions().size()];
                            for (int i = 0; i < c.getRegions().size(); i++) {
                                r_names[i] = c.getRegions().get(i).getName();

                            }
                            ((Spinner) findViewById(R.id.select_region)).setAdapter(new
                                    StringArrayAdapter(SearchWorkshops.this, r_names));
                            ((Spinner) findViewById(R.id.select_region)).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    region_id = c.getRegions().get(position).getId();
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {

                                }
                            });


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    for (int i = 0; i < categories.length(); i++)
                        cats_names[i] = categories.getJSONObject(i).getString("name");
                    ((Spinner) findViewById(R.id.select_cat)).setAdapter(new
                            StringArrayAdapter(SearchWorkshops.this, cats_names));
                    ((Spinner) findViewById(R.id.select_city)).setAdapter(new
                            StringArrayAdapter(SearchWorkshops.this, c_names));

                    findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    });


                    findViewById(R.id.go_search).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String name = ((EditText)findViewById(R.id.cname)).getText().toString().trim();
                            try {
                                String cat_id=categories.getJSONObject(  ((Spinner)
                                        findViewById(R.id.select_cat)).getSelectedItemPosition()).getString("id");
                                String city_id=cities.get(  ((Spinner)
                                        findViewById(R.id.select_city)).getSelectedItemPosition()).getId();
                                Intent ii = new Intent(SearchWorkshops.this ,SearchResults.class);
                                ii.putExtra("name" ,name);
                                ii.putExtra("cat_id" ,cat_id);
                                //ii.putExtra("city_id" ,city_id);
                                ii.putExtra("region_id" ,region_id);
                                ii.putExtra("is_emergency",getIntent().getBooleanExtra("is_emergency",false));
                                ii.putExtra("type","workshop");
                                startActivity(ii);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }

            }
        });
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_workshops);
     init();

    }
}
