package com.nyx.warshat.activities.edits;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.nyx.warshat.Intrafaces.IMove;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.logins.SelectSignupType;
import com.nyx.warshat.adapters.ProblemImagesAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.ConnectionUtils;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.helper.SharedPrefManager;
import com.nyx.warshat.models.ProblemImageObject;
import com.nyx.warshat.models.SelectedRegionsForSend;
import com.yalantis.ucrop.UCrop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import Arabs.ArabEditText;

public class NewEditWorkshop extends GlobalActivity {

    private ImageButton btn_back;
    private ArabEditText edt_name,edt_mobile,edt_land,edt_desc;
    private LinearLayout btn_update_basics,btn_edit_regions;
    private RecyclerView rv_images;
    private ArabEditText edt_old_pass,edt_new_pass,edt_c_pass;
    private LinearLayout btn_update_password,btn_logout;
    private RelativeLayout root;
    private ImageView icon;
    private LinearLayout btn_update_icon;
    private ProgressBar loading_icon;

    private final static int SELECT_PHOTO = 12345;
    private final static int PLACE_PICKER_REQUEST = 999;
    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    private String imagePath = "";
    private Bitmap bitmap;
    private Uri croppedImg;
    private static final int PICK_FROM_GALLERY = 14436;
    private List<String> imagesPathsList;
    private List<ProblemImageObject> imagesBitmapsList;
    private ProblemImagesAdapter imagesAdapter;

    private String id = "";
    private String selected_regions = "";
    private String selected_icon_path = "";
    private int clicked_image_type = 0;
    private String old_icone = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_edit_workshop);
        init_views();
        init_events();
        init_activity();
    }

    private void init_views() {
        //ImageButton
        btn_back = findViewById(R.id.go_back);
        //EditText
        edt_name = findViewById(R.id.username);
        edt_mobile = findViewById(R.id.mobile);
        edt_land = findViewById(R.id.landline);
        edt_desc = findViewById(R.id.desc);
        edt_old_pass = findViewById(R.id.old_password);
        edt_new_pass = findViewById(R.id.new_password);
        edt_c_pass = findViewById(R.id.c_password);
        //LinearLayout
        btn_edit_regions = findViewById(R.id.edit_regions);
        btn_update_basics = findViewById(R.id.save_basics);
        btn_update_password = findViewById(R.id.edit_password);
        btn_update_icon = findViewById(R.id.save_icon);
        btn_logout = findViewById(R.id.logout);
        //RecyclerView
        rv_images = findViewById(R.id.images_recycler);
        //RelativeLayout
        root = findViewById(R.id.new_edit_workshop_layout);
        //ImageView
        icon = findViewById(R.id.icon);
        //ProgressBar
        loading_icon = findViewById(R.id.loading_icon);
    }

    private void init_events() {
        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefManager.getInstance(NewEditWorkshop.this).logout();
                Intent intent=new Intent(NewEditWorkshop.this , SelectSignupType.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });
        btn_update_basics.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_name.getText().toString().equals("")){
                    edt_name.requestFocus();
                    edt_name.setHintTextColor(Color.RED);
                    return;
                }
                if (edt_mobile.getText().toString().equals("")){
                    edt_mobile.requestFocus();
                    edt_mobile.setHintTextColor(Color.RED);
                    return;
                }
                if (edt_land.getText().toString().equals("")){
                    edt_land.requestFocus();
                    edt_land.setHintTextColor(Color.RED);
                    return;
                }
                if (edt_desc.getText().toString().equals("")){
                    edt_desc.requestFocus();
                    edt_desc.setHintTextColor(Color.RED);
                    return;
                }
                callUpdateBasicInfoAPI();
            }
        });
        btn_update_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ConnectionUtils.isNetworkAvailable(getApplicationContext())){
                    Toast.makeText(getApplicationContext(), "انت غير متصل بالانترنت , يرجى الاتصال و اعادة المحاولة", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (edt_old_pass.getText().toString().equals("")){
                    edt_old_pass.requestFocus();
                    edt_old_pass.setHintTextColor(Color.RED);
                    return;
                }
                if (edt_new_pass.getText().toString().equals("")){
                    edt_new_pass.requestFocus();
                    edt_new_pass.setHintTextColor(Color.RED);
                    return;
                }
                if (edt_c_pass.getText().toString().equals("")){
                    edt_c_pass.requestFocus();
                    edt_c_pass.setHintTextColor(Color.RED);
                    return;
                }
                if (edt_new_pass.getText().length()<8){
                    Toast.makeText(NewEditWorkshop.this, "يجب أن تكون كلمة المرور الجديدة ثماني محارف على الأقل", Toast.LENGTH_SHORT).show();
                }
                if (!edt_new_pass.getText().toString().equals(edt_c_pass.getText().toString())){
                    Toast.makeText(NewEditWorkshop.this, "يجب أن تقوم بتأكيد كلمة المرور بالشكل الصحيح", Toast.LENGTH_SHORT).show();
                }
                callUpdatePasswordAPI();
            }
        });
        btn_edit_regions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewEditWorkshop.this,EditWorkshopRegionsStepOne.class);
                intent.putExtra("regions",selected_regions);
                startActivity(intent);
                finish();
            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clicked_image_type = 2;
                try {
                    if (ActivityCompat.checkSelfPermission(NewEditWorkshop.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(NewEditWorkshop.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    } else {
                        pick();
                    }
                } catch (Exception e) {
                }
            }
        });

        btn_update_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selected_icon_path.equals("")){
                    Toast.makeText(NewEditWorkshop.this, "لم تقم باختيار ايقونة جديدة للورشة, قم بالضغط على الايقونة في الاعلى لاختيار أيقونة جديدة", Toast.LENGTH_LONG).show();
                    return;
                }
                loading_icon.setVisibility(View.VISIBLE);
                BackgroundServices.getInstance(NewEditWorkshop.this).setBaseUrl(APIUrl.SERVER+"update-cover")
                        .addFile("cover",new File(selected_icon_path))
                        .CallPost(new PostAction() {
                            @Override
                            public void whenFinished(String status, String response) throws JSONException {
                                loading_icon.setVisibility(View.GONE);
                                if (status.equals("200")){
                                    Toast.makeText(NewEditWorkshop.this, "تم تحديث الأيقونة بنجاح", Toast.LENGTH_SHORT).show();
                                    selected_icon_path = "";
                                }else {
                                    selected_icon_path = "";
                                    Glide.with(NewEditWorkshop.this).load(old_icone)
                                            .into(icon);
                                    Toast.makeText(NewEditWorkshop.this, "حدث خطأ ما, يرجى إعادة المحاولة", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
            }
        });
    }

    private void init_activity() {
        init_images_recycler();
        callBasicInfoAPI();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pick();
                } else {
                    Toast.makeText(getApplicationContext(), "Not allowed to open gallery", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }

    void pick() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }

        startActivityForResult(photoPickerIntent, SELECT_PHOTO);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("RESULTED ", requestCode + " and " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        Log.e("GOTCHA ", "yes");
                        final Uri resultUri = UCrop.getOutput(data);
                        Log.e("Picsaved1 in : ", resultUri.toString());
                        Log.e("Picsaved2 in : ", resultUri.getPath());
                        Log.e("Picsaved3 in : ", resultUri.getEncodedPath());

                        try {
                            if (clicked_image_type == 1) {
                                imagePath = resultUri.getPath();
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                                bitmap = BitmapFactory.decodeFile(resultUri.getPath(), options);
                                callAddImageAPI(imagePath, bitmap);
                            }else if (clicked_image_type == 2){
                                selected_icon_path = resultUri.getPath();
                                BitmapFactory.Options options = new BitmapFactory.Options();
                                options.inPreferredConfig = Bitmap.Config.ARGB_8888;
                                bitmap = BitmapFactory.decodeFile(resultUri.getPath(), options);
                                icon.setImageBitmap(bitmap);
                            }

                        } catch (Exception e) {
                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
                        }
                    }
                    break;


                case SELECT_PHOTO:
                    if (data != null) {
                        Uri pickedImage = data.getData();
                        String destinationFileName = "P_"+System.nanoTime() + ".jpg";


                        UCrop.of(pickedImage, Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(1, 1)
                                .withMaxResultSize(480, 480)
                                .start(NewEditWorkshop.this);
                    }
                    break;
            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.e("CROP ERR", cropError.getMessage());

        }


    }

    private void init_images_recycler(){
        imagesPathsList = new ArrayList<>();
        imagesBitmapsList = new ArrayList<>();
        imagesBitmapsList.add(new ProblemImageObject(null,0));
        imagesAdapter = new ProblemImagesAdapter(NewEditWorkshop.this, imagesBitmapsList, new IMove() {
            @Override
            public void move(int position) {
                try {
                    clicked_image_type = 1;
                    if (ActivityCompat.checkSelfPermission(NewEditWorkshop.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(NewEditWorkshop.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    } else {
                        pick();
                    }
                } catch (Exception e) {
                }
            }
        });
        rv_images.setLayoutManager(new LinearLayoutManager(NewEditWorkshop.this,LinearLayoutManager.HORIZONTAL,false));
        rv_images.setAdapter(imagesAdapter);
    }


    private void callAddImageAPI(final String imagePath, final Bitmap bitmap){
        BackgroundServices.getInstance(this)
                .setBaseUrl(APIUrl.SERVER+"gallery/new")
                .addFile("cover" ,new File(imagePath)).CallPost(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                Toast.makeText(NewEditWorkshop.this, new JSONObject(response)
                        .getString("message"), Toast.LENGTH_SHORT).show();
                if(status.equals("200")){
                    imagesBitmapsList.add(new ProblemImageObject(bitmap,1));
                    imagesPathsList.add(imagePath);
                    imagesAdapter.notifyDataSetChanged();
                }

            }

        });
    }

    private void callUpdateRegionsAPI(String regions_list){
        BackgroundServices.getInstance(NewEditWorkshop.this).setBaseUrl(APIUrl.SERVER+"update_profile")
                .addPostParam("regions" ,  regions_list)
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        findViewById(R.id.new_regions).setEnabled(true);
                        Toast.makeText(NewEditWorkshop.this, new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callBasicInfoAPI(){
        findViewById(R.id.main).setVisibility(View.GONE);
        findViewById(R.id.loading).setVisibility(View.VISIBLE);
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"my_profile").CallPost(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                if (status.equals("200")) {
                    final JSONObject data = new JSONObject(response).getJSONObject("data");
                    //ID
                    id = data.getString("id");
                    //Workshop name
                    if (data.getString("work_name")!=null){
                        edt_name.setText(data.getString("work_name"));
                    }
                    //Workshop mobile
                    if (data.getString("work_phone")!=null){
                        edt_mobile.setText(data.getString("work_phone"));
                    }
                    //Workshop landLine
                    edt_land.setText(data.getString("landline"));
                    //Description
                    edt_desc.setText(data.getString("details"));
                    //Icon
                    try{
                        Glide.with(NewEditWorkshop.this).load(data.getString("profile_pic"))
                                .into(icon);
                        old_icone = data.getString("profile_pic");
                    }catch (Exception e){}
                    callRestDataAPI(id);
                }else {
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Snackbar.make(root, "حدث خطأ ما, الرجاء إعادة المحاولة..", Snackbar.LENGTH_INDEFINITE)
                            .setAction("إعادة المحاولة", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    callBasicInfoAPI();
                                }
                            }).setActionTextColor(getResources().getColor(R.color.white)).show();
                }
            }
        });
    }

    private void callUpdateBasicInfoAPI(){
        btn_update_basics.setEnabled(false);
        BackgroundServices.getInstance(NewEditWorkshop.this).setBaseUrl(APIUrl.SERVER+"update_profile")
                .addPostParam("name" ,  edt_name.getText().toString().trim())
                .addPostParam("phone" ,  edt_mobile.getText().toString().trim())
                //.addPostParam("address" ,  ( (EditText) findViewById(R.id.address)).getText().toString().trim())
                .addPostParam("landline" ,  edt_land.getText().toString().trim())
                .addPostParam("details" ,  edt_desc.getText().toString())
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        btn_update_basics.setEnabled(true);
                        Toast.makeText(NewEditWorkshop.this, new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void callUpdatePasswordAPI(){
        btn_update_password.setEnabled(false);
        BackgroundServices.getInstance(NewEditWorkshop.this).setBaseUrl(APIUrl.SERVER+"update_profile")
                .addPostParam("password" ,  edt_new_pass.getText().toString().trim())
                .addPostParam("old_password",edt_old_pass.getText().toString().trim())
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        btn_update_password.setEnabled(true);
                        Toast.makeText(NewEditWorkshop.this, new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();
                        if (status.equals("200")){
                            edt_c_pass.setText("");
                            edt_new_pass.setText("");
                            edt_old_pass.setText("");
                        }
                    }
                });
    }

    private void callRestDataAPI(final String id){
        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"get_workshop?id="+id).CallGet(new PostAction() {
            @Override
            public void whenFinished(String status, final String response) throws JSONException {
                if(status.equals("500")){
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    Snackbar.make(root, "حدث خطأ ما, الرجاء إعادة المحاولة..", Snackbar.LENGTH_INDEFINITE)
                            .setAction("إعادة المحاولة", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    callRestDataAPI(id);
                                }
                            }).setActionTextColor(getResources().getColor(R.color.white)).show();
                }else if (status.equals("200")){
                    //Images
                    JSONArray array = new JSONObject(response).getJSONObject("data").getJSONArray("gallery");
                    if (array.length()>0){
                        for (int i = 0; i < array.length(); i++) {
                            ProblemImageObject pio = new ProblemImageObject();
                            pio.setUrl(array.getJSONObject(i).getString("pic_url"));
                            pio.setType(2);
                            imagesBitmapsList.add(pio);
                            imagesAdapter.notifyDataSetChanged();
                        }
                    }
                    //Regions
                    List<SelectedRegionsForSend> list = new ArrayList<>();
                    JSONArray curr_reg = new JSONObject(response).getJSONObject("data").getJSONArray("regions");
                    if (curr_reg.length()>0){
                        for (int i = 0; i < curr_reg.length(); i++) {
                            SelectedRegionsForSend srfo = new SelectedRegionsForSend();
                            srfo.setId(curr_reg.getJSONObject(i).getInt("region_id"));
                            srfo.setE(curr_reg.getJSONObject(i).getInt("service_type"));
                            list.add(srfo);
                        }
                        selected_regions = new Gson().toJson(list);
                    }
                    findViewById(R.id.loading).setVisibility(View.GONE);
                    findViewById(R.id.main).setVisibility(View.VISIBLE);
                }
            }
        });
    }
}
