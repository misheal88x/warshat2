package com.nyx.warshat.activities.logins;
import com.facebook.CallbackManager;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import androidx.annotation.NonNull;
import androidx.core.content.IntentCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.helper.SharedPrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;

public class LoginActivity extends GlobalActivity {


    private ProgressDialog mProgressDialog;
    private static final String TAG = "GMAILXC";
    private static final int RC_SIGN_IN = 007;
    CallbackManager callbackManager = CallbackManager.Factory.create();
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //  handleSignInResult(result);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //   final LoginButton loginButton = (LoginButton) findViewById(R.id.login_button_fb);


//
//        loginButton.setPermissions(Arrays.asList(
//                "public_profile", "email"));
//        // If you are using in a fragment, call loginButton.setFragment(this);
//
//        // Callback registration
//        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
//            @Override
//            public void onSuccess(LoginResult loginResult) {
//                // App code
//                GraphRequest request = GraphRequest.newMeRequest(
//                        loginResult.getAccessToken(),
//                new GraphRequest.GraphJSONObjectCallback() {
//                    @Override
//                    public void onCompleted(JSONObject object, GraphResponse response) {
//                        Log.v("LoginActivity", response.toString());
//
//                        // Application code
//                        try {
////                       Log.d("FROMFB" ,""+
////                               object.getString("id")+" "+
////                               object.getString("email")+" "+
////                               object.getString("name"));
////
//
//
//                        //    findViewById(R.id.go_facebook).setEnabled(true);
//                            Intent ii = new Intent(LoginActivity.this ,FbGmWait.class);
//                            ii.putExtra("name" ,object.getString("name"));
//                            ii.putExtra("email" ,object.getString("email"));
//                            ii.putExtra("method"  ,"1");
//                            ii.putExtra("social_id"  ,object.getString("id"));
//
//                            startActivity(ii);
//                            LoginManager.getInstance().logOut();
//                        }catch (Exception J){
//                            Log.d("FROMFB" ,"error "+J.getMessage());
//                        }
//                        //   String birthday = object.getString("birthday"); // 01/31/1980 format
//                    }
//                });
//                Bundle parameters = new Bundle();
//                parameters.putString("fields", "id,name,email");
//                request.setParameters(parameters);
//                request.executeAsync();
//            }
//
//            @Override
//            public void onCancel() {
//                // App code
//                Log.d("FROMFB" ,"Canceled ");
//            }
//
//            @Override
//            public void onError(FacebookException exception) {
//                // App code
//                Log.d("FROMFB" ,"OnErr "+exception.getMessage());
//
//            }
//        });
//
//        findViewById(R.id.go_facebook).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//
//                v.setEnabled(false);
//               loginButton.performClick();
//            }
//        });

        findViewById(R.id.go_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final EditText email = (EditText) findViewById(R.id.username);
                EditText pass = (EditText) findViewById(R.id.password);
                if (email.getText().toString().trim().equals("")) {
                    email.setHintTextColor(Color.RED);
                    email.requestFocus();
                    return;
                }
                if (pass.getText().toString().trim().equals("")) {
                    pass.setHintTextColor(Color.RED);
                    pass.requestFocus();
                    return;
                }
                v.setEnabled(false);
                BackgroundServices.getInstance(LoginActivity.this)
                        .setBaseUrl(APIUrl.SERVER + "login")
                        .addPostParam("login_type", "1")
                        .addPostParam("username", email.getText().toString())
                        .addPostParam("password", pass.getText().toString()).CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        v.setEnabled(true);
                        if (status.equals("403")){
                            Toast.makeText(LoginActivity.this,new JSONObject(response).getString("message") , Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this,ConfirmAccountActivity.class);
                            intent.putExtra("phone_number",email.getText().toString());
                            startActivity(intent);
                            finish();
                            return;
                        } else if (!status.equals("200")) {
                            Toast.makeText(LoginActivity.this,
                                    new JSONObject(response).getString("message"), Toast.LENGTH_SHORT).show();
                        } else {
                            JSONObject data = new JSONObject(response).getJSONObject("data");
                            String token = data.getString("token");
                            String type = data.getInt("user_type") + "";
                            Log.i("access_token", "whenFinished: "+token);
                            SharedPrefManager.getInstance(LoginActivity.this).saveToken(token, type);
                            SharedPrefManager.getInstance(LoginActivity.this).savePhoneNumber(email.getText().toString());
                            Intent intent = new Intent(LoginActivity.this, SelectSignupType.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                            finish();
                        }
                    }
                });

            }
        });
    }
}

//        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
//                .requestEmail()
//                .build();
//
//        mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
//                .enableAutoManage(LoginActivity.this,
//                        new GoogleApiClient.OnConnectionFailedListener() {
//                            @Override
//                            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
//                                Log.d(TAG ,"FAILED TO CONNECT");
//                            }
//                        })
//                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//                .build();

//        // Customizing G+ button
//        ((SignInButton) findViewById(R.id.btn_sign_in)).setSize(SignInButton.SIZE_STANDARD);
//       // ((SignInButton) findViewById(R.id.btn_sign_in)).setScopes(gso.getScopeArray());
//
//        findViewById(R.id.go_gmail).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                v.setEnabled(false);
//                signIn();
////                OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
////                if (opr.isDone()) {
////                    // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
////                    // and the GoogleSignInResult will be available instantly.
////                    Log.d(TAG, "Got cached sign-in");
////                    GoogleSignInResult result = opr.get();
////                    handleSignInResult(result);
////                } else {
////                    // If the user has not previously signed in on this device or the sign-in has expired,
////                    // this asynchronous branch will attempt to sign in the user silently.  Cross-device
////                    // single sign-on will occur in this branch.
////                    showProgressDialog();
////                    opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
////                        @Override
////                        public void onResult(GoogleSignInResult googleSignInResult) {
////                           hideProgressDialog();
////                            handleSignInResult(googleSignInResult);
////                        }
////                    });
////                }
//
//
//
//            }
//        });
//    }
//
//    private void handleSignInResult(GoogleSignInResult result) {
//        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
//        if (result.isSuccess()) {
//            // Signed in successfully, show authenticated UI.
//            GoogleSignInAccount acct = result.getSignInAccount();
//
//            Log.e(TAG, "display name: " + acct.getDisplayName());
//
//            String personName = acct.getDisplayName();
//           // String personPhotoUrl = acct.getPhotoUrl().toString();
//            String email = acct.getEmail();
////
////            Log.e(TAG, "Name: " + personName + ", email: " + email
////                    + ", Image: " + personPhotoUrl);
//findViewById(R.id.go_gmail).setEnabled(true);
//            findViewById(R.id.go_facebook).setEnabled(true);
//            Intent ii = new Intent(LoginActivity.this ,FbGmWait.class);
//            ii.putExtra("name" , acct.getDisplayName());
//            ii.putExtra("email" ,acct.getEmail());
//            ii.putExtra("method"  ,"2");
//            ii.putExtra("social_id"  ,acct.getId());
//
//            startActivity(ii);
//            try {
//                Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
//                        new ResultCallback<Status>() {
//                            @Override
//                            public void onResult(Status status) {
//
//                            }
//                        });
//            }catch (IllegalStateException rr){}
//          //  updateUI(true);
//        } else {
//            // Signed out, show unauthenticated UI.
//          //  updateUI(false);
//        }
//    }

//    private void signIn() {
//        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
//        startActivityForResult(signInIntent, RC_SIGN_IN);
//    }
//            private void showProgressDialog() {
//                if (mProgressDialog == null) {
//                    mProgressDialog = new ProgressDialog(this);
//                    mProgressDialog.setMessage(getString(R.string.please_wait));
//                    mProgressDialog.setIndeterminate(true);
//                }
//
//                mProgressDialog.show();
//            }
//
//            private void hideProgressDialog() {
//                if (mProgressDialog != null && mProgressDialog.isShowing()) {
//                    mProgressDialog.hide();
//                }
//            }
//    private void revokeAccess() {
//        Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient).setResultCallback(
//                new ResultCallback<Status>() {
//                    @Override
//                    public void onResult(Status status) {
//                      //  updateUI(false);
//                    }
//                });
//    }




//
//
//
//
//}
