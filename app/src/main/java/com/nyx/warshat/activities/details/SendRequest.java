package com.nyx.warshat.activities.details;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import android.os.Bundle;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
import com.nyx.warshat.R;
import com.nyx.warshat.activities.abstracts.GlobalActivity;
import com.nyx.warshat.activities.infinites.CustomerRequests;
import com.nyx.warshat.activities.logins.VerifyPhoneActivity;
import com.nyx.warshat.activities.maps.PickPlaceActivity;
import com.nyx.warshat.adapters.DatesAdapterEditable;
import com.nyx.warshat.adapters.GalleryAdapter;
import com.nyx.warshat.helper.APIUrl;
import com.nyx.warshat.helper.BackgroundServices;
import com.nyx.warshat.helper.PostAction;
import com.nyx.warshat.models.Image;
import com.nyx.warshat.models.SelectedDate;
import com.nyx.warshat.utilities.SelectHourPopUp;
import com.yalantis.ucrop.UCrop;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class SendRequest extends GlobalActivity {
    private final static int SELECT_PHOTO = 12345;
    private final static int PLACE_PICKER_REQUEST = 999;
    ArrayList<SelectedDate> selected_dates = new ArrayList<>();
    DatesAdapterEditable dates_adapter = new DatesAdapterEditable(selected_dates ,this);
    String final_phone = "" ,my_phone="";
    ArrayList<Image> imaages=new ArrayList<>();
    GalleryAdapter adapter;

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";
    String imagePath = "";
    Bitmap bitmap;
    boolean isUpdating = false;
    Uri croppedImg;
    private static final int PICK_FROM_GALLERY = 14436;

    @Override
    protected void onResume() {
        super.onResume();

        // Getting reference to TextView to show the status

        // Getting status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if(status!= ConnectionResult.SUCCESS)
        {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        }
    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        switch (requestCode) {
            case PICK_FROM_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    pick();
                } else {
                    Toast.makeText(getApplicationContext(), "Not allowed to open gallery", Toast.LENGTH_SHORT).show();
                    //do something like displaying a message that he didn`t allow the app to access gallery and you wont be able to let him select from gallery
                }
                break;
        }
    }



    void pick() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_GET_CONTENT)
                .setType("image/*")
                .addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            String[] mimeTypes = {"image/jpeg", "image/png"};
            photoPickerIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        }

        startActivityForResult(photoPickerIntent, SELECT_PHOTO);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("RESULTED ", requestCode + " and " + resultCode);

        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case VerifyPhoneActivity.VERIVY_PHONE:
                    if(data.getBooleanExtra("result" ,true)){
                        Toast.makeText(this, "تم التحقق من الرقم بنجاح", Toast.LENGTH_SHORT).show();
                        is_phone_verified = true;
                    }

                    break;
                case UCrop.REQUEST_CROP:
                    if (data != null) {
                        Log.e("GOTCHA ", "yes");
                        final Uri resultUri = UCrop.getOutput(data);
                        Log.e("Picsaved1 in : ", resultUri.toString());
                        Log.e("Picsaved2 in : ", resultUri.getPath());
                        Log.e("Picsaved3 in : ", resultUri.getEncodedPath());



                        try {
                            imagePath = resultUri.getPath();
                             imaages.add(new Image("" ,imagePath));
                                        adapter.notifyDataSetChanged();

//                            BackgroundServices.getInstance(this)
//                                    .setBaseUrl(APIUrl.SERVER+"gallery/new").addFile("cover" ,new File(imagePath)).CallPost(new PostAction() {
//                                @Override
//                                public void whenFinished(String status, String response) throws JSONException {
//
//                                    findViewById(R.id.go_upload).setEnabled(true);
//
//                                    Toast.makeText(EditWorkshop.this, new JSONObject(response)
//                                            .getString("message"), Toast.LENGTH_SHORT).show();
//                                    if(status.equals("200")){

//                                    }
//
//                                }
//
//                            });
                            //    ((ImageView) findViewById(R.id.offer_preview_pic)).setImageBitmap(bitmap);
                            //   findViewById(R.id.offer_preview_pic).setVisibility(View.VISIBLE);
                            //   Toast.makeText(getApplicationContext(), R.string.pic_selected_succesfully, Toast.LENGTH_SHORT).show();
                        } catch (Exception e) {

                            Toast.makeText(getApplicationContext(), "error"+e.getMessage(), Toast.LENGTH_SHORT).show();
                        }




//                        try {
//                            imagePath = resultUri.getPath();
//                            BitmapFactory.Options options = new BitmapFactory.Options();
//                            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
//                            bitmap = BitmapFactory.decodeFile(resultUri.getPath(), options);
//
//                            ((ImageView) findViewById(R.id.problem_pic)).setImageBitmap(bitmap);
//
//                        } catch (Exception e) {
//
//                            Toast.makeText(getApplicationContext(), "error", Toast.LENGTH_SHORT).show();
//                        }
                        // Do something with the bitmap
                        // At the end remember to close the cursor or you will end with the RuntimeException!
                    }
                    break;


                case SELECT_PHOTO:
                    if (data != null) {
                        Uri pickedImage = data.getData();
                        String destinationFileName = "P_"+System.nanoTime() + ".jpg";


                        UCrop.of(pickedImage, Uri.fromFile(new File(getCacheDir(), destinationFileName)))
                                .withAspectRatio(1, 1)
                                .withMaxResultSize(480, 480)
                                .start(SendRequest.this);
                    }
                    break;
                case PLACE_PICKER_REQUEST:
                    if (data != null) {

lat = data.getStringExtra("lat");
lng = data.getStringExtra("lng");

findViewById(R.id.loc_msg).setVisibility(View.VISIBLE);
                    }
                    break;


            }
        } else if (resultCode == UCrop.RESULT_ERROR) {
            final Throwable cropError = UCrop.getError(data);
            Log.e("CROP ERR", cropError.getMessage());

        }


    }



String lat=""
 ,lng="";


    void addate(String date ,String period){
       // Toast.makeText(this, date + " " + period, Toast.LENGTH_SHORT).show();
        selected_dates.add(new SelectedDate(date ,period));
        dates_adapter.notifyDataSetChanged();
    }

    boolean is_phone_verified = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_request);
        findViewById(R.id.go_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        RecyclerView list = (RecyclerView)findViewById(R.id.pics_list);
        list.setLayoutManager(new GridLayoutManager(SendRequest.this ,2));
        adapter =new GalleryAdapter(imaages ,SendRequest.this ,true);
        list.setAdapter(adapter);
        RecyclerView date_list = (RecyclerView)findViewById(R.id.dates_list);
        date_list.setLayoutManager(new LinearLayoutManager(this));
        date_list.setAdapter(dates_adapter);









        BackgroundServices.getInstance(this).setBaseUrl(APIUrl.SERVER+"my_profile").CallPost(new PostAction() {
            @Override
            public void whenFinished(String status, String response) throws JSONException {
                if(status.equals("200")) {
                    JSONObject data = new JSONObject(response).getJSONObject("data");
                    ((RadioButton)findViewById(R.id.same)).setText("الرقم المؤكد في حسابي ("+ data.getString("phone")+")");
                    my_phone = data.getString("phone");

                }
            }
        });



        findViewById(R.id.add_date).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar myCalendar = Calendar.getInstance();

                DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub

                        String d = dayOfMonth+"/" + monthOfYear + "/" + year;
                        String myFormat = "MM/dd/yyyy"; //In which you need put here
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                       final SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                     //   Toast.makeText(SendRequest.this, , Toast.LENGTH_SHORT).show();


                        SelectHourPopUp pop = new SelectHourPopUp(SendRequest.this ,"") {
                            @Override
                            public void yesClicked(String optiona) {
                                addate(sdf.format(myCalendar.getTime())  ,optiona);
                            }
                        };

pop.show();



                    }

                };
                new DatePickerDialog(SendRequest.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



findViewById(R.id.pick).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent i = new Intent(SendRequest.this, PickPlaceActivity.class);
        startActivityForResult(i, PLACE_PICKER_REQUEST);
    }
});

        String id = getIntent().getStringExtra("id");
        ((TextView)findViewById(R.id.name2)).setText(getIntent().getStringExtra("name"));
        Glide.with(this).load(getIntent().getStringExtra("icon")).into(
                ((ImageView)findViewById(R.id.pic)));
        ((RatingBar)findViewById(R.id.rating)).setRating(getIntent().getFloatExtra("rating" ,0.0f));
        findViewById(R.id.problem_pic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (ActivityCompat.checkSelfPermission(SendRequest.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(SendRequest.this,
                                new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                        Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                PICK_FROM_GALLERY);
                    } else {
                        pick();
                    }
                } catch (Exception e) {
                }

            }
        });

findViewById(R.id.go_send).setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(final View v) {



        EditText d = (EditText)findViewById(R.id.details);
        if(d.getText().toString().trim().equals("")){
            d.setHintTextColor(Color.RED);
            Toast.makeText(SendRequest.this, "أدخل وصف مشكلتك من فضلك", Toast.LENGTH_SHORT).show();
            d.requestFocus();
            return;
        }



        if(lat==null || lng==null ||  lat.equals("") || lng.equals("")){
            Toast.makeText(SendRequest.this, R.string.no_locations, Toast.LENGTH_SHORT).show();
            Intent i = new Intent(SendRequest.this, PickPlaceActivity.class);
            startActivityForResult(i, PLACE_PICKER_REQUEST);
            return;
        }
        if(imaages.size()==0){
            Toast.makeText(SendRequest.this, getString(R.string.problem_pic_not_selected), Toast.LENGTH_SHORT).show();
            try {
                if (ActivityCompat.checkSelfPermission(SendRequest.this,
                        Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SendRequest.this,
                            new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            PICK_FROM_GALLERY);
                } else {
                    pick();
                }
            } catch (Exception e) {
            }
            return;
        }

        if(selected_dates.size()==0){
            Toast.makeText(SendRequest.this, "يجب اختيار يوم على الأقل للتصليحة", Toast.LENGTH_SHORT).show();

            return;
        }




       if(((RadioButton)findViewById(R.id.same)).isChecked() ){
           final_phone = my_phone;
       }else {

           EditText phone = (EditText) findViewById(R.id.phone);
           if (phone.getText().toString().trim().equals("")) {
               phone.setHintTextColor(Color.RED);
               Toast.makeText(SendRequest.this, "أدخل رقم الهاتف من فضلك", Toast.LENGTH_SHORT).show();
               phone.requestFocus();
               return;
           }

           if ( !is_phone_verified) {
               Toast.makeText(SendRequest.this, "قم بالتحقق من رقم الهاتف", Toast.LENGTH_SHORT).show();
               Intent i = new Intent(SendRequest.this, VerifyPhoneActivity.class);
               startActivityForResult(i, VerifyPhoneActivity.VERIVY_PHONE);
               return;
           }else {

                   final_phone = phone.getText().toString().trim();
           }
       }

//        if(true){
//            Toast.makeText(SendRequest.this, "لا يزال السيرفر قيد البرمجة", Toast.LENGTH_SHORT).show();
//            return;
//        }

        v.setEnabled(false);
     BackgroundServices bg =    BackgroundServices.getInstance(SendRequest.this)
        .setBaseUrl(APIUrl.SERVER+"requests/new");
     for (Image s : imaages)
                bg.addFile("request_pic[]" ,new File(s.getPath()));
                bg.addPostParam("to_id" ,getIntent().getStringExtra("id"))
                .addPostParam("lat" ,lat)
                .addPostParam("req_phone" ,final_phone)
                .addPostParam("lng" ,lng)
                .addPostParam("dates" ,new Gson().toJson(selected_dates))
                .addPostParam("problem_details" ,d.getText().toString().trim())
                .CallPost(new PostAction() {
                    @Override
                    public void whenFinished(String status, String response) throws JSONException {
                        v.setEnabled(true);
                        if(status.equals("200")){
                            Toast.makeText(SendRequest.this, getString(R.string.sent_succesfully), Toast.LENGTH_SHORT).show();
                            Intent intent=new Intent(SendRequest.this , CustomerRequests.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intent.putExtra("is_from_notifications",false);
                            startActivity(intent);
                            finish();
                        }else{
                            Toast.makeText(SendRequest.this, response, Toast.LENGTH_SHORT).show();
                        }
                    }
                });


    }
});
     //   retrive(id);
    }
}


