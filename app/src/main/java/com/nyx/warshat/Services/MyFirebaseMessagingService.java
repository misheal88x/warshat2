package com.nyx.warshat.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nyx.warshat.activities.details.RequestDetails;
import com.nyx.warshat.activities.infinites.CustomerRequests;
import com.nyx.warshat.helper.SharedPrefManager;
import com.nyx.warshat.utilities.BaseFunctions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    final int MY_NOTIFICATION_ID = 1;
    final String NOTIFICATION_CHANNEL_ID = "10001";
    NotificationManager notificationManager;
    //Notification myNotification;
    NotificationCompat.Builder builder;
    final Context context = this;
    public SharedPreferences sharedPreferences;


    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        try {
            String recent_token = FirebaseInstanceId.getInstance().getToken();
            BaseFunctions.callUpdateTokenAPI(getApplicationContext(),recent_token);
            Log.i("refreshed_token", "the token: " + recent_token);
        }catch (Exception e){}
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            Map<String, String> params = remoteMessage.getData();
            JSONObject jsonObject = new JSONObject(params);
            Log.i("JSON_OBJECT", "the : " + jsonObject.toString());
            sendNotification(jsonObject);
        }catch (Exception e){}
    }
    private void sendNotification(JSONObject jsonObject) {
        String content = "";
        String target_type = "";
        String target_id = "";
        try {
            //content = jsonObject.getString("content");
            target_type = jsonObject.getString("target_type");
            target_id = jsonObject.getString("target_id");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        switch (target_type){
            //Send Request to Company
            case "100" : {
                if (!SharedPrefManager.getInstance(context).isCustomer()){
                    Intent intent = new Intent(context, RequestDetails.class);
                    intent.putExtra("id" ,target_id);
                    intent.putExtra("show",true);
                    //todo city and region is missing
                    //intent.putExtra("city","دمشق");
                    //intent.putExtra("region","الورود");
                    content = "قام زبون بإرسال طلب إليك قم باستعراضه من هنا";
                    PendingIntent pendingIntent = PendingIntent.getActivity(
                            context,
                            1,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                                    | PendingIntent.FLAG_ONE_SHOT);
                    BaseFunctions.showNotification(context,pendingIntent,content,builder,notificationManager,NOTIFICATION_CHANNEL_ID,MY_NOTIFICATION_ID);
                }
            }break;
            //Company Responds to Customer
            case "200" : {
                if (SharedPrefManager.getInstance(context).isCustomer()) {
                    Intent intent = new Intent(context, CustomerRequests.class);
                    intent.putExtra("is_from_notifications",true);
                    PendingIntent pendingIntent = PendingIntent.getActivity(
                            context,
                            1,
                            intent,
                            PendingIntent.FLAG_UPDATE_CURRENT
                                    | PendingIntent.FLAG_ONE_SHOT);
                    content = "لقد وافقت الورشة على طلبك و ستباشر بالعمل";
                    BaseFunctions.showNotification(context, pendingIntent, content, builder, notificationManager, NOTIFICATION_CHANNEL_ID, MY_NOTIFICATION_ID);
                }
            }break;
        }
    }
}
