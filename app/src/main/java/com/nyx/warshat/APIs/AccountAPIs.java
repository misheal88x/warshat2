package com.nyx.warshat.APIs;

import com.nyx.warshat.models.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AccountAPIs {
    @GET("settings/privacy")
    Call<BaseResponse> get_privacy();

    @FormUrlEncoded
    @POST("verify_phone")
    Call<BaseResponse> confirm_account(
            @Field("phone") String phone,
            @Field("code") String code
    );

    @FormUrlEncoded
    @POST("send_code")
    Call<BaseResponse> resend_code(
            @Field("phone") String phone
    );
    @FormUrlEncoded
    @POST("check-username")
    Call<BaseResponse> check_phone(
            @Field("phone") String phone,
            @Field("email") String email
    );
}
