package com.nyx.warshat.APIs;

import com.nyx.warshat.models.BaseResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RequestsAPIs {

    @Multipart
    @POST("requests/new")
    Call<BaseResponse> add_request(
            @Part("to_id") RequestBody to_id,
            @Part("lat") RequestBody lat,
            @Part("lng") RequestBody lng,
            @Part("req_phone") RequestBody req_phone,
            @Part("dates") RequestBody dates,
            @Part("problem_details") RequestBody problem_details,
            @Part MultipartBody.Part[] images
    );
}
