package Arabs;

import android.content.Context;
import android.graphics.Typeface;

import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;

/**
 * Created by Rock on 2/19/2016.
 */
public class ArabButton extends
        AppCompatButton
{
    public ArabButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ArabButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArabButton(Context context) {
        super(context);
    }
    public void setTypeface(Typeface tf, int style) {
        if (style == Typeface.BOLD) {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Tajawal-Bold.ttf")/*, -1*/);
        } else {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Tajawal-Light.ttf")/*, -1*/);
        }
       // setIconGravity(ICON_GRAVITY_TEXT_START);
    }
}
