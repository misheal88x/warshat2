package Arabs;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatAutoCompleteTextView;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by Rock on 2/18/2016.
 */
public class ArabAutoEditText extends AppCompatAutoCompleteTextView {
    public ArabAutoEditText(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ArabAutoEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArabAutoEditText(Context context) {
        super(context);
    }
    public void setTypeface(Typeface tf, int style) {
        if (style == Typeface.BOLD) {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Tajawal-Bold.ttf")/*, -1*/);
        } else {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Tajawal-Light.ttf")/*, -1*/);
        }
    }
}
