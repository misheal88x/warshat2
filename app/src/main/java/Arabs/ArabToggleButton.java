package Arabs;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatButton;
import android.util.AttributeSet;
import android.widget.ToggleButton;

/**
 * Created by Rock on 2/19/2016.
 */
public class ArabToggleButton extends ToggleButton {
    public ArabToggleButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ArabToggleButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArabToggleButton(Context context) {
        super(context);
    }
    public void setTypeface(Typeface tf, int style) {
        if (style == Typeface.BOLD) {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/GE_SS_Two_Bold.otf")/*, -1*/);
        } else {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/GE_SS_Two_Light.otf")/*, -1*/);
        }
    }
}
