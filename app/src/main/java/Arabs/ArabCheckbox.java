package Arabs;

import android.content.Context;
import android.graphics.Typeface;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatRadioButton;
import android.util.AttributeSet;

/**
 * Created by Rock on 2/19/2016.
 */
public class ArabCheckbox extends AppCompatCheckBox {
    public ArabCheckbox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public ArabCheckbox(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArabCheckbox(Context context) {
        super(context);
    }
    public void setTypeface(Typeface tf, int style) {
        if (style == Typeface.BOLD) {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Tajawal-Bold.ttf")/*, -1*/);
        } else {
            super.setTypeface(Typeface.createFromAsset(getContext().getAssets(), "fonts/Tajawal-Light.ttf")/*, -1*/);
        }
    }
}
