package Arabs;

import android.content.Context;
import android.graphics.Typeface;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by Rock on 2/18/2016.
 */
public class ArabSearchView extends android.widget.SearchView {
    public ArabSearchView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface arabFont = Typeface.createFromAsset(context.getAssets(), "fonts/DroidKufi-Regular.ttf");
        int id = getContext().getResources().getIdentifier("android:id/search_src_text", null, null);
        TextView searchText = (TextView) findViewById(id);
       searchText.setTypeface(arabFont);
        searchText.setPadding(2,2 ,2 ,2);
//        searchText.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimaryDark));
//        searchText.setHintTextColor(ContextCompat.getColor(getContext(), R.color.colorGrayLight));


    }
}
